﻿namespace HomeBase
{
    public class CErroModel : IMensagem
    {

        public CErroModel()
        {
            if (Titulo == null)
            {
                Titulo = eEstadoModel.Error.ToString();
            }
        }

        public string Mensagem { get; set; }
        public string Titulo { get; set; }

        public string Stack { get; set; } = string.Empty;


#if DEBUG
        public bool ExibirStackTrace { get; set; } = true;
#else
        public bool ExibirStackTrace { get; set; } = false;

#endif
    }
}