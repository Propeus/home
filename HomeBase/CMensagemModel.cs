﻿namespace HomeBase
{
    public class CMensagemModel
    {
        public CMensagemModel()
        {
        }

        public int timeout { get; set; } = 2000;
        public CErroModel erro { get; set; }
        public CSucessoModel sucesso { get; set; }
    }
}