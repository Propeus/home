﻿using System;

namespace HomeBase
{
    public class CSignalModel
    {
        public string HandshakeCode { get; internal set; }
        public string Nome { get; set; } = $"Handshake_Affinity";
        public double ExpiresCookie { get; set; } = DateTime.Now.AddDays(2).TotaOfDays();
    }
}