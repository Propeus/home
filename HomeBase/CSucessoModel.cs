﻿using System;

namespace HomeBase
{
    public class CSucessoModel : IMensagem
    {
        public CSucessoModel()
        {
            if (Titulo == null)
            {
                Titulo = eEstadoModel.Sucesso.ToString();
            }
        }

        public string Mensagem { get; set; }
        public string Titulo { get; set; }
        public bool ExibirStackTrace { get; set; }
        public string Stack { get; set; }
    }
}