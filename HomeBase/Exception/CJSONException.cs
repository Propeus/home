﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HomeBase.Excecao
{
    public class CJSONException : CMasterException
    {

        public CJSONException(CJSONModel model,HttpContext contexto=null) : base()
        {
            Excecao = model;

        }

        public virtual CJSONModel Excecao { get; private set; }

    }
}
