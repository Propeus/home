﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HomeBase.Excecao
{
    public class CJSONMensagemException : CJSONException
    {
        public CJSONMensagemException(CJSONMensagemModel model, HttpContext context = null) : base(model)
        {
            Excecao = model;
            if (model.url == null)
            {
                Excecao.url = $"/{context.Request.UrlReferrer.Segments.Skip(1).Take(1).SingleOrDefault()}{context.Request.UrlReferrer.Segments.Skip(2).Take(1).SingleOrDefault()}";
            }
        }

        public new CJSONMensagemModel Excecao { get; private set; }
    }
}
