﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HomeBase.Excecao.ErrorMensagem;

namespace HomeBase.Excecao
{
    public class CJSONPermissaoException : CJSONMensagemException
    {
        public CJSONPermissaoException(CJSONMensagemModel model) : base(model)
        {
        }

        public CJSONPermissaoException(string acesso, string usuarioNome)
            : base(
                  new CJSONMensagemModel
                  {
                      status = eEstadoModel.Error,
                      mensagem = new CMensagemModel
                      {
                          erro = new CErroModel
                          {
                              Mensagem = string.Format(PERMISSAONEGAO, usuarioNome, acesso.Split('.')[1], acesso.Split('.')[0]),
                              Titulo = "Permissao negado."
                          }
                      },
                      Nome = JSONTITULOERROPERMISSAO
                  })
        {

        }


    }
}
