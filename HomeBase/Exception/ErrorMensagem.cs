﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBase.Excecao
{
    public static partial class ErrorMensagem
    {

        public static string ACESSONEGAO => "O usuario {0} não possui acesso a {1}";
        public static string PERMISSAONEGAO => "O usuario {0} não possui permissao de {1} ao acesso {2}";


        public static string JSONTITULOERROPERMISSAO => "Permissao negado.";


    }
}
