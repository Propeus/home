﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HomeBase
{
    public static partial class Helper
    {
        private static string GetALLASCII()
        {
            StringBuilder sb = new StringBuilder();
            Parallel.For(0, 126, (i) =>
            {
                sb.Append((char)i);
            });
            return sb.ToString();
        }


        public static string RandomString(int length=15)
        {
            string chars = GetALLASCII();
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string ToJson(this CJSONModel viewmodel)
        {
            return (new JavaScriptSerializer()).Serialize(viewmodel);
        }
        public static T FromJson<T>(this string model)
        {
            return (new JavaScriptSerializer()).Deserialize<T>(model);
        }
        public static object FromJson(this string model, Type type)
        {
            return (new JavaScriptSerializer()).Deserialize(model, type);
        }

        public static int TotaOfDays(this DateTime date)
        {
            DateTime now = DateTime.Now;
            TimeSpan ts = now - date;
            int days = Math.Abs(ts.Days);
            return days;
        }

    }
}
