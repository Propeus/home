﻿namespace HomeBase
{
    public interface IMensagem
    {
        bool ExibirStackTrace { get; set; }
        string Mensagem { get; set; }
        string Stack { get; set; }
        string Titulo { get; set; }
    }
}