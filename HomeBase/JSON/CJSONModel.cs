﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeBase
{
    public class CJSONModel
    {
        public eEstadoModel status { get; set; }
        public eTipoView tipoView { get; set; }
        public string Nome { get; set; }
        public string url { get; set; }
        public string view { get; set; }
        public string script { get; set; }
        public IModel JSONModel { get; set; }
    }
}
