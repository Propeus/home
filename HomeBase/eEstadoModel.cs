﻿namespace HomeBase
{
    public enum eEstadoModel
    {
        Error = 1,
        Sucesso,
        Aviso,
        View,
        Url,
        ErrorUrl,
        SucessoUrl,
        AvisoUrl,
        SucessoView,
        Handshake,
        Pesquisa,
        ActionResult,
        SignalRChat,
        SucessoSignal,
        ErroSignal
    }

    public enum eTipoView
    {
        Partial,
        Modal
        
    }
}