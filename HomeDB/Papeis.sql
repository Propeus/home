﻿CREATE TABLE [dbo].[Papeis] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [Codigo]          INT          NOT NULL,
    [Nome]            VARCHAR (50) NOT NULL,
    [CodigoAcesso]    INT          NOT NULL,
    [CodigoPermissao] INT          NOT NULL,
    CONSTRAINT [PK__Papeis__3214EC074C0E9DFA] PRIMARY KEY CLUSTERED ([Id] ASC)
);


