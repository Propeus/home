﻿CREATE TABLE [dbo].[Agenda] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [IdUsuario]  INT          NOT NULL,
    [NomeEvento] VARCHAR (50) NOT NULL,
    [Local]      VARCHAR (50) NULL,
    [Inicio]     DATETIME     NOT NULL,
    [Fim]        DATETIME     NULL,
    [DiaInteiro] BIT          NOT NULL,
    [Descricao]  TEXT         NULL,
    [Lembrete]   DATETIME     NULL,
    CONSTRAINT [PK_Agenda] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Agenda_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [dbo].[Usuario] ([Id])
);



