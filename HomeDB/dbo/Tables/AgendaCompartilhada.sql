﻿CREATE TABLE [dbo].[AgendaCompartilhada] (
    [IdAgenda]  INT NOT NULL,
    [IdUsuario] INT NOT NULL,
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_AgendaCompartilhada] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AgendaCompartilhada_Agenda] FOREIGN KEY ([IdAgenda]) REFERENCES [dbo].[Agenda] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AgendaCompartilhada_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [dbo].[Usuario] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [IX_AgendaCompartilhada] UNIQUE NONCLUSTERED ([IdAgenda] ASC, [IdUsuario] ASC)
);



