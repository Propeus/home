﻿CREATE TABLE [dbo].[Amizades] (
    [Id]              INT IDENTITY (1, 1) NOT NULL,
    [IdUsuario]       INT NOT NULL,
    [IdAmigo]         INT NOT NULL,
    [IdEstadoAmizade] INT NOT NULL,
    CONSTRAINT [PK_Amizades] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Amizades_EstadoAmizade] FOREIGN KEY ([IdEstadoAmizade]) REFERENCES [dbo].[EstadoAmizade] ([Id]),
    CONSTRAINT [FK_Amizades_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [dbo].[Usuario] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Amizades_UsuarioAmigo] FOREIGN KEY ([IdAmigo]) REFERENCES [dbo].[Usuario] ([Id])
);



