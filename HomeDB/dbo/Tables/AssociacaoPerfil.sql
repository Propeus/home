﻿CREATE TABLE [dbo].[AssociacaoPerfil] (
    [Id]              INT IDENTITY (1, 1) NOT NULL,
    [CodigoPerfil]    INT NOT NULL,
    [CodigoAcesso]    INT NOT NULL,
    [CodigoPermissao] INT NOT NULL,
    CONSTRAINT [PK_AssociacaoPerfil] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AssociacaoPerfil_Acesso] FOREIGN KEY ([CodigoAcesso]) REFERENCES [dbo].[Acesso] ([Codigo]) ON DELETE CASCADE,
    CONSTRAINT [FK_AssociacaoPerfil_Perfil] FOREIGN KEY ([CodigoPerfil]) REFERENCES [dbo].[Perfil] ([Codigo]) ON DELETE CASCADE,
    CONSTRAINT [FK_AssociacaoPerfil_Permissao] FOREIGN KEY ([CodigoPermissao]) REFERENCES [dbo].[Permissao] ([Codigo]) ON DELETE CASCADE,
    CONSTRAINT [IX_AssociacaoPerfil] UNIQUE NONCLUSTERED ([CodigoAcesso] ASC, [CodigoPerfil] ASC, [CodigoPermissao] ASC)
);

