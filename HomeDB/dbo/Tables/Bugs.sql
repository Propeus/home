﻿CREATE TABLE [dbo].[Bugs]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CodigoBug] INT NOT NULL, 
    [NomeBug] VARCHAR(50) NOT NULL, 
    [Qunatidade] INT NOT NULL DEFAULT 0
)
