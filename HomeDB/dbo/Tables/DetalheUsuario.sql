﻿CREATE TABLE [dbo].[DetalheUsuario] (
    [Id]                          INT NOT NULL,
    [ContadorAgendaCompartilhada] INT NOT NULL,
    [ContadorSolicitacaoAmizade]  INT NOT NULL,
    CONSTRAINT [PK_DetalheUsuario] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DetalheUsuario_Usuario] FOREIGN KEY ([Id]) REFERENCES [dbo].[Usuario] ([Id]) ON DELETE CASCADE
);

