﻿CREATE TABLE [dbo].[EstadoAmizade] (
    [Id]        INT          IDENTITY (1, 1) NOT NULL,
    [Descricao] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_EstadoAmizade] PRIMARY KEY CLUSTERED ([Id] ASC)
);

