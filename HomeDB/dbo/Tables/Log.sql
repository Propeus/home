﻿CREATE TABLE [dbo].[Log] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Mensagem]  VARCHAR (255) NOT NULL,
    [Descricao] TEXT          NULL,
    [DataLog]   DATETIME      NOT NULL,
    [NivelLog]  VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED ([ID] ASC)
);

