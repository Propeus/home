﻿CREATE TABLE [dbo].[Permissao] (
    [Codigo]    INT           IDENTITY (1, 1) NOT NULL,
    [Nome]      VARCHAR (50)  NOT NULL,
    [Descricao] VARCHAR (255) NULL,
    CONSTRAINT [PK_Permissoes_1] PRIMARY KEY CLUSTERED ([Codigo] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Serão dadas todas as permissoes no website', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Permissao';

