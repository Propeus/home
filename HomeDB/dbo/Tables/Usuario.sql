﻿CREATE TABLE [dbo].[Usuario] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Usuario]   VARCHAR (50)  NOT NULL,
    [Senha]     VARCHAR (100) NOT NULL,
    [E_mail]    VARCHAR (50)  NOT NULL,
    [Nome]      VARCHAR (50)  NOT NULL,
    [Sobrenome] VARCHAR (50)  NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([Id] ASC)
);



