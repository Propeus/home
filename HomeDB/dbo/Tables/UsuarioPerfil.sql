﻿CREATE TABLE [dbo].[UsuarioPerfil] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [IdUsuario]    INT NOT NULL,
    [CodigoPerfil] INT NOT NULL,
    CONSTRAINT [PK_UsuarioPerfil] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UsuarioPerfil_Perfil] FOREIGN KEY ([CodigoPerfil]) REFERENCES [dbo].[Perfil] ([Codigo]) ON DELETE CASCADE,
    CONSTRAINT [FK_UsuarioPerfil_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [dbo].[Usuario] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [IX_UsuarioPerfil] UNIQUE NONCLUSTERED ([CodigoPerfil] ASC, [IdUsuario] ASC)
);








GO


