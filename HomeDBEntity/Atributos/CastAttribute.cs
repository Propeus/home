﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeDBEntity.Atributos
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class CastAttribute : Attribute
    {
        public Type TypeCast { get; private set; }

        public CastAttribute(Type type)
        {
            TypeCast = type;
        }
    }
}