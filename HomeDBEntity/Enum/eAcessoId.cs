﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity.Enum
{
    public enum eAcessoId
    {
        Painel,
        Perfil,
        Usuario,
        Configuracao,
        Agenda,
        Amigos,
        Chat,
        Bug
    }
}
