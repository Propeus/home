﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity.Enum
{
    public enum eEstadoAmizade
    {
        Nenhum=1,
        [Description("Aguardando solicitação")]
        AguardandoSolicitacao,
        [Description("Amigos")]
        Amigos
    }
}
