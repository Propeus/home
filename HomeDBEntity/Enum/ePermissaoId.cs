﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity.Enum
{
    public enum ePermissaoId
    {
        Gerenciar,
        Exibir,
        Excluir,
        Editar,
        Associar,
        Inserir,
        Pesquisar,
        SQL
    }
}
