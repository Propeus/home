﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity.Helper
{
    public static partial class Helper
    {

        public static string Descricao<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        public static List<T> AddIfNotExists<T>(this List<T> l, IEnumerable<T> itens)
        {
            foreach (var item in itens)
            {
                if (l.Count(x => x.EqualsValue(item)) == 0)
                {
                    l.Add(item);
                }
            }


            return l;
        }
        public static bool EqualsValue<T>(this T v, T c)
        {

            var Pv = v.GetType().GetProperties();
            var Pc = c.GetType().GetProperties();

            bool result = true;

            if (Pv.Any(x => Pc.Contains(x)))
            {
                foreach (var iPv in Pv)
                {
                    var o = iPv.GetValue(v);
                    var q = iPv.GetValue(c);


                    if ((o != null && q != null) && (o.GetType().IsPrimitive && q.GetType().IsPrimitive) && !(o.Equals(q)))
                    {
                        return false;
                    }
                }

            }
            else
            {
                return false;
            }


            return result;
        }
    }
}
