﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity.Helper
{
    internal enum MensagemHelper
    {
        [Description("Não foi encontrado nenhuma associação!")]
        E_AssociacaoNaoEncontrado = 1000,
        [Description("Não existe nenhuma associação!")]
        E_AssociacaoInexistente,
        [Description("A associação {0}.{1} já existe no perfil {2}")]
        E_AssociacaoExistente
    }
}
