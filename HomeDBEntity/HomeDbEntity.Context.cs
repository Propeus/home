﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HomeDBEntity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class HomeDSNVEntities : DbContext
    {
        public HomeDSNVEntities()
            : base("name=HomeDSNVEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Amizades> Amizades { get; set; }
        public virtual DbSet<EstadoAmizade> EstadoAmizade { get; set; }
        public virtual DbSet<Acesso> Acesso { get; set; }
        public virtual DbSet<Agenda> Agenda { get; set; }
        public virtual DbSet<AgendaCompartilhada> AgendaCompartilhada { get; set; }
        public virtual DbSet<AssociacaoPerfil> AssociacaoPerfil { get; set; }
        public virtual DbSet<DetalheUsuario> DetalheUsuario { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<Perfil> Perfil { get; set; }
        public virtual DbSet<Permissao> Permissao { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<UsuarioPerfil> UsuarioPerfil { get; set; }
        public virtual DbSet<Bugs> Bugs { get; set; }
    }
}
