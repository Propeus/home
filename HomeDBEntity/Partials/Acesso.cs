﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity
{
   public partial class Acesso
    {
        public bool Valido => ((Nome != string.Empty && Nome != ""));

        public List<Permissao> Permissoes { get; set; }
    }
}
