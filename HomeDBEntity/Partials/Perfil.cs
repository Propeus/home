﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity
{
    public partial class Perfil
    {
        public bool Valido => (Nome != string.Empty && Nome != "");

        public List<Acesso> Acessos { get; set; }

    }
}
