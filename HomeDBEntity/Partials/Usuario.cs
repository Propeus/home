﻿using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity
{

    public partial class Usuario
    {

        public bool Valido => (

            !string.IsNullOrEmpty(this.Nome) &&
            !string.IsNullOrEmpty(this.Senha) &&    
            !string.IsNullOrEmpty(this.Usuario1)

            );

        public List<Perfil> Perfis { get; set; } 
        public List<string> Acessos { get; set; }
    }
}
