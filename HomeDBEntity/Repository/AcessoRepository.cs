﻿using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace HomeDBEntity
{
    public class AcessoRepository : GenericRepository<Acesso>
    {
        [Transaction]
        public override void Inserir(Acesso acesso)
        {
            base.Inserir(acesso);
        }
        [Transaction]
        public override void Remover(Acesso acesso)
        {
            base.Remover(acesso);
        }
        [Transaction]
        public override void Editar(Acesso acesso)
        {
            base.Editar(acesso);
        }

        public Acesso ObterPorCodigo(int codigo)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Acesso.FirstOrDefault(x => x.Codigo == codigo);
            }
        }

        public List<Acesso> ObterPorCodigos(params int[] codigos)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Acesso.Where(x => codigos.Contains(x.Codigo)).ToList();
            }
        }

        public Acesso ObterPorNome(string nome)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Acesso.FirstOrDefault(x => x.Nome == nome);
            }
        }

        public List<Acesso> ObterTodos()
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Acesso.ToList();
            }
        }

        public List<Acesso> ObterPorNomes(string[] nomes)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Acesso.Where(x => nomes.Contains(x.Nome)).ToList();
            }
        }



        public List<Acesso> ObterTodosNaoAssociados()
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var ass = db.AssociacaoPerfil;
                var ac = db.Acesso.AsQueryable();
                var pm = db.Permissao.AsQueryable();

                var aces = (from a in db.Acesso
                            from p in db.Permissao
                            select new
                            {
                                CodigoAcesso = a.Codigo,
                                CodigoPermissao = p.Codigo
                            }).ToList();

                //terminar de realizar crossjoin https://stackoverflow.com/questions/56547/how-do-you-perform-a-cross-join-with-linq-to-sql
                //remover a partir da lista acima remova todos que estao inseridos na tabela associacao
                aces.RemoveAll(x => ass.Count(y => y.CodigoAcesso == x.CodigoAcesso && y.CodigoPermissao == x.CodigoPermissao) > 0);

                List<Acesso> La = new List<Acesso>();

                foreach (var item in aces)
                {
                    var a = ac.First(x => x.Codigo == item.CodigoAcesso);
                    var p = pm.First(x => x.Codigo == item.CodigoPermissao);

                    var r = La.FirstOrDefault(x => x.Codigo == item.CodigoAcesso);
                    if (r == null)
                    {
                        a.Permissoes = new List<Permissao>
                         {
                            p
                         };
                        La.Add(a);
                    }
                    else
                    {
                        r.Permissoes.Add(p);
                    }
                }

                return La;
            }
        }

        public List<Acesso> ObterPorPerfil(Perfil perfil)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {

                var r = (from ass in db.AssociacaoPerfil
                         from acess in db.Acesso
                         where ass.CodigoPerfil == perfil.Codigo && acess.Codigo == ass.CodigoAcesso
                         group acess by acess.Codigo).Select(x => x.FirstOrDefault()).ToList();

                return r;
            }

        }
    }
}
