﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Data.Entity;

namespace HomeDBEntity.Repository
{
    public class AgendaRepository : GenericRepository<Agenda>
    {

        public override void Editar(Agenda entity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (GenericRepository<AgendaCompartilhada> genericorepository = new GenericRepository<AgendaCompartilhada>())
                {
                    var ids = entity.AgendaCompartilhada.Select(y => y.IdUsuario);
                    genericorepository.Remover(x => ids.Contains(x.IdUsuario) && x.IdAgenda==entity.Id);
                    base.Editar(entity);
                    genericorepository.InserirLote(entity.AgendaCompartilhada);
                    scope.Complete();
                }
            }

        }

        public override List<Agenda> Obter(Expression<Func<Agenda, bool>> expressao)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Agenda.Include("Usuario").Include("AgendaCompartilhada").Where(expressao).ToList();
            }
        }

        public List<Agenda> ObterPorUsuario(int id)
        {
            List<Agenda> agenda = Obter(x => x.IdUsuario == id || x.AgendaCompartilhada.Count(y => y.IdUsuario == id) > 0).ToList();
            return agenda;
        }
        public Agenda ObterPorId(int id)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var agenda = db.Agenda.Include(x => x.AgendaCompartilhada).Include(x => x.Usuario).Where(x => x.Id == id).FirstOrDefault();
                foreach(var agendaCompatilhada in agenda.AgendaCompartilhada)
                {
                    agendaCompatilhada.Usuario = db.Usuario.FirstOrDefault(x => x.Id == agendaCompatilhada.IdUsuario);
                };
                return agenda;

            }
        }

        public void Descompartilhar(int idUsuario,params int []idsAgenda)
        {
            using(GenericRepository<AgendaCompartilhada> genericRepository = new GenericRepository<AgendaCompartilhada>())
            {
                genericRepository.Remover(x => idsAgenda.Contains(x.IdAgenda) && x.IdUsuario == idUsuario);
            }
        }

        public override void Inserir(Agenda entity)
        {
            base.Inserir(entity);
        }

        public void Compartilhar(Agenda agenda, params int[] usuarios)
        {
            Parallel.ForEach(usuarios, (usuario) =>
           {
               agenda.AgendaCompartilhada.Add(new AgendaCompartilhada
               {
                   IdUsuario = usuario
               });
           });

            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.AgendaCompartilhada.AddRange(agenda.AgendaCompartilhada);
                    db.SaveChanges();
                    scope.Complete();
                }
            }

        }

    }
}
