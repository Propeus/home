﻿using HomeDBEntity.Enum;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace HomeDBEntity
{
    public class AmizadeRepository : GenericRepository<Amizades>
    {
        public void Inserir(int IdUsuario, int IdAmigo)
        {
            var amg = new Amizades()
            {
                IdUsuario = IdUsuario,
                IdAmigo = IdAmigo,
                IdEstadoAmizade = eEstadoAmizade.AguardandoSolicitacao
            };
            using (TransactionScope scope = new TransactionScope())
            {
                base.Inserir(amg);

                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    var detalheUser = db.DetalheUsuario.First(x => x.Id == IdAmigo);
                    detalheUser.ContadorSolicitacaoAmizade++;
                    db.DetalheUsuario.AddOrUpdate(detalheUser);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        public void Remover(int IdUsuario, int IdAmigo)
        {
            var amg = new Amizades()
            {
                IdUsuario = IdUsuario,
                IdAmigo = IdAmigo
            };

            base.Remover(amg);
        }


        public List<Amizades> ObterSolicitacoesAmizade(int IdUsuario)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var data = db.Amizades.Include("Usuario").Where(x => x.IdAmigo == IdUsuario && x.IdEstadoAmizade == eEstadoAmizade.AguardandoSolicitacao).ToList();
                return data;
            }
        }
        public List<Amizades> ObterAmizades(int IdUsuario)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var data = db.Amizades.Include("Usuario").Include("Usuario1").Where(x => (x.IdAmigo == IdUsuario || x.IdUsuario == IdUsuario)).ToList();
                return data;
            }
        }



        public int ObterContadorSolicitacaoAmizades(int idUsuario)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.DetalheUsuario.First(x => x.Id == idUsuario).ContadorSolicitacaoAmizade;
            }
        }



        public void SolicitarAmizade(int IdUsuario, int idAmigo)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.Amizades.Add(new Amizades
                    {
                        IdAmigo = idAmigo,
                        IdUsuario = IdUsuario,
                        IdEstadoAmizade = eEstadoAmizade.AguardandoSolicitacao
                    });

                    var data = db.DetalheUsuario.FirstOrDefault(x => x.Id == idAmigo);
                    data.ContadorSolicitacaoAmizade++;
                    db.DetalheUsuario.AddOrUpdate(data);
                    db.SaveChanges();
                    scope.Complete();
                }
            }

        }

        public void DesfazerSolicitacaoAmizade(int userId, int idAmigo)
        {
            base.Remover(x => (x.IdAmigo == userId ^ x.IdUsuario == userId) && (x.IdAmigo == idAmigo ^ x.IdUsuario == idAmigo));
        }

        public void ConfirmarAmizade(int userId, List<int> idAmigo)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    var data = db.Amizades.Where(x => (x.IdAmigo == userId && idAmigo.Contains(x.IdUsuario))).ToArray();
                    foreach (var item in data)
                    {
                        item.IdEstadoAmizade = eEstadoAmizade.Amigos;
                    }
                    db.Amizades.AddOrUpdate(data);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        public void RecusarAmizade(int IdUsuarioAtual, List<int> IdAmigo)
        {
            Remover(x => (x.IdAmigo == IdUsuarioAtual && IdAmigo.Contains(x.IdUsuario)));
        }
        public void DesfazerAmizade(int userId, int[] ids)
        {
            base.Remover(x => x.IdUsuario == userId && ids.Contains(x.IdAmigo) || ids.Contains(x.IdUsuario) && x.IdAmigo == userId);
        }
        public void ResetarContadorAmizade(int userId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    var data = db.DetalheUsuario.First(x => x.Id == userId);
                    data.ContadorSolicitacaoAmizade = 0;
                    db.DetalheUsuario.AddOrUpdate(data);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
    }
}
