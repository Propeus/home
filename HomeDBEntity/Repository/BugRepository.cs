﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity.Repository
{
    public class BugRepository : GenericRepository<Bugs>
    {
        public Bugs ObterBug(int codigoBug)
        {
            return ObterPrimeiro(x => x.CodigoBug == codigoBug);
        }

        public int ObterQuantidade()
        {
            return ObterTodos().Select(x => x.Qunatidade).Sum();
        }

        public void RelatarBug(Exception excecao)
        {
            var data = ObterBug(excecao.HResult);
            if (data == null)
            {
                data = new Bugs
                {
                    CodigoBug = excecao.HResult,
                    NomeBug = excecao.GetType().Name,
                    Qunatidade = 1
                };
            }
            else
            {
                data.Qunatidade++;
            }
            Editar(data);

        }
    }
}
