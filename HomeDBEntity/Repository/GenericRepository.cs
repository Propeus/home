﻿using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Transactions;

namespace HomeDBEntity
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>, IDisposable where TEntity : class
    {

        public GenericRepository()
        {
        }
        ~GenericRepository()
        {
        }

        [Transaction]
        public virtual void Inserir(TEntity entity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.Set<TEntity>().Add(entity);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        [Transaction]
        public void InserirLote(IEnumerable<TEntity> entity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.Set<TEntity>().AddRange(entity);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        [Transaction]
        public virtual void Editar(TEntity entity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.Set<TEntity>().AddOrUpdate(entity);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        [Transaction]
        public virtual void Remover(TEntity entity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    var entitie = db.Set<TEntity>();
                    entity = entitie.Attach(entity);
                    entitie.Remove(entity);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }

        public List<TEntity> ObterTodos()
        {
           using(HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Set<TEntity>().ToList();
            }
        }

        [Transaction]
        public void RemoverLote(IEnumerable<TEntity> entity)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.Set<TEntity>().RemoveRange(entity);
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        public virtual void Remover(Expression<Func<TEntity, bool>> expressao)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.Set<TEntity>().RemoveRange(db.Set<TEntity>().Where(expressao));
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }
        public virtual TEntity ObterPrimeiro(Expression<Func<TEntity, bool>> expressao)
        {

            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Set<TEntity>().Where(expressao).FirstOrDefault();
            }

        }
        public virtual List<TEntity> Obter(Expression<Func<TEntity, bool>> expressao)
        {

            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Set<TEntity>().Where(expressao).ToList();
            }
        }

        public string ComandoSQL(string Query)
        {
            using(HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var connection = (System.Data.SqlClient.SqlConnection)db.Database.Connection;
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                var dt = new DataTable();

                using (var com = new System.Data.SqlClient.SqlDataAdapter(Query, connection))
                {
                    com.Fill(dt);
                }
                string res = string.Join(";", dt.Rows.OfType<DataRow>().Select(x => string.Join("|", x.ItemArray)));
                return res;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {



                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        // ~GenericRepository() {
        //   // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
        //   Dispose(false);
        // }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion

        //public void Dispose()
        //{
        //    if(db!=null && scope!= null)
        //    {
        //        db.SaveChanges();
        //        scope.Complete();
        //    }
        //    if (db != null)
        //    {
        //        db.Dispose();
        //        db = null;
        //    }
        //    if (scope != null)
        //    {
        //        scope.Dispose();
        //        scope = null;
        //    }




        //}
    }
}
