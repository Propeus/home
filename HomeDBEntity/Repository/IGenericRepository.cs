﻿namespace HomeDBEntity
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void Editar(TEntity entity);
        void Inserir(TEntity entity);
        void Remover(TEntity entity);
    }
}