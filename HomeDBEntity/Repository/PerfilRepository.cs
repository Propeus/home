﻿using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using HomeDBEntity.Helper;
using static HomeDBEntity.Helper.Helper;

namespace HomeDBEntity
{
    public class PerfilRepository : GenericRepository<Perfil>
    {
        public override void Inserir(Perfil perfil)
        {
            base.Inserir(perfil);
        }
        public override void Editar(Perfil perfil)
        {
            base.Editar(perfil);
        }
        public override void Remover(Perfil perfil)
        {
            base.Remover(x => x.Codigo == perfil.Codigo);
        }

        public Perfil ObterPorCodigo(int codigo)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                Perfil perfil = db.Perfil.Include("AssociacaoPerfil").FirstOrDefault(x => x.Codigo == codigo);
                //if (perfil != null)
                //{
                //    var associacaoPerfil = db.AssociacaoPerfil.Include("Permissao").Include("Acesso").Where(x => x.CodigoPerfil == codigo).ToList();
                //    var Acessos = associacaoPerfil.Select(x => x.Acesso).ToList();
                //    //var Acessos = acessoRepository.ObterPorCodigos(associacaoPerfil.Select(x => x.CodigoAcesso).ToArray());
                //    foreach (var acesso in Acessos)
                //    {
                //        var permissoes = associacaoPerfil.Where(x => x.Acesso.Codigo == acesso.Codigo).Select(x => x.Permissao).ToList();
                //        acesso.Permissoes = permissoes;
                //    }
                //    perfil.Acessos = Acessos;
                //}

                return perfil;
            }
        }
        public List<Perfil> ObterTodos()
        {
            List<Perfil> perfis = new List<Perfil>();
            Perfil perfilaux;
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var codigos = db.Perfil.Include("Permissao").Include("Acesso").Select(x => x.Codigo).Distinct().ToList();
                foreach (var codigo in codigos)
                {
                    perfilaux = ObterPorCodigo(codigo);
                    if (perfilaux != null)
                    {
                        perfis.Add(perfilaux);
                    }
                }

                return perfis;
            }
        }
        /// <summary>
        /// Retorna o perfil que possui a associação entre Acesso e perfil.
        /// Caso não encontre nenhuma associação retorna o default de <see cref="Perfil"/>
        /// </summary>
        /// <param name="acesso"></param>
        /// <param name="permissao"></param>
        /// <returns>Caso não encontre nenhuma associação retorna o default de <see cref="Perfil"/></returns>
        public Perfil ObterPorAssociacao(Acesso acesso, Permissao permissao)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var perfil = db.AssociacaoPerfil.FirstOrDefault(x => x.CodigoAcesso == acesso.Codigo && x.CodigoPermissao == permissao.Codigo);
                if (perfil == null)
                {
                    return default(Perfil);
                }
                else
                {
                    return ObterPorCodigo(perfil.CodigoPerfil);
                }
            }
        }

        public void Desassociar(Perfil perfil)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    var assos = db.AssociacaoPerfil.Where(x => x.CodigoPerfil == perfil.Codigo);
                    db.AssociacaoPerfil.RemoveRange(assos);
                    db.SaveChanges();
                }
                scope.Complete();
            }
        }

        public Perfil ObterPorNome(string nome)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return ObterPorCodigo(db.Perfil.First(x => x.Nome == nome).Codigo);
            }
        }

        [Transaction]
        public void Associar(Perfil perfil, Acesso acesso, Permissao permissao)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {

                    db.AssociacaoPerfil.Add(new AssociacaoPerfil()
                    {
                        CodigoPerfil = perfil.Codigo,
                        CodigoAcesso = acesso.Codigo,
                        CodigoPermissao = permissao.Codigo
                    });
                    db.SaveChanges();
                }
                scope.Complete();
            }
        }
        public void Desassociar(Acesso acesso, Permissao permissao)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {

                    if (ExisteAssociacaoPerfil(acesso, permissao))
                    {
                        var asociassao = db.AssociacaoPerfil.FirstOrDefault(x => x.CodigoAcesso == acesso.Codigo && x.CodigoPermissao == x.CodigoPermissao);
                        if (asociassao != null)
                        {
                            db.AssociacaoPerfil.Remove(asociassao);
                        }
                    }
                    else
                    {
                        throw new Exception(MensagemHelper.E_AssociacaoInexistente.Descricao());
                    }
                    db.SaveChanges();
                }

                scope.Complete();
            }
        }

        public bool ExisteAssociacaoPerfil(Acesso acesso, Permissao permissao)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.AssociacaoPerfil.Count(x => x.CodigoAcesso == acesso.Codigo && x.CodigoPermissao == permissao.Codigo) > 0;
            }
        }


        public void nInserir(Perfil perfil)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                base.Inserir(perfil);
       
                scope.Complete();
            }

        }



    }
}
