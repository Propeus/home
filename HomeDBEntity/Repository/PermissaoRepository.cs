﻿using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeDBEntity
{
    public class PermissaoRepository : GenericRepository<Permissao>
    {
        [Transaction]
        public override void Inserir(Permissao permissao)
        {
            base.Inserir(permissao);
        }
        [Transaction]
        public override void Remover(Permissao permissao)
        {
            base.Remover(permissao);
        }
        [Transaction]
        public override void Editar(Permissao permissao)
        {
            base.Editar(permissao);
        }

        public Permissao ObterPorCodigo(int codigo)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Permissao.FirstOrDefault(x => x.Codigo == codigo);
            }
        }

        public List<Permissao> ObterPorCodigos(params int[] codigos)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Permissao.Where(x => codigos.Contains(x.Codigo)).ToList();
            }
        }

        public Permissao ObterPorNome(string nome)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Permissao.FirstOrDefault(x => x.Nome == nome);
            }
        }
        public List<Permissao> ObterTodos()
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Permissao.ToList();
            }

        }

        public List<Permissao> ObterPorNomes(string[] nomes)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.Permissao.Where(x => nomes.Contains(x.Nome)).ToList();
            }
        }

        public List<Permissao> ObterPorAcesso(Acesso acesso)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var r = (from ass in db.AssociacaoPerfil
                         from perm in db.Permissao
                         where ass.CodigoAcesso == acesso.Codigo && perm.Codigo == ass.CodigoPermissao
                         group perm by perm.Codigo).Select(x => x.FirstOrDefault()).ToList();

                return r;
            }
        }

    }
}
