﻿using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace HomeDBEntity.Repository
{
    public class UsuarioRepository : GenericRepository<Usuario>
    {

        public void Novo(Usuario usuario)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                Inserir(usuario);
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {

                    db.DetalheUsuario.Add(new DetalheUsuario
                    {
                        Id = usuario.Id
                    });
                    db.SaveChanges();
                    scope.Complete();
                }

            }

        }


        public override void Remover(Usuario usuario)
        {
            base.Remover(usuario);
        }

        public override void Editar(Usuario usuario)
        {
            base.Editar(usuario);
        }

        public Usuario ObterPorId(int id)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                Usuario usuario = db.Usuario.Include("UsuarioPerfil").FirstOrDefault(x => x.Id == id);

                return usuario;
            }
        }

        public List<Usuario> ObterPorIds(int[] id)
        {

            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var usuarios = db.Usuario.Include("UsuarioPerfil").Where(x => id.Contains(x.Id));
                if (usuarios != null)
                {
                    Parallel.ForEach(usuarios, (usuario) =>
                    {
                        usuario.Perfis = new List<Perfil>();
                        usuario.Acessos = new List<string>();
                        var perfisCodigo = usuario.UsuarioPerfil.Select(x => x.CodigoPerfil).Distinct().ToArray();
                        foreach (var perfilcodigo in perfisCodigo)
                        {
                            var perfilaux = usuario.UsuarioPerfil.Where(x => x.Perfil.Codigo == perfilcodigo).FirstOrDefault().Perfil;
                            if (perfilaux != null)
                            {
                                usuario.Perfis.Add(perfilaux);
                                usuario.Acessos.AddRange(perfilaux.AssociacaoPerfil.Select(x => new { AcessoNome = x.Acesso.Nome, PermissaoNome = x.Permissao.Nome }).Select(x => $"{x.AcessoNome}.{x.PermissaoNome}"));
                            }
                        }
                    });
                }
                return usuarios.ToList();
            }
        }
        [Transaction]
        public Usuario ObterPorLogin(string login, string senha)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                Usuario usuario = db.Usuario.Include("UsuarioPerfil").Include("DetalheUsuario").FirstOrDefault(x => x.Usuario1 == login && x.Senha == senha);
                if (usuario != null)
                {
                    usuario.Perfis = new List<Perfil>();
                    usuario.Acessos = new List<string>();
                    var perfisCodigo = usuario.UsuarioPerfil.Select(x => x.CodigoPerfil).Distinct().ToArray();
                    foreach (var perfilcodigo in perfisCodigo)
                    {
                        var perfilaux = usuario.UsuarioPerfil.Where(x => x.Perfil.Codigo == perfilcodigo).FirstOrDefault().Perfil;
                        if (perfilaux != null)
                        {
                            usuario.Perfis.Add(perfilaux);
                            usuario.Acessos.AddRange(perfilaux.AssociacaoPerfil.Select(x => new { AcessoNome = x.Acesso.Nome, PermissaoNome = x.Permissao.Nome }).Select(x => $"{x.AcessoNome}.{x.PermissaoNome}"));
                        }
                    }
                }

                return usuario;
            }
        }


        public void AssociarPerfil(Usuario usuario, Perfil perfil)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    if (usuario.Valido)
                    {
                        db.UsuarioPerfil.Add(new UsuarioPerfil()
                        {
                            CodigoPerfil = perfil.Codigo,
                            IdUsuario = usuario.Id
                        });
                        db.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("O usuario não é valido");
                    }
                }
                scope.Complete();
            }

        }

        public void DesassociarPerfil(Usuario usuario, Perfil perfil)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (HomeDSNVEntities db = new HomeDSNVEntities())
                {
                    db.UsuarioPerfil.Remove(db.UsuarioPerfil.First(x => x.CodigoPerfil == perfil.Codigo && x.IdUsuario == usuario.Id));
                }
            }
        }

        public List<Usuario> ObterNaoAssociados(Perfil perfil)
        {

            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                var ass = from a in db.UsuarioPerfil
                          where a.CodigoPerfil == perfil.Codigo
                          select a;

                var r = from b in db.Usuario
                        where !ass.Select(x => x.IdUsuario).Contains(b.Id)
                        select b;

                return r.ToList();
            }

        }

        public bool Associado(Usuario usuario, Perfil perfil)
        {
            using (HomeDSNVEntities db = new HomeDSNVEntities())
            {
                return db.UsuarioPerfil.Count(x => x.CodigoPerfil == perfil.Codigo && x.IdUsuario == usuario.Id) > 0;
            }
        }

    }
}
