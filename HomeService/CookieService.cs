﻿using HomeBase;
using System;
using System.Web;
using System.Web.Script.Serialization;

namespace HomeService
{
    public class CookieService
    {

        public void Inserir(string obj, string cookieNome, string cookieKey, DateTime Expiracao = default(DateTime))
        {
            //if (Expiracao == default(DateTime))
            //{
            //    Expiracao = DateTime.Now.AddSeconds(5);
            //}

            HttpCookie c = default(HttpCookie);
            if (HttpContext.Current.Request.Cookies.Get(cookieNome) != null)
            {
                c = HttpContext.Current.Request.Cookies.Get(cookieNome);
            }
            else
            {
                c = new HttpCookie(cookieNome);
            }
            //c.Path = "/";
            //c.Values.Add(cookieKey, obj);



            HttpContext.Current.Response.Cookies.Set(c);

        }
        public void Inserir(object model, string cookieNome, DateTime Expiracao = default(DateTime))
        {


            if (Expiracao == default(DateTime))
            {
                Expiracao = DateTime.Now.AddSeconds(15);
            }

            HttpCookie c = default(HttpCookie);
            if (HttpContext.Current.Request.Cookies.Get(cookieNome) != null)
            {
                c = HttpContext.Current.Request.Cookies.Get(cookieNome);
            }
            else
            {
                c = new HttpCookie(cookieNome);
            }
            c.Path = "/";
            c.Value = (new JavaScriptSerializer()).Serialize(model);
            HttpContext.Current.Response.Cookies.Set(c);
        }

        public bool ExisteCookie(string cookieNome)
        {
            return HttpContext.Current.Request.Cookies.Get(cookieNome) != null;
        }

        public void Remover(string cookieNome)
        {
            HttpContext.Current.Response.Cookies[cookieNome].Expires = DateTime.Now.AddDays(-1);
        }

        public void EnviarMensagem(string mensagem, string titulo, eEstadoModel tipoMensagem)
        {
            Inserir(mensagem, tipoMensagem.ToString(), "Mensagem");
            Inserir(mensagem, tipoMensagem.ToString(), "Titulo");
        }
        public void EnviarMensagem(object JsonModel, eEstadoModel tipoMensagem)
        {
            Inserir(JsonModel, tipoMensagem.ToString());
        }
        public void EnviarJSONModel(CJSONModel model)
        {
            Inserir(model, "CJsonModel_Cookie");
        }
        public void EnviarConfiguracoesGlobais()
        {
            var data = new CJSONConfiguracoesGlobaisModel
            {
                Id = UsuarioService.Atual.Id
            };

            Inserir(data, Helper.UsuarioGlobalConfiguracao);
        }
        public string ObterValorSessao()
        {
            return ObterValorCookie<object>(Helper.SessaoCookie) as string;
        }
        public T ObterValorCookie<T>(string nome)
        {
            if (ExisteCookie(nome))
            {
                return (new JavaScriptSerializer()).Deserialize<T>(HttpContext.Current.Request.Cookies.Get(nome)?.Value);
            }
            else
            {
                return default(T);
            }
        }

    }
}
