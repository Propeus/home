﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace HomeService
{
    public class EmailService
    {

        SmtpClient EmailClient;

        public EmailService()
        {
            EmailClient = new SmtpClient()
            {
                Host = ConfigurationManager.AppSettings["HostSMTP"],
                Port = int.Parse(ConfigurationManager.AppSettings["PortSMTP"]),
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserNameSMTP"], ConfigurationManager.AppSettings["PasswordSMTP"])
            };
            
        }

        public void EnviarEmail(string emailPara, string titulo, string conteudo)
        {
          
            MailMessage msg = new MailMessage()
            {
                From = new MailAddress(ConfigurationManager.AppSettings["EmailSMTP"]),
                Subject = titulo,
                Body = conteudo
            };
            msg.To.Add(emailPara);

            EmailClient.Send(msg);

        }

    }

}

