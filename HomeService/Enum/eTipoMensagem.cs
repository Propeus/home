﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService.Enum
{
    public enum eTipoMensagem
    {
        Error=1,
        Sucesso,
        Aviso,
        View,
        Url,
        ErrorUrl,
        SucessoUrl,
        AvisoUrl,
        SucessoView,
        Handshake
    }
}
