﻿using HomeBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HomeService
{
    public static partial class Helper
    {

        public static string UsuarioGlobalConfiguracao => "CJSONConfiguracoesGlobaisModel_Cookie";
        public static string UsuariosSessao => "User";
        public static string SessaoCookie => "ASP.NET_SessionId";
        public static string UsuariosSignalRSessao => "Handshake_Affinity";

        public static JsonResult ToJsonResult(this CJSONModel viewmodel)
        {
            if (!(new CookieService()).ExisteCookie("CJsonViewModel_Cookie"))
                return (new JsonResult() { Data = (new JavaScriptSerializer()).Serialize(viewmodel), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            else
                return null;
        }

        public static string GetDescription(this System.Enum value)
        {

            DescriptionAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;

        }

        public static bool IsNullOrEmpty<T>(this List<T> l)
        {
            return l == null || l.Count == 0;
        }

        public static string ToJSON(this DateTime date)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(date);
        }

    }
}
