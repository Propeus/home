﻿using HomeDBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService
{

    public enum eTipoLog
    {
        Info,
        Erro,
        Fatal
    }

    public class LogSevice : GenericService<Log>
    {
        public void LogDb(string mensagem,string descricao,eTipoLog tipo)
        {
            Log log = new Log()
            {
                DataLog = DateTime.Now,
                Mensagem = mensagem,
                Descricao = descricao,
                NivelLog = tipo.ToString()
            };
            base.Inserir(log);
        }
    }
}
