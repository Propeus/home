﻿using HomeBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService
{
    public class CJSONConfiguracoesGlobaisModel : CJSONMensagemModel
    {

        public int Id { get; set; }
        public List<string> IdGrupoAdmin { get; set; } = new List<string>();    
        
    }
}
