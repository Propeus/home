﻿using HomeDBEntity;
using HomeDBEntity.Repository;
using System.Collections.Generic;
using System.Linq;
using System;
using HomeDBEntity.Atributos;
using System.Transactions;

namespace HomeService
{
    public class AcessoService : GenericService<Acesso>
    {
        protected new AcessoRepository Repository => base.Repository as AcessoRepository;

        public AcessoService() : base(new AcessoRepository())
        {

        }

        
        public override void Editar(Acesso acesso)
        {
            if (acesso.Valido)
            {
                base.Editar(acesso);
            }
        }
        
        public override void Inserir(Acesso acesso)
        {
            if (acesso.Valido)
            {
                base.Inserir(acesso);
            }
        }
                
        public override void Remover(Acesso acesso)
        {
            base.Remover(acesso);
        }

        public Acesso ObterPorCodigo(int codigo)
        {
            return Repository.ObterPorCodigo(codigo);
        }

        public List<Acesso> ObterPorCodigos(params int[] codigos)
        {
            return Repository.ObterPorCodigos(codigos);
        }

        public Acesso ObterPorNome(string nome)
        {
            return Repository.ObterPorNome(nome);
        }

        public List<Acesso> ObterPorNomes(params string[] nomes)
        {
            return Repository.ObterPorNomes(nomes);
        }

        public List<Acesso> ObterTodos()
        {
            return Repository.ObterTodos();
        }

        public List<Acesso> ObterTodosNaoAssociados()
        {
            return Repository.ObterTodosNaoAssociados();
        }

        public List<Acesso> ObterPorPerfil(Perfil perfil)
        {

            using (PermissaoRepository permissaoRepository = new PermissaoRepository())
            {
                var r = Repository.ObterPorPerfil(perfil);

                foreach (var item in r)
                {
                    item.Permissoes = permissaoRepository.ObterPorAcesso(item);
                }
                return r;
            }


        }


    }
}
