﻿using HomeDBEntity;
using HomeDBEntity.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace HomeService
{
    public class AgendaService : GenericService<Agenda>
    {
        protected new AgendaRepository Repository => base.Repository as AgendaRepository;

        public AgendaService() : base(new AgendaRepository())
        {

        }

        public List<Agenda> ObterPorUsuario(Usuario usuario)
        {
            return Repository.ObterPorUsuario(usuario.Id);
        }
        public Agenda ObterPorId(int Id)
        {
            return Repository.ObterPorId(Id);
        }
        public override void Inserir(Agenda entity)
        {
            base.Inserir(entity);
        }
        public void Remover(params int[] Ids)
        {
            base.Remover(x => Ids.Contains(x.Id));
        }
        public override void Editar(Agenda entity)
        {
            Repository.Editar(entity);
        }
        public void Compartilhar(Agenda agenda, params int[] contatosId)
        {

            using (UsuarioService usuarioService = new UsuarioService())
            {
                Repository.Compartilhar(agenda, contatosId);
            }

        }

        public List<Agenda> ObterPorTitulo(string NomeEvento)
        {
            int userId = UsuarioService.Atual.Id;
            return Repository.Obter(x => (x.IdUsuario == userId || x.AgendaCompartilhada.Count(y => y.Id == userId) > 0) && x.NomeEvento.Contains(NomeEvento));
        }

        public List<Agenda> ObterPorIds(int[] ids)
        {
            return Repository.Obter(x => ids.Contains(x.Id));
        }

        public void Descompartilhar(params int []ids)
        {
            int userId = UsuarioService.Atual.Id;
            Repository.Descompartilhar(userId, ids);
        }
    }
}
