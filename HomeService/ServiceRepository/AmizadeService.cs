﻿using HomeDBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService
{
    public class AmizadeService : GenericService<Amizades>
    {
        protected new AmizadeRepository Repository => base.Repository as AmizadeRepository;

        public AmizadeService() : base(new AmizadeRepository())
        {

        }
        public void Inserir(int IdUsuario, int IdAmigo)
        {
            base.Inserir(new Amizades()
            {
                IdAmigo = IdAmigo,
                IdUsuario = IdUsuario
            });
        }
        public void Remover(int IdUsuario, int IdAmigo)
        {
            base.Remover(x => x.IdAmigo == IdAmigo && x.IdUsuario == IdUsuario);
        }


        public List<Amizades> ObterAmigos()
        {
            int IdUsuario = UsuarioService.Atual.Id;
            return Repository.ObterAmizades(IdUsuario);

        }
        public Amizades ObterAmigo(int IdAmigo)
        {
            int IdUser = UsuarioService.Atual.Id;
            return Repository.ObterAmizades(IdUser).FirstOrDefault(x => x.IdAmigo == IdAmigo || x.IdUsuario == IdAmigo);

        }
        public List<Amizades> ObterSolicitacoesAmizade()
        {
            int userId = UsuarioService.Atual.Id;
            return Repository.ObterSolicitacoesAmizade(userId);

        }
        public Amizades ObterSolicitacaoAmizade(int idAmigo)
        {
            int UserId = UsuarioService.Atual.Id;
            return Repository.ObterSolicitacoesAmizade(UserId).FirstOrDefault(x => x.IdUsuario == idAmigo);
        }
    

        public void ResetarContadorAmizade()
        {
            int userId = UsuarioService.Atual.Id;
            Repository.ResetarContadorAmizade(userId);
        }

        public int ObterContadorSolicitacaoAmizades()
        {
            return Repository.ObterContadorSolicitacaoAmizades(UsuarioService.Atual.Id);
        }
        public void DesfazerAmizade(int[] ids)
        {
            int userId = UsuarioService.Atual.Id;
            Repository.DesfazerAmizade(userId, ids);
        }
        public void SolicitarAmizade(int idAmigo)
        {
            int userId = UsuarioService.Atual.Id;
            Repository.SolicitarAmizade(userId, idAmigo);
        }
        public void DesfazerSolicitacaoAmizade(int id)
        {
            int userId = UsuarioService.Atual.Id;
            Repository.DesfazerSolicitacaoAmizade(userId, id);
        }
        public void ConfirmarAmizade(List<int> IdUserSolicitacao)
        {
            int userId = UsuarioService.Atual.Id;
            Repository.ConfirmarAmizade(userId, IdUserSolicitacao);
        }
        public void RecusarAmizade(List<int> IdUserSolicitacao)
        {
            int iduser = UsuarioService.Atual.Id;
            Repository.RecusarAmizade(iduser, IdUserSolicitacao);
        }
    }
}
