﻿using HomeDBEntity;
using HomeDBEntity.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService
{
    public class BugService : GenericService<Bugs>
    {
        protected new BugRepository Repository => base.Repository as BugRepository;
        public BugService() : base(new BugRepository())
        {

        }

        public void RelatarBug(Exception exception)
        {
            Repository.RelatarBug(exception);
        }

        public List<Bugs> Rank()
        {
            var data= Repository.ObterTodos();
            var soma = Repository.ObterQuantidade();
            Parallel.ForEach(data, (i) =>
             {
                 i.Porcentagem = (100/soma)*i.Qunatidade;
             });
            return data.OrderBy(x => x.Qunatidade).ToList();
        }
        public Bugs ObterPorCodigo(int codigo)
        {
            var data= Repository.ObterBug(codigo);
            data.Porcentagem = Repository.ObterQuantidade();
            return data;
        }


    }
}
