﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace HomeService
{
    public class GenericService<TEntity> : IDisposable
        where TEntity : class
    {
        protected GenericRepository<TEntity> Repository;
        public GenericService()
        {
            Repository = new GenericRepository<TEntity>();
        }

        public GenericService(GenericRepository<TEntity> repo)
        {
            Repository = repo;
        }
        ~GenericService()
        {
            Dispose();
        }

        public virtual void Inserir(TEntity entity)
        {
            Repository.Inserir(entity);
        }
        public virtual void Inserir(IEnumerable<TEntity> entities)
        {
            Repository.InserirLote(entities);
        }
        public virtual void Editar(TEntity entity)
        {
            Repository.Editar(entity);
        }
        public virtual void Remover(TEntity entity)
        {
            Repository.Remover(entity);
        }
        public virtual void Remover(Expression<Func<TEntity,bool>> expressaso)
        {
            Repository.Remover(expressaso);
        }
        public virtual void Remover(params TEntity[] entity)
        {
            Repository.RemoverLote(entity);
        }
        public virtual List<TEntity> Obter(Expression<Func<TEntity,bool>> expressao)
        {  
            return Repository.Obter(expressao);
        }

        public virtual TEntity ObterPrimeiro(Expression<Func<TEntity, bool>> expressao)
        {
            return Repository.ObterPrimeiro(expressao);
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        public List<TEntity> ObterTodos()
        {
            return Repository.ObterTodos();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Repository.Dispose();
                    Repository = null;
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        // ~GenericService() {
        //   // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
        //   Dispose(false);
        // }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
