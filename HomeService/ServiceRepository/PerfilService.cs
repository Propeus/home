﻿using HomeDBEntity;
using System.Linq;
using System;
using System.Collections.Generic;
using HomeDBEntity.Atributos;
using System.Transactions;
using System.Threading.Tasks;

namespace HomeService
{
    public class PerfilService : GenericService<Perfil>
    {

        protected new PerfilRepository Repository => base.Repository as PerfilRepository;

        public PerfilService() : base(new PerfilRepository())
        {

        }


        public override void Editar(Perfil perfil)
        {
            if (perfil.Valido)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    AtualizarAssociacao(perfil);
                    base.Editar(perfil);
                    scope.Complete();
                }

            }
        }

        public override void Inserir(Perfil perfil)
        {
            if (perfil.Valido)
            {

                Repository.nInserir(perfil);

            }
            else
            {
                throw new Exception("Perfil invalido");
            }
        }

        public override void Remover(Perfil perfil)
        {
            base.Remover(perfil);
        }
        public void CriarAssociar(Perfil perfil)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                Inserir(perfil);
                Associar(perfil);
            }

        }

        public Perfil ObterPorCodigo(int codigo)
        {
            PerfilRepository perfilRepo = new PerfilRepository();

            var result = perfilRepo.ObterPorCodigo(codigo);
            perfilRepo.Dispose();
            return result;
        }

        public bool ExisteAssociacao(Acesso acesso, Permissao permissao)
        {
            var result = Repository.ExisteAssociacaoPerfil(acesso, permissao);
            return result;
        }

        public Perfil ObterPorNome(string nome)
        {
            return Repository.ObterPorNome(nome);
        }
        public Perfil ObterPorAssociacao(Acesso acesso, Permissao permissao)
        {

            return Repository.ObterPorAssociacao(acesso, permissao);
        }
        //public List<Perfil> ObterTodos()
        //{
        //    return Repository.ObterTodos();
        //}

        public void Associar(Perfil perfil, Acesso acesso, Permissao permissao)
        {
            Repository.Associar(perfil, acesso, permissao);
        }
        /// <summary>
        /// Dia 19/08/2017: Quebra de regra de negocio
        /// </summary>
        /// <param name="perfil"></param>
        public void Associar(Perfil perfil)
        {

            Desassociar(perfil);
            foreach (var acesso in perfil.Acessos)
            {
                foreach (var permissao in acesso.Permissoes)
                {
                    Repository.Associar(perfil, acesso, permissao);
                }
            }
        }

        public void Desassociar(Acesso acesso, Permissao permissao)
        {
            Repository.Desassociar(acesso, permissao);
        }

        public void Desassociar(Perfil perfil)
        {

            Repository.Desassociar(perfil);
        }

        public void AtualizarAssociacao(Perfil p)
        {
            Associar(p);
        }

    }
}
