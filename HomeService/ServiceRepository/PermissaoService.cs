﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeService
{
    public class PermissaoService : GenericService<Permissao>
    {
        protected new PermissaoRepository Repository => base.Repository as PermissaoRepository;

        public PermissaoService() : base(new PermissaoRepository())
        {

        }

        public override void Editar(Permissao permissao)
        {
            if (permissao.Valido)
            {
                base.Editar(permissao);
            }
        }
        
        public override void Inserir(Permissao permissao)
        {
            if (permissao.Valido)
            {
                base.Editar(permissao);
            }
        }
        
        public override void Remover(Permissao permissao)
        {
            base.Remover(permissao);
        }

        public Permissao ObterPorCodigo(int codigo)
        {
           
            return Repository.ObterPorCodigo(codigo);
        }

        public List<Permissao> ObterPorCodigos(params int[] codigos)
        {
            return Repository.ObterPorCodigos(codigos);
        }

        public Permissao ObterPorNome(string nome)
        {
            return Repository.ObterPorNome(nome);
        }

        public List<Permissao> ObterPorNomes(params string[] nomes)
        {
            return Repository.ObterPorNomes(nomes);
        }

        public List<Permissao> ObterTodos()
        {
            return Repository.ObterTodos();
        }

        public List<Permissao> ObterPorAcesso(Acesso acesso)
        {
            return Repository.ObterPorAcesso(acesso);
        }
    }
}
