﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeDBEntity.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Transactions;
using System;
using System.Web;
using HomeDBEntity.Enum;
using HomeBase.Excecao;
using HomeBase;

namespace HomeService
{
    public partial class UsuarioService : GenericService<Usuario>
    {
        protected new UsuarioRepository Repository => base.Repository as UsuarioRepository;

        public UsuarioService() : base(new UsuarioRepository())
        {
        }

        public override void Inserir(Usuario usuario)
        {

            if (usuario.Valido)
            {

                //Criando hash da senha
                var r = HashAlgorithm.Create("SHA256").ComputeHash(Encoding.UTF8.GetBytes(usuario.Senha));
                StringBuilder sb = new StringBuilder();
                foreach (byte b in r)
                    sb.Append(b.ToString("X2"));


                usuario.Senha = sb.ToString();
                //Novo usuario

                Repository.Novo(usuario);
                //Associação a um perfil basico
                using (PerfilService perfilService = new PerfilService())
                {
                    var perfilBasico = perfilService.ObterPorCodigo(1);
                    AssociarPerfil(usuario, perfilBasico);

                }


            }
            else
            {
                throw new CJSONException(new HomeBase.CJSONMensagemModel
                {
                    status = HomeBase.eEstadoModel.ErrorUrl,
                    mensagem = new HomeBase.CMensagemModel
                    {
                        erro = new HomeBase.CErroModel
                        {
                            Mensagem = "Os dados são inválidos para cadastro"
                        }
                    }
                });
            }
        }
        public override void Remover(Usuario usuario)
        {
            Repository.Remover(usuario);
        }
        public void Remover(int Id)
        {
            using(TransactionScope scope = new TransactionScope())
            {
                using (AgendaService agendaservie = new AgendaService())
                {
                    agendaservie.Remover(x => x.IdUsuario == Id);
                }
                using (AmizadeService amizadeService = new AmizadeService())
                {
                    amizadeService.Remover(x => x.IdUsuario == Id || x.IdAmigo == Id);
                }
                Repository.Remover(x => x.Id == Id);
                scope.Complete();
            }
        
        }
        public override void Editar(Usuario usuario)
        {
            usuario.Senha = Repository.ObterPorId(usuario.Id).Senha;
            Repository.Editar(usuario);
        }


        public Usuario ObterUsuario(string login, string senha)
        {
            var r = HashAlgorithm.Create("SHA256").ComputeHash(Encoding.UTF8.GetBytes(senha));
            StringBuilder sb = new StringBuilder();
            foreach (byte b in r)
            {
                sb.Append(b.ToString("X2"));
            }
            senha = sb.ToString();

            Usuario usuario = Repository.ObterPorLogin(login, senha);
            return usuario;
        }
        public Usuario ObterPorId(int id)
        {
            return Repository.ObterPorId(id);
        }
        public List<Usuario> ObterPorIds(params int[] id)
        {
            return Repository.ObterPorIds(id);
        }

        public List<Usuario> ObterNaoAssociados(Perfil perfil)
        {
            return Repository.ObterNaoAssociados(perfil);
        }
        public void AssociarPerfil(Usuario usuario, Perfil perfil)
        {

            if (!Repository.Associado(usuario, perfil))
            {
                Repository.AssociarPerfil(usuario, perfil);
            }


        }
        public void DesassociarPerfil(Usuario usuario, Perfil perfil)
        {
            Repository.DesassociarPerfil(usuario, perfil);
        }
        public bool Associado(Usuario usuario, Perfil perfil)
        {

            var result = Repository.Associado(usuario, perfil);
            return result;
        }
        public static bool SessaoAtiva()
        {
            return Atual != null;
        }
        public static bool PossuiAcesso(string acesso)
        {
            string p = string.Empty;



            if (Atual == null)
            {
                return false;
            }

            if (!acesso.Contains("."))
            {
                var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

                if (routeValues.ContainsKey("controller"))
                    p = (string)routeValues["controller"];

                acesso = string.Format("{0}.{1}", p, acesso);
            }

            var AP = acesso.Split('.');

            return Atual.Acessos.Contains(acesso);
        }
        public static bool PossuiAcesso(eAcessoId acesso, ePermissaoId permissao)
        {
            return PossuiAcesso($"{acesso.ToString()}.{permissao.ToString()}");
        }
        public static bool PossuiAcesso(ePermissaoId permissao)
        {
            return PossuiAcesso(permissao.ToString());
        }
        public static Usuario Atual => (new SessaoService<Usuario>()).ObterSessao(Helper.UsuariosSessao);
        public static int ObterContadorSolicitacaoAmizade()
        {
            return (Atual.DetalheUsuario.ContadorSolicitacaoAmizade);
        }
        public static int ObterContadorAgendaCompartilhada()
        {
            return (Atual.DetalheUsuario.ContadorAgendaCompartilhada);
        }
    }
}
