﻿using HomeDBEntity;
using System.Linq;
using System.Web;

namespace HomeService
{
    public class SessaoService<T> where T : class
    {
        private HttpContextBase Context { get; set; }

        public SessaoService()
        {

        }
        public SessaoService(HttpContextBase context) => Context = context;

        public T ObterSessao(string nome)
        {
            if (Context != null)
            {
                var data = Context.Session[nome];
                if (data == null)
                    return default(T);
                else
                    return (T)data;
            }
            else
            {
                var data = HttpContext.Current.Session[nome];
                if (data == null)
                    return default(T);
                else
                    return (T)data;
            }

        }

        public void InserirSessao(T obj, string nome)
        {
            if (Context != null)
            {
                Context.Session.Add(nome, obj);
            }
            else
            {
                HttpContext.Current.Session.Add(nome, obj);
            }

        }

        public bool PossuiAcesso(string acesso)
        {


            string p = string.Empty;

            var User = (new SessaoService<Usuario>()).ObterSessao("User");

            if (User == null)
            {
                return false;
            }

            if (!acesso.Contains("."))
            {
                var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

                if (routeValues.ContainsKey("controller"))
                    p = (string)routeValues["controller"];

                acesso = string.Format("{0}.{1}", p, acesso);
            }

            return (User.Acessos.Contains(acesso));

        }

        public void LimparSessao(string sessao)
        {
            HttpContext.Current.Session.Remove(sessao);
        }

    }
}
