﻿using HomeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSignalService
{
    public class CJSONSignalRMensagemModel : CJSONSignalRModel
    {
        public CJSONSignalRMensagemModel() : base()
        {
            status = HomeBase.eEstadoModel.SignalRChat;
            Nome = $"Mensagem_SignalR";
        }

        public List<int> IdDestinatario { get; set; }
        public List<string> IdSignalRDestinatario { get; set; }

    }
}
