﻿using HomeBase;
using HomeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSignalService
{
    public class CJSONSignalRModel : CJSONConfiguracoesGlobaisModel
    {

        public CJSONSignalRModel()
        {
            status = eEstadoModel.Handshake;
            Nome = $"Handshake_Affinity";
        }

        public string HandshakeCode { get; internal set; }
        public double ExpiresCookie { get; set; } = DateTime.Now.AddDays(2).TotaOfDays();

        public int IdRementente { get; set; }
        public string IdSignalRRemetente { get; set; }
    }
}
