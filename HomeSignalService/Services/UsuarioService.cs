﻿using HomeDBEntity;
using HomeBase;
using HomeService;


namespace HomeSignalService
{
    public partial class UsuarioService : GenericService<Usuario>
    {

        public static CJSONSignalRModel SignalRAtual => (new SessaoService<CJSONSignalRModel>()).ObterSessao(HomeService.Helper.UsuariosSessao);

    }
}
