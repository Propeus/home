﻿using HomeBase;
using HomeService;
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HomeSignalService
{
    public class SignalRBase : Hub
    {
        public string ConexaoAtual { get { return Context.ConnectionId; } }
        public string UserNmae => Context.User.Identity.Name;

        public virtual void Send(string message)
        {


            Clients.All.broadcastMessage(message);

        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            return base.OnConnected();
        }
    }
}
