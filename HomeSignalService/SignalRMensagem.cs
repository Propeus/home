﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeDBEntity;
using HomeBase;

namespace HomeSignalService
{

    public class SignalRMensagem : SignalSessao
    {
        public SignalRMensagem() : base()
        {
        }

        public override void Send(string message)
        {

            message = ConfigureIdUser(message, typeof(CJSONSignalRMensagemModel));
            var data = message.FromJson<CJSONSignalRMensagemModel>();
            data.IdSignalRDestinatario = ObterSessaoSignal(data.IdDestinatario);
            if (data.IdSignalRDestinatario != null && data.IdSignalRDestinatario.Count > 0)
            {
                var listC = data.IdSignalRDestinatario;
                Clients.Clients(listC).broadcastMessage(message);
            }
            else
            {
                base.Send(data.ToJson());
            }
        }
    }
}
