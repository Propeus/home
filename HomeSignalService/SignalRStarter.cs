﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;
using HomeSignalService;

[assembly: OwinStartup(typeof(SignalRStarter))]
namespace HomeSignalService
{
    public partial class SignalRStarter
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}