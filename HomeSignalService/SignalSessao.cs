﻿using HomeDBEntity;
using HomeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using HomeBase;

namespace HomeSignalService
{
    public class SignalSessao : SignalRBase
    {
        public static Dictionary<int, string> Clientes { get; private set; } = new Dictionary<int, string>();

        public CJSONConfiguracoesGlobaisModel ConfiguracaoGlobal => (new CookieService()).ObterValorCookie<CJSONConfiguracoesGlobaisModel>(HomeService.Helper.UsuarioGlobalConfiguracao);
       

        public SignalSessao() : base()
        {

        }

        public string ConfigureIdUser(string message, Type T)
        {
            var data = message.FromJson(T) as CJSONSignalRModel;
            data.IdSignalRRemetente = Context.ConnectionId;
            data.IdRementente = ConfiguracaoGlobal.Id;
            return data.ToJson();
        }

        public override void Send(string message)
        {
            message = ConfigureIdUser(message, typeof(CJSONSignalRModel));
            base.Send(message);
        }

        public void CriarSessao(CJSONConfiguracoesGlobaisModel usuario)
        {
            if (Clientes.Count(x => x.Key == usuario.Id) > 0)
            {
                Clientes[usuario.Id] = ConexaoAtual;
            }
            else
            {
                Clientes.Add(usuario.Id, ConexaoAtual);

            }
        }

        public List<string> ObterSessaoSignal(List<int> IdUser)
        {
            return Clientes.Where(x => IdUser.Contains(x.Key)).Select(x => x.Value).ToList();
        }

        public override Task OnConnected()
        {

            var data = base.OnConnected();
            CriarSessao(ConfiguracaoGlobal);
            return data;
        }

    }
}