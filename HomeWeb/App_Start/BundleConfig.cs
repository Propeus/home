﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace HomeWeb
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/Basico").Include(
                 "~/Scripts/jquery-{version}.js",
                 "~/Scripts/jquery.cookie.js",
                 "~/Content/iziToast/iziToast.js",
                 "~/Scripts/semantic.js",
                 "~/Scripts/Base/CookieServiceScript.js",
                 "~/Scripts/Base/DoublyLinkedList.js",
                 "~/Scripts/Base/ModalScript.js",
                 "~/Scripts/Base/Base.js",
                 "~/Scripts/Base/PesquisaScript.js",
                 "~/Scripts/jquery.signalR-{version}.js",
                 "~/Scripts/vue.js"             
                 ));

    
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/semantic.css",
                      "~/Content/base.css",
                      "~/Content/iziToast/iziToast.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/Menu").Include(
                "~/Scripts/Menu/MenuScript.js",
                "~/Content/components/dropdown.js"));

            bundles.Add(new ScriptBundle("~/bundles/Perfil").Include(
                "~/Content/components/tab.js",
                "~/Scripts/Perfil/PerfilScript.js"));
        }
    }
}