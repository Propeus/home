﻿using HomeWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HomeWeb;
using HomeWeb.Models.Atributos;
using static HomeWeb.Helper;
using HomeService;
using HomeDBEntity.Enum;
using HomeDBEntity;
using HomeBase;
using HomeWeb.Models.Enum;

namespace HomeWeb.Controllers
{
    public class AgendaController : MasterController
    {
        [Permissao(eAcessoId.Agenda, ePermissaoId.Exibir)]
        public ActionResult Index()
        {
            List<CAgendaViewModel> model = new List<CAgendaViewModel>();
            using (AgendaService agendaService = new AgendaService())
            {
                model.AddRange(agendaService.ObterPorUsuario(UsuarioService.Atual).mToViewModel<CAgendaViewModel>());

            }

            var agendas = new CAgendasViewModel()
            {
                Agendas = model
            };
            return View(agendas);
        }

        [Permissao(eAcessoId.Agenda, ePermissaoId.Pesquisar)]
        public JsonResult Pesquisar(string Query)
        {
            var data = Query.Split('|');
            Query = data[0];
            eTipoPesquisa tipoPesquisa = (eTipoPesquisa)Convert.ToInt32(data[1]);

            switch (tipoPesquisa)
            {
                case eTipoPesquisa.Search:
                    using (AgendaService agendaservice = new AgendaService())
                    {
                        CPesquisasViewModel pesquisa = new CPesquisasViewModel
                        {
                            Objetos = agendaservice.ObterPorTitulo(Query).mToViewModel<CAgendaViewModel>().ToPesquisaViewModel<CPesquisaViewModel>(true),
                        };


                        var r = new CJSONMensagemModel
                        {
                            status = eEstadoModel.Pesquisa,
                            JSONModel = pesquisa

                        }.ToJsonResult();
                        return r;
                    }
                case eTipoPesquisa.Dropdown:
                    using (AmizadeService usuarioService = new AmizadeService())
                    {
                        var datavm = usuarioService.ObterAmigos().mToViewModel<CAmizadeViewModel>();
                        List<CUsuarioViewModel> r = new List<CUsuarioViewModel>();
                        foreach (var d in datavm)
                        {
                            if (d.IdUsuario == UsuarioService.Atual.Id)
                            {
                                r.Add(d.Usuario1.mToViewModel<CUsuarioViewModel>());
                            }
                            else
                            {
                                r.Add(d.Usuario.mToViewModel<CUsuarioViewModel>());
                            }
                        }
                        CPesquisasViewModel pesquisa = new CPesquisasViewModel
                        {
                            Objetos = r.ToPesquisaViewModel<CPesquisaViewModel>(true)
                        };

                        return new CJSONMensagemModel
                        {
                            status = eEstadoModel.Pesquisa,
                            JSONModel = pesquisa

                        }.ToJsonResult();
                    }
            }

            return new CJSONMensagemModel
            {
                url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                status = eEstadoModel.ErrorUrl,
                mensagem = new CMensagemModel
                {
                    erro = new CErroModel
                    {
                        Mensagem = "Houve um erro ao tentar pesquisar"
                    }
                }
            }.ToJsonResult();

        }

        [HttpGet]
        public JsonResult ModalCriarAgenda()
        {

            return new CJSONMensagemModel
            {
                view = this.RenderPartialView(),
                status = eEstadoModel.View,
                tipoView = eTipoView.Modal
            }.ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Agenda, ePermissaoId.Inserir)]
        public JsonResult ModalCriarAgenda(CAgendaViewModel model)
        {

            if (!model.DiaInteiro)
            {
                model.Fim = model.DataFim.AddHours(model.HoraFim.Hour).AddMinutes(model.HoraFim.Minute);
            }
            model.Inicio = model.DataInicio.AddHours(model.HoraInicio.Hour).AddMinutes(model.HoraInicio.Minute);
            model.IdUsuario = UsuarioService.Atual.Id;

            Agenda agenda = model.mToViewModel<Agenda>();

            if (model.postAmigos != null)
            {
                foreach (var idAmigo in model.postAmigos)
                {
                    agenda.AgendaCompartilhada.Add(new AgendaCompartilhada
                    {
                        IdUsuario = idAmigo
                    });
                }
            }


            try
            {
                using (AgendaService agendaService = new AgendaService())
                {
                    agendaService.Inserir(agenda);
                }

                return (new CJSONMensagemModel
                {
                    url = CViewModel.UrlAnterior,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = $"Sucesso ao criar o evento {agenda.NomeEvento}"

                        }
                    },
                    status = eEstadoModel.SucessoUrl
                }).ToJsonResult();
            }
            catch (Exception ex)
            {
                return (new CJSONMensagemModel
                {
                    url = CViewModel.UrlAnterior,
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel
                        {
                            Mensagem = ex.Message,
                            Titulo = "Erro ao criar um novo evento",
                            Stack = ex.StackTrace
                        }
                    },
                    status = eEstadoModel.ErrorUrl
                }).ToJsonResult();
            }


        }
        [HttpGet]
        [Permissao(eAcessoId.Agenda, ePermissaoId.Exibir)]
        public JsonResult ModalVisualizarAgenda(List<int> Id)
        {
            CAgendaViewModel agenda;
            using (AgendaService agendaService = new AgendaService())
            {
                var aux = agendaService.ObterPorId(Id.First());
                agenda = aux.mToViewModel<CAgendaViewModel>();
                //agenda.AgendaCompartilhada = aux.Usuario1.ToViewModel<CAgendaViewModel>();
                //using(AmizadeService amizadeService = new AmizadeService())
                //{
                //    agenda.Amigos = amizadeService.ObterAmigos().Where(x=> agenda.AgendaCompartilhada.Select(y => y.IdUsuario).Contains(x.IdAmigo)).ToViewModel<CAmizadeViewModel>();
                //}
                //agenda = new CAgendaViewModel
                //{
                //    NomeEvento = aux.NomeEvento,
                //    DataFim = aux.Fim.Value.Date,
                //    DataInicio = aux.Inicio.Date,
                //    Local = aux.Local,
                //    Descricao = aux.Descricao,
                //    DiaInteiro = aux.DiaInteiro,
                //    Lembrete = aux.Lembrete.Value,
                //    Inicio = aux.Inicio,
                //    Fim = aux.Fim,
                //    IdUsuario = aux.IdUsuario,
                //    Amigos = aux.AgendaCompartilhada.Select(x => new CAmizadeViewModel
                //    {
                //        Usuario = x.Usuario.ToViewModel<CUsuarioViewModel>(),

                //        IdAmigo = x.IdUsuario,
                //        IdUsuario = UsuarioService.Atual.Id,

                //    }).ToList()
                //};
            }
            return new CJSONMensagemModel
            {
                view = this.RenderPartialView(model: agenda),
                status = eEstadoModel.View,
                tipoView = eTipoView.Modal
            }.ToJsonResult();
        }
        [HttpGet]
        public JsonResult ModalExcluirAgenda(List<int> Id)
        {
            List<CAgendaViewModel> agenda = new List<CAgendaViewModel>();
            using (AgendaService agendaService = new AgendaService())
            {
                foreach (var id in Id)
                {
                    agenda.Add(agendaService.ObterPorId(id).mToViewModel<CAgendaViewModel>());
                }
            }

            CAgendasViewModel model = new CAgendasViewModel
            {
                Agendas = agenda
            };

            return new CJSONMensagemModel
            {
                view = this.RenderPartialView(model: model),
                status = eEstadoModel.View,
                tipoView = eTipoView.Modal
            }.ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Agenda, ePermissaoId.Excluir)]
        public JsonResult ModalExcluirAgenda(CAgendasViewModel model)
        {

            try
            {
                using (AgendaService agendaService = new AgendaService())
                {
                    agendaService.Remover(x => x.IdUsuario == UsuarioService.Atual.Id && model.Ids.Contains(x.Id));
                    var dataAgenda = agendaService.ObterPorIds(model.Ids.ToArray()).Where(x => x.IdUsuario != UsuarioService.Atual.Id && model.Ids.Contains(x.Id));
                    agendaService.Descompartilhar(dataAgenda.Select(x => x.Id).ToArray());
                }

                return (new CJSONMensagemModel
                {
                    url = CViewModel.UrlAnterior,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = "Sucesso ao excluir o evento.",
                        }
                    },
                    status = eEstadoModel.SucessoUrl
                }).ToJsonResult();
            }
            catch (Exception ex)
            {
                return (new CJSONMensagemModel
                {
                    url = CViewModel.UrlAnterior,
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel
                        {
                            Mensagem = ex.Message,
                            Titulo = "Erro ao excluir um evento",
                            Stack = ex.StackTrace
                        }
                    },
                    status = eEstadoModel.ErrorUrl
                }).ToJsonResult();
            }

        }
        [HttpGet]
        public JsonResult ModalEditarAgenda(List<int> Id)
        {
            CAgendaViewModel agenda;
            using (AgendaService agendaService = new AgendaService())
            {
                agenda = agendaService.ObterPorId(Id.First()).mToViewModel<CAgendaViewModel>();
            }
            if (UsuarioService.Atual.Id != agenda.IdUsuario)
            {
                return new CJSONMensagemModel
                {
                    view = this.RenderPartialView(model: agenda),
                    status = eEstadoModel.Error,
                    tipoView = eTipoView.Partial,
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel
                        {
                            Titulo = "Acesso negado",
                            Mensagem = "Você não possui permissão para editar este evento."
                        }
                    }
                }.ToJsonResult();
            }
            CPesquisasViewModel pesquisasViewModel = new CPesquisasViewModel()
            {
                Objetos = agenda.AgendaCompartilhada.Select(x => x.Usuario).ToPesquisaViewModel<CPesquisaViewModel>(),
                tipoPesquisa = eTipoPesquisa.Dropdown
            };

            return new CJSONMensagemModel
            {
                view = this.RenderPartialView(model: agenda),
                status = eEstadoModel.View,
                tipoView = eTipoView.Modal,
                JSONModel = pesquisasViewModel
            }.ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Agenda, ePermissaoId.Editar)]
        public JsonResult ModalEditarAgenda(CAgendaViewModel model)
        {

            if (!model.DiaInteiro)
            {
                model.Fim = model.DataFim.AddHours(model.HoraFim.Hour).AddMinutes(model.HoraFim.Minute);
            }
            model.Inicio = model.DataInicio.AddHours(model.HoraInicio.Hour).AddMinutes(model.HoraInicio.Minute);
            model.IdUsuario = UsuarioService.Atual.Id;

            Agenda agenda = model.mToViewModel<Agenda>();
            if (model.postAmigos != null)
            {
                foreach (var idAmigo in model.postAmigos)
                {
                    agenda.AgendaCompartilhada.Add(new AgendaCompartilhada
                    {
                        IdAgenda = agenda.Id,
                        IdUsuario = idAmigo
                    });
                }
            }
            try
            {
                using (AgendaService agendaService = new AgendaService())
                {
                    agendaService.Editar(agenda);

                }

                return (new CJSONMensagemModel
                {
                    url = CViewModel.UrlAnterior,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = $"Sucesso ao editar o evento {agenda.NomeEvento}",

                        }
                    },
                    status = eEstadoModel.SucessoUrl
                }).ToJsonResult();
            }
            catch (Exception ex)
            {
                return (new CJSONMensagemModel
                {
                    url = CViewModel.UrlAnterior,
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel
                        {
                            Mensagem = ex.Message,
                            Titulo = "Erro ao criar um novo evento",
                            Stack = ex.StackTrace
                        }
                    },
                    status = eEstadoModel.ErrorUrl
                }).ToJsonResult();
            }


        }
    }
}