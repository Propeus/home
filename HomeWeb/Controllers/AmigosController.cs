﻿using HomeBase;
using HomeDBEntity;
using HomeDBEntity.Enum;
using HomeService;
using HomeWeb.Models.Atributos;
using HomeWeb.Models.Enum;
using HomeWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeWeb.Controllers
{
    public class AmigosController : MasterController
    {
        // GET: Amigo
        [Permissao(eAcessoId.Amigos, ePermissaoId.Gerenciar)]
        public ActionResult Index()
        {
            CAmizadesViewModel Amigos = new CAmizadesViewModel();
            using (AmizadeService amizadeService = new AmizadeService())
            {
                Amigos.Amizades = amizadeService.ObterAmigos().mToViewModel<CAmizadeViewModel>();
            }
            return this.RenderView(model: Amigos);
        }

        [Permissao(eAcessoId.Amigos, ePermissaoId.Pesquisar)]
        public JsonResult Pesquisar(string Query)
        {

            var data = Query.Split('|');
            Query = data[0];
            eTipoPesquisa tipoPesquisa = (eTipoPesquisa)Convert.ToInt32(data[1]);

            switch (tipoPesquisa)
            {
                case eTipoPesquisa.Search:
                    using (UsuarioService usuarioService = new UsuarioService())
                    {
                        CPesquisasViewModel pesquisa = new CPesquisasViewModel
                        {
                            Objetos = usuarioService.Obter(x => x.Usuario1.Contains(Query) && x.Id != UsuarioService.Atual.Id).mToViewModel<CUsuarioViewModel>().ToPesquisaViewModel<CPesquisaViewModel>(true),
                        };


                        var r = new CJSONMensagemModel
                        {
                            status = eEstadoModel.Pesquisa,
                            JSONModel = pesquisa

                        }.ToJsonResult();
                        return r;
                    }
                case eTipoPesquisa.Dropdown:
                    break;
            }

            return new CJSONMensagemModel
            {
                url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                status = eEstadoModel.ErrorUrl,
                mensagem = new CMensagemModel
                {
                    erro = new CErroModel
                    {
                        Mensagem = "Houve um erro ao tentar pesquisar"
                    }
                }
            }.ToJsonResult();

            
        }

        [HttpGet]
        public ActionResult VisualizaSolicitacaoAmizade()
        {
            CAmizadesViewModel model = new CAmizadesViewModel();

            using (AmizadeService amizadeService = new AmizadeService())
            {
                model.Amizades = amizadeService.ObterSolicitacoesAmizade().mToViewModel<CAmizadeViewModel>();
                if (UsuarioService.Atual.DetalheUsuario.ContadorSolicitacaoAmizade > 0)
                {
                    amizadeService.ResetarContadorAmizade();
                    var data = UsuarioService.Atual;
                    data.DetalheUsuario.ContadorSolicitacaoAmizade = 0;
                    (new SessaoService<Usuario>()).InserirSessao(data, "User");
                }
            }
            return this.RenderView(model: model);
        }

        [Permissao(eAcessoId.Amigos, ePermissaoId.Exibir)]
        public JsonResult ModalVisualizar(int id)
        {

            CAmizadeViewModel model;
            using (AmizadeService amizadeService = new AmizadeService())
            {
                model = amizadeService.ObterAmigo(id)?.mToViewModel<CAmizadeViewModel>();
            }
            if (model == null)
            {
                model = new CAmizadeViewModel();
                using (UsuarioService amizadeService = new UsuarioService())
                {
                    model.Usuario = amizadeService.ObterPorId(id).mToViewModel<CUsuarioViewModel>();
                }
            }

            return new CJSONModel
            {
                status = eEstadoModel.View,
                view = this.RenderPartialView(model: model)
            }.ToJsonResult();
        }
        [HttpGet]
        public JsonResult ModalExcluir(List<int> Id)
        {
            CUsuariosViewModel amizade = new CUsuariosViewModel();
            using (UsuarioService usuarioService = new UsuarioService())
            {
                amizade.Usuarios = usuarioService.ObterPorIds(Id.ToArray()).mToViewModel<CUsuarioViewModel>();
            }
            return new CJSONModel
            {
                status = eEstadoModel.View,
                view = this.RenderPartialView(model: amizade)
            }.ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Excluir)]
        public JsonResult ModalExcluir(CAmizadesViewModel model)
        {
          
            using (AmizadeService amizadeService = new AmizadeService())
            {
                amizadeService.DesfazerAmizade(model.Id.ToArray());
            }
            return new CJSONMensagemModel
            {
                status = eEstadoModel.SucessoUrl,
                url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                mensagem = new CMensagemModel
                {
                    sucesso = new CSucessoModel
                    {
                        Mensagem = "Sucesso ao desfazer a(s) amizade(s)."
                    }
                }
            }.ToJsonResult();
        }
        [HttpGet]
        public JsonResult ModalCriar()
        {

            return new CJSONModel
            {
                status = eEstadoModel.View,
                view = this.RenderPartialView()
            }.ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Excluir)]
        public JsonResult ModalCriar(CAmizadesViewModel model)
        {
            CUsuariosViewModel amizade = new CUsuariosViewModel();
            using (AmizadeService amizadeService = new AmizadeService())
            {
                amizadeService.DesfazerAmizade(model.Id.ToArray());
            }
            return new CJSONMensagemModel
            {
                status = eEstadoModel.Sucesso,
                mensagem = new CMensagemModel
                {
                    sucesso = new CSucessoModel
                    {
                        Mensagem = "Sucesso ao desfazer a(s) amizade(s)."
                    }
                }
            }.ToJsonResult();
        }




        [HttpPost]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Inserir)]
        public JsonResult SolicitarAmizade(int id)
        {
            using (AmizadeService amizadeService = new AmizadeService())
            {
                amizadeService.SolicitarAmizade(id);
                return new CJSONMensagemModel
                {
                    status = eEstadoModel.Sucesso,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = "Sucesso ao solicitar amizade!"
                        }
                    }
                }.ToJsonResult();
            }
        }
        [HttpPost]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Inserir)]
        public JsonResult DesfazerSolicitacaoAmizade(int id)
        {
            using (AmizadeService amizadeService = new AmizadeService())
            {
                amizadeService.DesfazerSolicitacaoAmizade(id);
                return new CJSONMensagemModel
                {
                    status = eEstadoModel.Sucesso,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = "Solicitação desfeita!"
                        }
                    }
                }.ToJsonResult();
            }
        }
        [HttpGet]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Editar)]
        public JsonResult ModalConfirmarAmizade(List<int> Id)
        {
            using (UsuarioService amizadeService = new UsuarioService())
            {
                var model = new CUsuariosViewModel
                {
                    Usuarios = amizadeService.ObterPorIds(Id.ToArray()).mToViewModel<CUsuarioViewModel>()
                };
                return new CJSONMensagemModel
                {
                    status = eEstadoModel.View,
                    view = this.RenderPartialView(model: model)
                }.ToJsonResult();
            }
        }
        [HttpPost]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Editar)]
        public JsonResult ModalConfirmarAmizade(CAmizadesViewModel model)
        {
            using (AmizadeService amizadeService = new AmizadeService())
            {
                amizadeService.ConfirmarAmizade(model.Id);
                return new CJSONMensagemModel
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = "Sucesso ao confirmar amizade!"
                        }
                    }
                }.ToJsonResult();
            }
        }
        [HttpGet]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Excluir)]
        public JsonResult ModalRecusarAmizade(List<int> Id)
        {
            using (UsuarioService amizadeService = new UsuarioService())
            {
                var model = new CUsuariosViewModel
                {
                    Usuarios = amizadeService.ObterPorIds(Id.ToArray()).mToViewModel<CUsuarioViewModel>()
                };
                return new CJSONMensagemModel
                {
                    status = eEstadoModel.View,
                    view = this.RenderPartialView(model: model)
                }.ToJsonResult();
            }
        }
        [HttpPost]
        [Permissao(eAcessoId.Amigos, ePermissaoId.Excluir)]
        public JsonResult ModalRecusarAmizade(CAmizadesViewModel model)
        {
            using (AmizadeService amizadeService = new AmizadeService())
            {
                amizadeService.RecusarAmizade(model.Id);
                return new CJSONMensagemModel
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel
                        {
                            Mensagem = "Sucesso ao recusar amizade!"
                        }
                    }
                }.ToJsonResult();
            }
        }
    }
}