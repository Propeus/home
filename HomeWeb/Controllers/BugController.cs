﻿using HomeBase;
using HomeDBEntity.Enum;
using HomeService;
using HomeWeb.Models.Atributos;
using HomeWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeWeb.Controllers
{
    public class BugController : Controller
    {
        // GET: Bug
        public ActionResult Index()
        {

            CBugsViewModel model = new CBugsViewModel();
            using (BugService bugService = new BugService())
            {
                model.Bugs = bugService.Rank().mToViewModel<CBugViewModel>();
            }
            return View(model);
        }

        [HttpGet]
        [Permissao(eAcessoId.Bug, ePermissaoId.Excluir)]
        public JsonResult ModalResolverBug(int cod)
        {

            CBugViewModel model = null;
            using (BugService bugService = new BugService())
            {
                model = bugService.ObterPorCodigo(cod).ToViewModel<CBugViewModel>();
            }

            return new CJSONMensagemModel
            {
                status = eEstadoModel.View,
                tipoView = eTipoView.Modal,
                view = this.RenderPartialView(model: model)
            }.ToJsonResult();
        }

        [HttpPost]
        [Permissao(eAcessoId.Bug, ePermissaoId.Excluir)]
        public JsonResult ModalResolverBug(CBugViewModel model)
        {

            using (BugService bugService = new BugService())
            {
                bugService.Remover(x => x.CodigoBug == model.CodigoBug);
            }


            return new CJSONMensagemModel
            {
                url = CViewModel.UrlAnterior,
                status = eEstadoModel.SucessoUrl,
                mensagem = new CMensagemModel
                {
                    sucesso = new CSucessoModel
                    {
                        Mensagem = "Problema resolvido com sucesso!"
                    }
                }
            }.ToJsonResult();
        }
    }
}