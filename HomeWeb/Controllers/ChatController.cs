﻿using HomeDBEntity.Enum;
using HomeWeb.Models.Atributos;
using System.Web.Mvc;
using HomeBase;
using HomeService;

namespace HomeWeb.Controllers
{
    public class ChatController : MasterController
    {
        [Permissao(eAcessoId.Chat, ePermissaoId.Exibir)]
        public ActionResult Index()
        {
            
            return View();
        }

        [Permissao(eAcessoId.Chat,ePermissaoId.Inserir)]
        public JsonResult EnviarMensagem()
        {
            return new CJSONMensagemModel
            {
                status = eEstadoModel.Sucesso,

            }.ToJsonResult();
        }
    }
}