﻿using HomeDBEntity;
using HomeDBEntity.Enum;
using HomeService;
using HomeWeb.Models.Atributos;
using HomeWeb.Models.ViewModel;
using System.Web.Mvc;

namespace HomeWeb.Controllers
{
    public class ConfiguracaoController : MasterController
    {
        [Permissao(eAcessoId.Configuracao, ePermissaoId.Exibir)]
        public ActionResult Configuracoes()
        {
            CUsuarioViewModel cuvm = UsuarioService.Atual.mToViewModel<CUsuarioViewModel>();

            return View(cuvm);
        }

    }


}
