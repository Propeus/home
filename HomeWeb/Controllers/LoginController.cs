﻿using HomeDBEntity;
using HomeService;
using HomeWeb.Models.ViewModel;
using System;
using System.Web.Mvc;
using HomeWeb.Models.Atributos;
namespace HomeWeb.Controllers
{
    public class LoginController : MasterController
    {
        [HttpGet]
        [IgnoreVerificacaoSessao]
        public ActionResult Login()
        {
            var agenda = new Agenda();
            if ((new SessaoService<Usuario>()).ObterSessao("User") == null)
                return this.RenderView();
            else
                return RedirectToAction("Index", "Painel");
        }


        [HttpPost]
        [IgnoreVerificacaoSessao]
        public JsonResult Login(CLoginViewModel loginViewModel)
        {
            Usuario user;
            

            using (UsuarioService usuarioService = new UsuarioService())
            {
                user = usuarioService.ObterUsuario(loginViewModel.Login, loginViewModel.Senha);
               
            }
          
            if (user != null)
            {
                (new SessaoService<Usuario>()).InserirSessao(user, HomeService.Helper.UsuariosSessao);
                (new CookieService()).EnviarConfiguracoesGlobais();

                var jr = new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    url = Url.Action("Index", "Painel"),
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel
                        {
                            Mensagem = string.Format("Bem-vindo! {0}", user.Usuario1),
                            Titulo = "Sucesso!",
                        }
                    }
                };

                return jr.ToJsonResult();

            }
            else
            {
                var jr = new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel
                        {
                            Mensagem = string.Format("Login ou senha incorreto(s)"),
                            Titulo = "Tente novamente!",
                            ExibirStackTrace = false
                        }
                    }
                };
                return jr.ToJsonResult();

            }
        }

        [HttpGet]
        [IgnoreVerificacaoSessao]
        public ActionResult Cadastro()
        {
            return this.RenderView();
        }

        [HttpPost]
        [IgnoreVerificacaoSessao]
        public JsonResult Cadastro(CCadastroViewModel cadastroViewModel)
        {
            Usuario usuario = cadastroViewModel.mToViewModel<Usuario>();
            try
            {
                using (UsuarioService usuarioservice = new UsuarioService())
                {
                    usuarioservice.Inserir(usuario);


#if !DEBUG
                (new EmailService()).EnviarEmail(usuario.E_mail, "Cadastro", "Cadastro realizado com sucesso!");
#endif
                    (new SessaoService<Usuario>()).InserirSessao(usuarioservice.ObterUsuario(cadastroViewModel.Usuario1,cadastroViewModel.Senha), HomeService.Helper.UsuariosSessao);
                    (new CookieService()).EnviarConfiguracoesGlobais();
                }
                return new CJsonViewModel
                {
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = string.Format("Cadastro realizado com sucesso!\nVoce será redirecionado para o painel."),
                            Titulo = "Sucesso!"
                        }
                    },
                    url = Url.Action("Index", "Painel"),
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl
                }.ToJsonResult();

            }
            catch (Exception ex)
            {
                return new CJsonViewModel
                {
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel()
                        {
                            Mensagem = string.Format(ex.Message),
                            Titulo = "Houve um erro ao tentar cadastrar",
                            Stack = ex.StackTrace,
                        }
                    },
                    status = HomeService.Enum.eTipoMensagem.Error
                }.ToJsonResult();
            }

        }

        [HttpGet]
        [IgnoreVerificacaoSessao]
        public ActionResult Recuperar()
        {
            return this.RenderView();
        }

        [HttpPost]
        [IgnoreVerificacaoSessao]
        public ActionResult Recuperar(CRecuperarViewModel login)
        {
            throw new NotImplementedException();
        }

        [IgnoreVerificacaoSessao]
        public ActionResult Logout()
        {
            (new SessaoService<Usuario>()).LimparSessao("User");
            return RedirectToAction("Login");
        }
    }
}