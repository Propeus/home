﻿using HomeDBEntity;
using HomeService;
using HomeWeb.Models.Atributos;
using HomeWeb.Models.ViewModel;
using System.Web.Mvc;
using System.Linq;
using HomeBase;
using HomeBase.Excecao;
using HomeDBEntity.Enum;

namespace HomeWeb.Controllers
{
    public class MasterController : Controller
    {


#if DEBUG
        [Permissao(eAcessoId.Painel, ePermissaoId.SQL)]
        public string SQL(string Query)
        {
            using (GenericRepository<object> generico = new GenericRepository<object>())
            {
                return generico.ComandoSQL(Query);
            }
        }
#endif

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var ignore = (filterContext.ActionDescriptor.GetCustomAttributes(typeof(IgnoreVerificacaoSessaoAttribute), false)).Length == 0
                ? false :
                (filterContext.ActionDescriptor.GetCustomAttributes(typeof(IgnoreVerificacaoSessaoAttribute), false)[0] as IgnoreVerificacaoSessaoAttribute).Ignore;

            if (ignore)
                return;

            string p = string.Empty;

            var User = UsuarioService.Atual;

            if (User == null)
            {
                if (CViewModel.UrlAnterior == null)
                {
                    var jr = new CJSONMensagemModel
                    {
                        status = eEstadoModel.Url,
                        url = UrlHelper.GenerateContentUrl("/Login/Login", filterContext.HttpContext)
                    };
                    throw new CJSONMensagemException(jr);

                }
                else
                {
                    var jr = new CJSONMensagemModel
                    {
                        mensagem = new CMensagemModel
                        {
                            erro = new CErroModel()
                            {
                                Mensagem = "A sessao expirou logue novamente",
                                Titulo = "Erro"
                            },

                        },
                        status = eEstadoModel.ErrorUrl,
                        url = UrlHelper.GenerateContentUrl("/Login/Login", filterContext.HttpContext)
                    };
                    throw new CJSONPermissaoException(jr);

                }


            }

            var permissao = (filterContext.ActionDescriptor.GetCustomAttributes(typeof(PermissaoAttribute), false)).Length == 0
                ? string.Empty :
                (filterContext.ActionDescriptor.GetCustomAttributes(typeof(PermissaoAttribute), false)[0] as PermissaoAttribute).PermissaoCod;

            if (permissao == string.Empty)
            {
                return;
            }

            //    var permissao = (filterContext.ActionDescriptor.GetCustomAttributes(typeof(PermissaoAttribute), false)[0] as PermissaoAttribute).PermissaoCod;
            if (!permissao.Contains("."))
            {
                var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                var action = filterContext.ActionDescriptor.ActionName;

                permissao = string.Format("{0}.{1}", controller, action);
            }

            if (User.Acessos.Contains(permissao))
            {
                return;
            }

            var AP = permissao.Split('.');

            var As = (new AcessoService()).ObterPorNome(AP[0]);
            var Pes = (new PermissaoService()).ObterPorNome(AP[1]);

            if (As == null || Pes == null)
            {
#if DEBUG
                if (User.Perfis.Count(x => x.Codigo == 2) > 0)
                {
                    AcessoService acessoService = new AcessoService();
                    PermissaoService permissaoService = new PermissaoService();
                    PerfilService perfilService = new PerfilService();
                    if (As == null)
                    {
                        acessoService.Inserir(new Acesso()
                        {
                            Nome = AP[0]
                        });
                    }
                    if (Pes == null)
                    {
                        permissaoService.Inserir(new Permissao()
                        {
                            Nome = AP[1]
                        });
                    }
                    As = acessoService.ObterPorNome(AP[0]);
                    Pes = permissaoService.ObterPorNome(AP[1]);

                    perfilService.Associar(User.Perfis.First(x => x.Codigo == 2), As, Pes);


                    var a = new CJSONMensagemModel
                    {
                        mensagem = new CMensagemModel
                        {
                            sucesso = new CSucessoModel()
                            {
                                Mensagem = "Acesso cadastrado! " + permissao + " Acesse novamete.",
                                Titulo = "Cadastro"
                            },

                        },
                        status = eEstadoModel.SucessoUrl,
                        url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)
                    };

                    filterContext.Redrect(a);

                }
#endif

                var jr = new CJSONMensagemModel
                {
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel()
                        {
                            Mensagem = $"Acesso não cadastrado. {permissao}",
                            Titulo = "Erro no sistema"
                        }
                    },
                    url = UrlHelper.GenerateContentUrl("/Painel/Index", filterContext.HttpContext)
                };
                throw new CJSONMensagemException(jr);
            }

            var Perfil = (new PerfilService()).ObterPorAssociacao(As, Pes);

            if (Perfil == null)
            {
                var jr = new CJSONMensagemModel
                {
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel()
                        {
                            Mensagem = "O Acesso não esta vinculado a nenhum perfil",
                            Titulo = "Erro no sistema"
                        }
                    },
                    status = eEstadoModel.ErrorUrl,
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)
                };
                throw new CJSONMensagemException(jr);
            }


            if (User.Perfis.Count(x => x.Codigo == Perfil.Codigo) == 0)
            {
                throw new CJSONPermissaoException(permissao, UsuarioService.Atual.Nome);
            }

            base.OnActionExecuting(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {

            var jr = filterContext.Exception as CJSONMensagemException;
            if (jr == null)
            {
                var r = new CJSONMensagemModel
                {
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel
                        {
                            Mensagem = filterContext.Exception.Message,
                            Stack = filterContext.Exception.StackTrace
                        }
                    },
                    status = eEstadoModel.ErrorUrl
                };
                jr = new CJSONMensagemException(r, CViewModel.Contexto);
            }


            if (jr.Excecao.status == eEstadoModel.Error || jr.Excecao.status == eEstadoModel.ErrorUrl)
            {
                using (BugService bugService = new BugService())
                {
                    bugService.RelatarBug(jr);
                }
            }


            if (filterContext.HttpContext.Request.IsAjaxRequest())
                filterContext.HttpContext.Response.Write(jr.Excecao.ToJson());
            else
            {
                (new CookieService()).EnviarJSONModel(jr.Excecao);
                filterContext.HttpContext.Response.Redirect(jr.Excecao.url, true);
            }

            filterContext.ExceptionHandled = true;
        }

    }
}