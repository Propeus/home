﻿using HomeDBEntity.Enum;
using HomeWeb.Models.Atributos;
using System.Web.Mvc;

namespace HomeWeb.Controllers
{
    public class PainelController : MasterController
    {
        [HttpGet]
        [Permissao(eAcessoId.Painel, ePermissaoId.Gerenciar)]
        public ActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult Configuracoes()
        //{
        //    return RedirectToAction("Index", "Configuracoes");
        //}
    }
}