﻿using HomeDBEntity;
using HomeService;
using HomeWeb.Models.Atributos;
using System.Linq;
using System.Web.Mvc;
using System;
using HomeWeb.Models.ViewModel;
using static HomeWeb.Helper;
using System.Threading.Tasks;
using System.Collections.Generic;
using HomeDBEntity.Helper;
using System.Text;
using HomeDBEntity.Enum;

namespace HomeWeb.Controllers
{
    public class PerfilController : MasterController
    {
        [Permissao(eAcessoId.Perfil, ePermissaoId.Exibir)]
        public ActionResult Perfis()
        {
            if (UsuarioService.PossuiAcesso("Perfil.Gerenciar"))
                return View(ObterPerfis());
            else
                return View(ObterPerfisUsuario());
        }

        #region Grids
        [HttpGet]
        public PartialViewResult GridPerfil(CPerfisViewModel model)
        {
            return PartialView(model);
        }
        [HttpGet]
        public PartialViewResult GridAcesso(CPerfisViewModel model)
        {
            return PartialView(model);
        }
        [HttpGet]
        public PartialViewResult GridPermissao(CPerfisViewModel model)
        {
            return PartialView(model);
        }
        #endregion


        #region Modal

        #region Perfil

        #region Criar
        [HttpGet]
        public JsonResult ModalCriarPerfil()
        {
            List<CPermissaoViewModel> listaPermissao;
            List<CAcessoViewModel> listaAcesso;

            using (PermissaoService permissaoService = new PermissaoService())
            {
                listaPermissao = permissaoService.ObterTodos().Select(x => new CPermissaoViewModel
                {
                    Codigo = x.Codigo,
                    Descricao = x.Descricao,
                    Nome = x.Nome
                }).ToList();
            }


            using (AcessoService acessoService = new AcessoService())
            {
                listaAcesso = acessoService.ObterTodos().Select(x => new CAcessoViewModel
                {
                    Codigo = x.Codigo,
                    Descricao = x.Descricao,
                    Nome = x.Nome,
                    Permissoes = listaPermissao
                }).ToList();
            }

            if (listaAcesso.IsNullOrEmpty())
            {
                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        status = HomeService.Enum.eTipoMensagem.Error,
                        erro = new CErroViewModel()
                        {
                            Mensagem = "Não há acessos para associar a um novo perfil.",
                            Titulo = "Associação indisponivél."
                        }
                    }
                });
            }

            CPerfilViewModel perfilViewModel = new CPerfilViewModel()
            {
                Acessos = listaAcesso
            };

            return (new CJsonViewModel()
            {
                view = this.RenderPartialView(model: perfilViewModel),
                status = HomeService.Enum.eTipoMensagem.View
            }).ToJsonResult();

        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Inserir)]
        public JsonResult ModalCriarPerfil(CPerfilViewModel model)
        {
            if (model.AcessosPost.IsNullOrEmpty())
            {
                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        status = HomeService.Enum.eTipoMensagem.Error,
                        erro = new CErroViewModel()
                        {
                            Mensagem = "Não é possivel criar um novo perfil sem associar a um acesso",
                            Titulo = "Houve um erro ao criar um novo perfil",

                        }
                    }
                });
            }

            try
            {
                Perfil perfil = model.mToViewModel<Perfil>();
                perfil.AssociacaoPerfil = new List<AssociacaoPerfil>();

                using (AcessoService acessoService = new AcessoService())
                {
                    using (PermissaoService permissaoService = new PermissaoService())
                    {
                        var dataAcessos = acessoService.ObterPorNomes(model.AcessosPost.Select(x => x.Split('.')[0]).GroupBy(x => x).Select(x => x.First()).ToArray());
                        foreach (var acesso in dataAcessos)
                        {
                            var dataPermissao = permissaoService.ObterPorNomes(model.AcessosPost.Where(x => x.Contains(acesso.Nome)).Select(x => x.Split('.')[1]).GroupBy(x => x).Select(x => x.First()).ToArray());
                            foreach (var permissao in dataPermissao)
                            {
                                perfil.AssociacaoPerfil.Add(new AssociacaoPerfil
                                {
                                    CodigoAcesso = acesso.Codigo,
                                    CodigoPermissao = permissao.Codigo
                                });
                            }
                        }
                    }
                }

                using (PerfilService perfilService = new PerfilService())
                {
                    perfilService.Inserir(perfil);
                }

                //es.CriarAssociar(perfil);
                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = $"Sucesso ao criar o perfil {model.Nome}"
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                });
            }
            catch (Exception ex)
            {

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.ErrorUrl,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel
                        {
                            Mensagem = $"Erro ao criar o perfil {model.Nome}",
                            Stack = ex.StackTrace,
                            Titulo = "Erro"
                        },
                        status = HomeService.Enum.eTipoMensagem.ErrorUrl
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),

                });
            }

        }



        #endregion

        #region Editar
        [HttpGet]
        public JsonResult ModalEditarPerfil(int cod)
        {

            var pR = new PerfilRepository();
            Perfil perfil;

            using (PerfilRepository perfilrepository = new PerfilRepository())
            {
                perfil = pR.ObterPorCodigo(cod);
            }

            List<CAcessoViewModel> acessos;
            using (AcessoService acessoService = new AcessoService())
            {
                acessos = acessoService.ObterTodos().mToViewModel<CAcessoViewModel>();
            }

            List<CPermissaoViewModel> permissoes;
            using (PermissaoService permissaoService = new PermissaoService())
            {
                permissoes = permissaoService.ObterTodos().mToViewModel<CPermissaoViewModel>();

            }

            foreach (var item in acessos)
            {
                item.Permissoes = permissoes;
            }

            List<string> lp = new List<string>();

            var ps = perfil.AssociacaoPerfil;
            foreach (var item in ps)
            {
                lp.Add($"{acessos.FirstOrDefault(x => x.Codigo == item.CodigoAcesso)?.Nome}.{permissoes.FirstOrDefault(x => x.Codigo == item.CodigoPermissao)?.Nome}");
            }

            CPerfilViewModel perVM = new CPerfilViewModel()
            {
                Codigo = perfil.Codigo,
                Nome = perfil.Nome,
                Acessos = acessos,
                Descricao = perfil.Descricao,
                AcessosPost = lp
            };

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: perVM),
                status = HomeService.Enum.eTipoMensagem.View,
            });

        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Editar)]
        public JsonResult ModalEditarPerfil(CPerfilViewModel model)
        {

            try
            {

                var acessosPost = model.AcessosPost?.ToArray();
                List<Acesso> acessos = new List<Acesso>();
                using (AcessoService As = new AcessoService())
                {
                    if (acessosPost != null)
                    {
                        using (PermissaoService Ps = new PermissaoService())
                        {
                            acessos = As.ObterPorNomes(acessosPost.Select(x => x.Split('.')[0]).ToArray());
                            Parallel.ForEach(acessos, (acesso) =>
                            {
                                acesso.Permissoes = Ps.ObterPorNomes(acessosPost.Where(x => x.Contains(acesso.Nome)).Select(x => x.Split('.')[1]).ToArray());
                            });
                        }
                    }
                }



                Perfil perfil = new Perfil()
                {
                    Acessos = acessos,
                    Codigo = model.Codigo,
                    Nome = model.Nome,
                    Descricao = model.Descricao
                };

                using (PerfilService es = new PerfilService())
                {
                    es.Editar(perfil);
                }

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = $"Sucesso ao editar o perfil {model.Nome}"
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                });
            }
            catch (Exception ex)
            {

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel()
                        {
                            Mensagem = ex.Message,
                            Titulo = $"Houve um erro ao editar o perfil {model.Nome}",
                            Stack = ex.StackTrace

                        }
                    }

                });
            }

        }
        #endregion

        #region Excluir
        [HttpGet]
        public JsonResult ModalExcluirPerfil(int cod)
        {
            CPerfilViewModel pvm;
            using (PerfilService perfilService = new PerfilService())
            {
                pvm = perfilService.ObterPorCodigo(cod).mToViewModel<CPerfilViewModel>();
            }
            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: pvm),
                status = HomeService.Enum.eTipoMensagem.View,
            });
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Excluir)]
        public JsonResult ModalExcluirPerfil(CPerfilViewModel model)
        {
            using (PerfilService Ps = new PerfilService())
            {
                Ps.Remover(model.mToViewModel<Perfil>());
            }

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                mensagem = new CMensagemViewModel
                {
                    sucesso = new CSucessoViewModel()
                    {
                        Mensagem = $"Sucesso ao excluir o perfil {model.Nome}"
                    }
                }

            });
        }
        #endregion

        #region Associar
        [HttpGet]
        public JsonResult ModalAssociarPerfil(int cod)
        {

            CPerfilViewModel pvm;
            Perfil perfil;
            using (PerfilRepository pR = new PerfilRepository())
            {
                perfil = pR.ObterPorCodigo(cod);
                pvm = perfil.mToViewModel<CPerfilViewModel>();
            }
            using (UsuarioService usuarioService = new UsuarioService())
            {
                pvm.Usuarios = usuarioService.ObterTodos().mToViewModel<CUsuarioViewModel>();
                pvm.UsuariosPost = usuarioService.ObterNaoAssociados(perfil).mToViewModel<CUsuarioViewModel>().Select(x => x.Id).ToList();

            }
            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: pvm),
                status = HomeService.Enum.eTipoMensagem.View,
            });
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil, ePermissaoId.Editar)]
        public JsonResult ModalAssociarPerfil(CPerfilViewModel model)
        {

            Perfil perfil;
            using (PerfilService PerfilService = new PerfilService())
            {
                perfil = PerfilService.ObterPorCodigo(model.Codigo);
            }

            try
            {
                using (UsuarioService Usuarioservice = new UsuarioService())
                {
                    var u = Usuarioservice.ObterTodos().Where(x => !model.UsuariosPost.Contains(x.Id));

                    Parallel.ForEach(u, (user) =>
                    {
                        Usuarioservice.DesassociarPerfil(user, perfil);
                    });

                    Parallel.ForEach(model.UsuariosPost, (user) =>
                    {
                        Usuarioservice.AssociarPerfil(Usuarioservice.ObterPorId(user), perfil);
                    });
                }
                return new CJsonViewModel
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel
                        {
                            Mensagem = $"Sucesso ao associar os usuarios ao perfil {model.Nome}",
                            Titulo = "Sucesso!"
                        },
                        status = HomeService.Enum.eTipoMensagem.SucessoUrl
                    },
                }.ToJsonResult();
            }
            catch (Exception ex)
            {
                return new CJsonViewModel
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel
                        {
                            Mensagem = $"Erro ao tentar associar o perfil {model.Nome} ao usuarios.",
                            Stack = ex.StackTrace,
                            Titulo = "Erro no sistema."
                        }
                    }
                }.ToJsonResult();
            }
        }
        #endregion

        #endregion

        #region Acesso
        [HttpGet]
        public JsonResult ModalCriarAcesso()
        {

            return (new CJsonViewModel()
            {
                view = this.RenderPartialView(),
                status = HomeService.Enum.eTipoMensagem.View
            }).ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Inserir)]
        public JsonResult ModalCriarAcesso(CAcessoViewModel model)
        {



            Acesso acesso = new Acesso()
            {
                Codigo = model.Codigo,
                Nome = model.Nome,
                Descricao = model.Descricao
            };

            try
            {
                using (AcessoService es = new AcessoService())
                {
                    es.Inserir(acesso);
                }
                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = $"Sucesso ao criar o acesso {model.Nome}"
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                });
            }
            catch (Exception ex)
            {

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.ErrorUrl,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel
                        {
                            Mensagem = $"Erro ao criar o acesso {model.Nome}",
                            Stack = ex.StackTrace,
                            Titulo = "Erro"
                        },
                        status = HomeService.Enum.eTipoMensagem.ErrorUrl
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),

                });
            }
        }

        [HttpGet]
        public JsonResult ModalExcluirAcesso(int cod)
        {
            CAcessoViewModel pvm;
            using (AcessoService acessoService = new AcessoService())
            {
                pvm = acessoService.ObterPorCodigo(cod).mToViewModel<CAcessoViewModel>();
            }

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: pvm),
                status = HomeService.Enum.eTipoMensagem.View,
            });
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Excluir)]
        public JsonResult ModalExcluirAcesso(CAcessoViewModel model)
        {
            using (AcessoService acessoService = new AcessoService())
            {
                acessoService.Remover(model.mToViewModel<Acesso>());
            }

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                mensagem = new CMensagemViewModel
                {
                    sucesso = new CSucessoViewModel()
                    {
                        Mensagem = $"Sucesso ao excluir o acesso {model.Nome}"
                    }
                }

            });
        }

        [HttpGet]
        public JsonResult ModalEditarAcesso(int cod)
        {
            Acesso acesso;
            using (AcessoService acessoService = new AcessoService())
            {
                acesso = acessoService.ObterPorCodigo(cod);
            }

            CAcessoViewModel acessVM = acesso.mToViewModel<CAcessoViewModel>();

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: acessVM),
                status = HomeService.Enum.eTipoMensagem.View,
            });
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Editar)]
        public JsonResult ModalEditarAcesso(CAcessoViewModel model)
        {
            try
            {

                using (AcessoService As = new AcessoService())
                {
                    Acesso ac = model.mToViewModel<Acesso>();
                    As.Editar(ac);
                }

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = $"Sucesso ao editar o acesso {model.Nome}"
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                });
            }
            catch (Exception ex)
            {

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel()
                        {
                            Mensagem = ex.Message,
                            Titulo = $"Houve um erro ao editar o acesso {model.Nome}",
                            Stack = ex.StackTrace

                        }
                    }

                });
            }
        }

        #endregion

        #region Permissao
        [HttpGet]
        public JsonResult ModalCriarPermissao()
        {
            return (new CJsonViewModel()
            {
                view = this.RenderPartialView(),
                status = HomeService.Enum.eTipoMensagem.View
            }).ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Inserir)]
        public JsonResult ModalCriarPermissao(CPermissaoViewModel model)
        {


            Permissao acesso = new Permissao()
            {
                Codigo = model.Codigo,
                Nome = model.Nome,
                Descricao = model.Descricao
            };

            try
            {

                using (PermissaoService ps = new PermissaoService())
                {
                    ps.Inserir(acesso);
                }

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = $"Sucesso ao criar a permissao {model.Nome}"
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                });
            }
            catch (Exception ex)
            {

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.ErrorUrl,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel
                        {
                            Mensagem = $"Erro ao criar o acesso {model.Nome}",
                            Stack = ex.StackTrace,
                            Titulo = "Erro"
                        },
                        status = HomeService.Enum.eTipoMensagem.ErrorUrl
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),

                });
            }
        }

        [HttpGet]
        public JsonResult ModalExcluirPermissao(int cod)
        {
            CPermissaoViewModel pvm;

            using (PermissaoService pR = new PermissaoService())
            {
                pvm = pR.ObterPorCodigo(cod).mToViewModel<CPermissaoViewModel>();
            }
            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: pvm),
                status = HomeService.Enum.eTipoMensagem.View,
            });
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Excluir)]
        public JsonResult ModalExcluirPermissao(CPermissaoViewModel model)
        {
            using (PermissaoService Ps = new PermissaoService())
            {
                Ps.Remover(model.mToViewModel<Permissao>());
            }

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                mensagem = new CMensagemViewModel
                {
                    sucesso = new CSucessoViewModel()
                    {
                        Mensagem = $"Sucesso ao excluir a permissao {model.Nome}"
                    }
                }

            });
        }

        [HttpGet]
        public JsonResult ModalEditarPermissao(int cod)
        {
            Permissao permissao;
            using (PermissaoService pR = new PermissaoService())
            {
                permissao = pR.ObterPorCodigo(cod);
            }

            CPermissaoViewModel acessoViewModel = permissao.mToViewModel<CPermissaoViewModel>();

            return Helper.ToJsonResult(new CJsonViewModel()
            {
                view = Helper.RenderPartialView(this, model: acessoViewModel),
                status = HomeService.Enum.eTipoMensagem.View,
            });
        }
        [HttpPost]
        [Permissao(eAcessoId.Perfil,ePermissaoId.Editar)]
        public JsonResult ModalEditarPermissao(CPermissaoViewModel model)
        {
            try
            {
                Permissao ac = model.mToViewModel<Permissao>();

                using (PermissaoService As = new PermissaoService())
                {
                    As.Editar(ac);
                }

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.SucessoUrl,
                    mensagem = new CMensagemViewModel
                    {
                        sucesso = new CSucessoViewModel()
                        {
                            Mensagem = $"Sucesso ao editar a permissao {model.Nome}"
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                });
            }
            catch (Exception ex)
            {

                return Helper.ToJsonResult(new CJsonViewModel()
                {
                    status = HomeService.Enum.eTipoMensagem.Error,
                    mensagem = new CMensagemViewModel
                    {
                        erro = new CErroViewModel()
                        {
                            Mensagem = ex.Message,
                            Titulo = $"Houve um erro ao editar a permissao {model.Nome}",
                            Stack = ex.StackTrace

                        }
                    }

                });
            }
        }
        #endregion

        #endregion
        public CPerfisViewModel ObterPerfis()
        {

            CPerfisViewModel CVM;
            using (PerfilService perfilService = new PerfilService())
            {
                using (AcessoService acessoService = new AcessoService())
                {
                    using (PermissaoService permissaoService = new PermissaoService())
                    {
                        var acessos = acessoService.ObterTodos().mToViewModel<CAcessoViewModel>();
                        var permissoes = permissaoService.ObterTodos().mToViewModel<CPermissaoViewModel>();
                        var perfis = perfilService.ObterTodos().mToViewModel<CPerfilViewModel>();
                        CVM = new CPerfisViewModel()
                        {
                            Perfis = perfis,
                            Acessos = acessos,
                            Permissoes = permissoes,
                        };
                    }
                }

            }

            return CVM;
        }
        public CPerfisViewModel ObterPerfisUsuario()
        {


            List<Perfil> Perfis = UsuarioService.Atual.Perfis;
            CPerfisViewModel CVM;
            using (PerfilService perfilService = new PerfilService())
            {
                using (AcessoService acessoService = new AcessoService())
                {
                    using (PermissaoService permissaoService = new PermissaoService())
                    {
                        CVM = new CPerfisViewModel()
                        {
                            Perfis = Perfis.mToViewModel<CPerfilViewModel>(),
                            Acessos = acessoService.ObterPorCodigos(Perfis.SelectMany(x => x.AssociacaoPerfil).GroupBy(x => x.CodigoAcesso).Select(x => x.FirstOrDefault().CodigoAcesso).ToArray()).mToViewModel<CAcessoViewModel>(),
                            Permissoes = permissaoService.ObterPorCodigos(Perfis.SelectMany(x => x.AssociacaoPerfil).GroupBy(x => x.CodigoPermissao).Select(x => x.FirstOrDefault().CodigoPermissao).ToArray()).mToViewModel<CPermissaoViewModel>()
                        };

                    }

                }
            }
            return CVM;
        }

        public List<Acesso> ObterPorCodigodescricao(params string[] codDescricao)
        {
            List<Acesso> La = new List<Acesso>();

            using (AcessoService As = new AcessoService())
            {
                using (PermissaoService Ps = new PermissaoService())
                {
                    var acessosPost = codDescricao.Select(x => x.Split('.')[0]).Distinct().ToArray();
                    var acessos = As.ObterPorNomes(acessosPost);
                    Parallel.ForEach(acessos, (acesso) =>
                    {
                        var permissaoPost = codDescricao.Where(x => x.Split('.')[0].Contains(acesso.Nome)).Select(x => x.Split('.')[1]).ToArray();
                        var permissoes = Ps.ObterPorNomes(permissaoPost);

                        Parallel.ForEach(permissoes, (permissao) =>
                        {
                            if (acesso.Permissoes != null)
                            {
                                acesso.Permissoes.Add(permissao);
                            }
                            else
                            {
                                acesso.Permissoes = new List<Permissao>()
                                 {
                                permissao
                                  };
                            }
                        });

                        La.Add(acesso);
                    });
                }

            }



            return La;
        }
    }
}