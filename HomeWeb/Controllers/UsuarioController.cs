﻿using HomeBase;
using HomeDBEntity;
using HomeDBEntity.Enum;
using HomeService;
using HomeWeb.Models.Atributos;
using HomeWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HomeWeb.Controllers
{
    public class UsuarioController : MasterController
    {
        [Permissao(eAcessoId.Usuario,ePermissaoId.Exibir)]
        public ActionResult Usuarios()
        {
            return View(ObterUsuarios());
        }

        [HttpGet]
        public PartialViewResult GridUsuarios()
        {
            var model = ObterUsuarios();
            return PartialView(model);
        }

        [HttpGet]
        public JsonResult ModalCriarUsuario()
        {

            return (new CJSONMensagemModel()
            {
                view = this.RenderPartialView(),
                status = eEstadoModel.SucessoView,
                tipoView = eTipoView.Modal
            }).ToJsonResult();
        }
        [HttpPost]
        [Permissao(eAcessoId.Usuario,ePermissaoId.Inserir)]
        public JsonResult ModalCriarUsuario(CUsuarioViewModel model)
        {

            UsuarioService ps = new UsuarioService();
            try
            {
                ps.Inserir(model.mToViewModel<Usuario>());

                return (new CJSONMensagemModel()
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel()
                        {
                            Mensagem = string.Format("Sucesso ao criar o usuario {0}", model.Nome),
                            Titulo = "Sucesso",
                        }
                    }
                }).ToJsonResult();

            }
            catch (Exception ex)
            {
                return (new CJSONMensagemModel()
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel()
                        {
                            Mensagem = ex.Message,
                            Titulo = "Houve um erro ao criar um novo usuario",
                            Stack = ex.StackTrace
                        }
                    }
                }).ToJsonResult();

            }

        }

        [HttpGet]
        public JsonResult ModalExcluirUsuario(int id)
        {
            using (UsuarioService Us = new UsuarioService())
            {
                return (new CJSONMensagemModel()
                {
                    view = this.RenderPartialView(model: Us.ObterPorId(id).mToViewModel<CUsuarioViewModel>()),
                    status = eEstadoModel.SucessoView
                }).ToJsonResult();
            }



        }
        [HttpPost]
        [Permissao(eAcessoId.Usuario,ePermissaoId.Excluir)]
        public JsonResult ModalExcluirUsuario(CUsuarioViewModel model)
        {
            try
            {
                using (UsuarioService ps = new UsuarioService())
                {
                    ps.Remover(model.Id);
                }

                return (new CJSONMensagemModel()
                {
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel()
                        {
                            Mensagem = string.Format("Sucesso ao excluir o usuario {0}", model.Nome),
                            Titulo = "Sucesso",
                        }
                    },
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior)

                }).ToJsonResult();
            }
            catch (Exception ex)
            {

                return (new CJSONMensagemModel()
                {
                    status =eEstadoModel.Error,

                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel()
                        {
                            Mensagem = ex.Message,
                            Titulo = string.Format("Houve um erro ao excluir o usuario {0}", model.Nome),
                            Stack = ex.StackTrace,
                        },

                    }

                }).ToJsonResult();
            }

        }

        [HttpGet]
        public JsonResult ModalEditarUsuario(int id)
        {
            using (UsuarioService Us = new UsuarioService())
            {
                return (new CJSONMensagemModel()
                {
                    view = this.RenderPartialView(model: Us.ObterPorId(id).mToViewModel<CUsuarioViewModel>()),
                    status = eEstadoModel.SucessoView
                }).ToJsonResult();
            }


        }
        [HttpPost]
        [Permissao(eAcessoId.Usuario,ePermissaoId.Editar)]
        public JsonResult ModalEditarUsuario(CUsuarioViewModel model)
        {
            try
            {
                using (UsuarioService ps = new UsuarioService())
                {
                    ps.Editar(model.mToViewModel<Usuario>());
                }

                return (new CJSONMensagemModel()
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        sucesso = new CSucessoModel()
                        {
                            Mensagem = string.Format("Sucesso ao criar o usuario {0}", model.Nome),
                            Titulo = "Sucesso",
                        }
                    }
                }).ToJsonResult();

            }
            catch (Exception ex)
            {
                return (new CJSONMensagemModel()
                {
                    url = Url.Action(CViewModel.ActionAnterior, CViewModel.ControllerAnterior),
                    status = eEstadoModel.SucessoUrl,
                    mensagem = new CMensagemModel
                    {
                        erro = new CErroModel()
                        {
                            Mensagem = ex.Message,
                            Titulo = "Houve um erro ao criar um novo usuario",
                            Stack = ex.StackTrace
                        }
                    }
                }).ToJsonResult();

            }

        }

        private CUsuariosViewModel ObterUsuarios()
        {
          
            CUsuariosViewModel Uvm = new CUsuariosViewModel()
            {
                Usuarios = new List<CUsuarioViewModel>()
            };
            using (UsuarioService Us = new UsuarioService())
            {
                Uvm.Usuarios.AddRange(Us.ObterTodos().mToViewModel<CUsuarioViewModel>());
            }
            return Uvm;
        }
    }
}