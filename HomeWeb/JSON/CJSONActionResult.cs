﻿using HomeBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeWeb.JSON
{
    public class CJSONActionResult : ViewResult
    {
        public CJSONModel JSONModel { get; set; }
        public eEstadoModel status { get; set; }
    }
}