﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

namespace HomeWeb.Models.Atributos
{
   [AttributeUsage(AttributeTargets.Method)]
    public class IgnoreVerificacaoSessaoAttribute : Attribute
    {

        public bool Ignore { get; private set; }

        public IgnoreVerificacaoSessaoAttribute(bool ignore = true)
        {
            Ignore = ignore;
        }
    }
}