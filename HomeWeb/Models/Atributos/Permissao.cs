﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeDBEntity.Enum;
using HomeService;
using HomeWeb.Models.Enum;
using HomeWeb.Models.ViewModel;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HomeWeb.Models.Atributos
{
    [System.AttributeUsage(AttributeTargets.Method)]
    public class PermissaoAttribute : Attribute
    {
        public string PermissaoCod { get; set; }

        public PermissaoAttribute([CallerMemberName] string permissao = null)
        {
            PermissaoCod = permissao;
        }

        public PermissaoAttribute(eAcessoId acesso, ePermissaoId permissao)
        {
            PermissaoCod = $"{acesso.ToString()}.{permissao.ToString()}";
        }
    }
}