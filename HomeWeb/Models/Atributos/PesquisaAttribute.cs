﻿using HomeDBEntity.Helper;
using HomeWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace HomeWeb.Models.Atributos
{

    public enum eCampoNome
    {
        [Description("title")]
        Titulo,
        [Description("description")]
        Descricao,
        [Description("url")]
        Url,
        [Description("value")]
        Valor,
        [Description("name")]
        Nome,
        [Description("selected")]
        Selecionado
    };

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class PesquisaAttribute : Attribute
    {
        public string Propriedade { get; private set; }
        public string Action { get; private set; }
        public PesquisaAttribute(eCampoNome campo)
        {
            Propriedade = campo.Descricao();
        }
        public PesquisaAttribute(eCampoNome campo, string Action)
        {
            Propriedade = campo.Descricao();
            this.Action = Action;
        }
       

    }



}