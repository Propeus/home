﻿using HomeService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.Atributos
{

    public enum eRegraPrivacidade
    {
        [Description("*******")]
        Senha
    }

    [System.AttributeUsage(AttributeTargets.Property)]
    public class PrivacidadeAttribute : Attribute
    {
        public string Privacidade { get; private set; }

        public PrivacidadeAttribute(eRegraPrivacidade tipoPrivacidade)
        {
            Privacidade = tipoPrivacidade.GetDescription();
        }
    }
}