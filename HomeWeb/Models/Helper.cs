﻿using HomeDBEntity.Atributos;
using HomeService.Enum;
using HomeWeb.Models.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using HomeBase;
using HomeService;
using HomeWeb.Models.Atributos;
using HomeWeb.JSON;
using System.Globalization;

namespace HomeWeb
{

    public static partial class Helper
    {

        //https://stackoverflow.com/questions/1181849/asp-net-mvc-combine-json-result-with-viewresult
        public static string RenderPartialView(this Controller controller, [CallerMemberName] string viewName = null, object model = null, bool renderScript = false)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            string view;

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                view = sw.GetStringBuilder().ToString();
            }

            if (renderScript)
            {
                var elems = Regex.Matches(view, "(src=\".+\")");
                StringBuilder scripts = new StringBuilder();

                foreach (Match elem in elems)
                {
                    var url = elem.Value.Replace("src=", string.Empty).Replace("\"", string.Empty);
                    scripts.AppendLine("<script>");
                    scripts.AppendLine(RenderScript(url));
                    scripts.AppendLine("</script>");
                }


                view = Regex.Replace(view, "<script src=\".+\"></script>", string.Empty);
                view = view + scripts.ToString();
            }
            return view;

        }
        public static string RenderScript(string path)
        {
            return File.ReadAllText(HttpContext.Current.Server.MapPath(path));
        }
        public static ActionResult RenderView(this Controller controller, [CallerMemberName] string viewName = null, object model = null, bool renderScript = false)
        {

            RenderPartialView(controller, viewName, model);

            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");


            controller.ViewData.Model = model;


            ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
            var viewAction = new CJSONActionResult()
            {
                ViewData = new ViewDataDictionary(model),
                View = viewResult.View,
                ViewName = viewName,
                JSONModel = new CJSONModel
                {
                    status = eEstadoModel.View,
                    view = RenderPartialView(controller, viewName, model, renderScript)
                },
                status = eEstadoModel.ActionResult
            };
            //viewAction.ExecuteResult(controller.ControllerContext);
            return viewAction;


        }
        public static void RedirectToView(this Controller controller, [CallerMemberName] string viewName = null, object model = null, bool renderScript = false)
        {

            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");


            controller.ViewData.Model = model;

            ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
            var viewAction = new ViewResult()
            {
                ViewData = new ViewDataDictionary(model),
                View = viewResult.View,
                ViewName = viewName
            };
            viewAction.ExecuteResult(controller.ControllerContext);
            //return viewAction;


        }

        //public static string ToJson(this CJsonViewModel viewmodel)
        //{
        //    return (new JavaScriptSerializer()).Serialize(viewmodel);
        //}
        public static string ToJson(this object viewmodel)
        {
            return (new JavaScriptSerializer()).Serialize(viewmodel);
        }

        public static JsonResult ToJsonResult(this CJsonViewModel viewmodel)
        {
            if (!(new CookieService()).ExisteCookie("CJsonViewModel_Cookie"))
                return (new JsonResult() { Data = (new JavaScriptSerializer()).Serialize(viewmodel), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            else
                return null;
        }

        public static JsonResult ResponseJSON(eTipoMensagem tipo, CMensagemViewModel mensagem = null, string url = null, string view = null)
        {
            return ToJsonResult(new CJsonViewModel
            {
                mensagem = mensagem,
                url = url,
                view = view,
                status = tipo
            });
        }

        public static void Redrect(this ActionExecutingContext filterContext, CJSONModel jr)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
                filterContext.HttpContext.Response.Write(jr.ToJson());
            else
                filterContext.HttpContext.Response.Redirect(jr.url, true);
        }

        public static string GerarSenhaExibicao()
        {
            System.Text.StringBuilder sb = new StringBuilder();

            sb.Append('*', (new Random()).Next(8, 20));
            return sb.ToString();
        }

        public static string ToDateView(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToString("yyyy-MM-dd");
            else
                return null;
        }
        public static string ToHourView(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToString("HH:mm");
            else
                return null;
        }
        public static string ToDateView(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
        public static string ToHourView(this DateTime date)
        {
            return date.ToString("HH:mm");
        }
        public static string Esconder(this string texto, int comprimento = 50)
        {
            if (string.IsNullOrEmpty(texto))
            {
                return texto;
            }
            else if (texto.Length > comprimento)
                return texto.Substring(0, comprimento) + "...";
            else
                return texto;
        }
        public static void InstanceList(object target, PropertyInfo propertyList, object insertion)
        {
            //var listType = typeof(List<>);
            //var constructedListType = listType.MakeGenericType(type);

            //var instance = Activator.CreateInstance(constructedListType);
            //return instance;

            var listType = typeof(List<>);
            var genericArgs = propertyList.PropertyType.GetGenericArguments();
            var concreteType = listType.MakeGenericType(genericArgs);
            var newList = Activator.CreateInstance(concreteType);
            propertyList.SetValue(target, newList);
            var i = (IEnumerable)insertion;
            IList l = (IList)newList;
            if (i != null)
            {
                foreach (var item in i)
                {
                    l.Add(item.mToViewModel(InfoObjectStruct.GetTypeList(propertyList)));
                }
            }
        }

        internal struct InfoObjectStruct
        {
            public InfoObjectStruct(object objeto)
            {
                Type = objeto.GetType();
                Properties = Type.GetProperties();
                Object = objeto;
                TypeCast = Type.GetCustomAttribute<CastAttribute>();
                Privaciade = Type.GetCustomAttribute<PrivacidadeAttribute>();
            }
            public InfoObjectStruct(Type objeto)
            {
                Type = objeto;
                Properties = Type.GetProperties();
                Object = Activator.CreateInstance(Type);
                TypeCast = Type.GetCustomAttribute<CastAttribute>();
                Privaciade = Type.GetCustomAttribute<PrivacidadeAttribute>();
            }

            public Type Type { get; set; }
            public PropertyInfo[] Properties { get; set; }
            public CastAttribute TypeCast { get; set; }
            public PrivacidadeAttribute Privaciade { get; set; }
            public object Object { get; set; }

            public static CastAttribute GetCast(PropertyInfo property)
            {
                if (property.PropertyType.IsGenericType && property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                {
                    var dataProperty = property.GetCustomAttribute<CastAttribute>();
                    if (dataProperty == null)
                    {
                        var data = property.PropertyType.GetTypeInfo().GenericTypeArguments[0];
                        dataProperty = data.GetCustomAttribute<CastAttribute>();
                        return dataProperty;
                    }
                    return dataProperty;
                }
                else
                {
                    return property.GetCustomAttribute<CastAttribute>() ?? property.PropertyType.GetCustomAttribute<CastAttribute>();
                }
            }
            public CastAttribute GetCastPropertyEqualsName(PropertyInfo property)
            {
                var dataaux = GetPropertyEqualsName(property);
                if (dataaux != null)
                {

                    if (IsPropertyList(dataaux))
                    {
                        var dataProperty = dataaux.GetCustomAttribute<CastAttribute>();
                        if (dataProperty == null)
                        {
                            var data = dataaux.PropertyType.GetTypeInfo().GenericTypeArguments[0];
                            dataProperty = data.GetCustomAttribute<CastAttribute>();
                            return dataProperty;
                        }
                        return dataProperty;
                    }
                    else
                    {
                        return dataaux.GetCustomAttribute<CastAttribute>() ?? dataaux.PropertyType.GetCustomAttribute<CastAttribute>();
                    }
                }
                return null;


            }
            public PrivacidadeAttribute GetPrivacidadePropertyEqualsName(PropertyInfo property)
            {
                var dataaux = GetPropertyEqualsName(property);
                if (dataaux != null)
                {
                    property = dataaux;
                    if (IsPropertyList(property))
                    {
                        var dataProperty = property.GetCustomAttribute<PrivacidadeAttribute>();
                        if (dataProperty == null)
                        {
                            var data = property.PropertyType.GetTypeInfo().GenericTypeArguments[0];
                            dataProperty = data.GetCustomAttribute<PrivacidadeAttribute>();
                            return dataProperty;
                        }
                        return dataProperty;
                    }
                    else
                    {
                        return property.GetCustomAttribute<PrivacidadeAttribute>() ?? property.PropertyType.GetCustomAttribute<PrivacidadeAttribute>();
                    }
                }
                return null;


            }
            public PropertyInfo GetPropertyEqualsName(PropertyInfo propertyTarget)
            {
                return Properties.FirstOrDefault(x => propertyTarget.Name == x.Name);
            }
            public PropertyInfo GetPropertyEquals(PropertyInfo propertyTarget)
            {
                try
                {
                    bool IsList = IsPropertyList(propertyTarget);
                    return Properties.FirstOrDefault(x => propertyTarget.Name == x.Name && ((IsList && GetTypeList(propertyTarget).Name == GetTypeList(x).Name) || x.PropertyType == propertyTarget.PropertyType));

                }
                catch (NullReferenceException)
                {
                    return null;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Erro ao obter a propriedade {propertyTarget.Name}", ex);
                }
            }
            public PropertyInfo GetPropertyConatainsName(PropertyInfo propertyTarget)
            {
                try
                {
                    bool IsList = IsPropertyList(propertyTarget);
                    return Properties.FirstOrDefault(x => propertyTarget.Name.Contains(x.Name) && ((IsList && GetTypeList(propertyTarget).Name == GetTypeList(x).Name) || x.PropertyType == propertyTarget.PropertyType));

                }
                catch (NullReferenceException)
                {
                    return null;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Erro ao obter a propriedade {propertyTarget.Name}", ex);
                }
            }
            public bool IsPropertyList(PropertyInfo property)
            {
                return property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)) && property.PropertyType.IsGenericType && property.PropertyType.GetTypeInfo().GenericTypeArguments[0] != null;
            }
            public bool IsCast(PropertyInfo property)
            {
                var data = GetCast(property);
                return data != null && data.TypeCast.Name == this.TypeCast.TypeCast.Name;
            }
            public static Type GetTypeList(PropertyInfo property)
            {
                if (property.PropertyType.IsGenericType)
                    return property.PropertyType.GetTypeInfo().GenericTypeArguments[0];
                else
                    return null;
            }
            public object GetValue(PropertyInfo property)
            {
                try
                {
                    var data = property.GetValue(Object);
                    return data;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            public PrivacidadeAttribute GetPrivacidadePropertyEquals(PropertyInfo property)
            {
                var dataaux = GetPropertyEquals(property);
                if (dataaux != null)
                {
                    property = dataaux;
                    if (IsPropertyList(property))
                    {
                        var dataProperty = property.GetCustomAttribute<PrivacidadeAttribute>();
                        if (dataProperty == null)
                        {
                            var data = property.PropertyType.GetTypeInfo().GenericTypeArguments[0];
                            dataProperty = data.GetCustomAttribute<PrivacidadeAttribute>();
                            return dataProperty;
                        }
                        return dataProperty;
                    }
                    else
                    {
                        return property.GetCustomAttribute<PrivacidadeAttribute>() ?? property.PropertyType.GetCustomAttribute<PrivacidadeAttribute>();
                    }
                }
                return null;
            }
        }


        public static object mToViewModel(this object de, Type tipo)
        {
            InfoObjectStruct Ode = new InfoObjectStruct(de);
            InfoObjectStruct Opara = new InfoObjectStruct(tipo);

            foreach (PropertyInfo propertyDe in Ode.Properties)
            {
                CastAttribute PropertyCast = Opara.GetCastPropertyEqualsName(propertyDe);
                if (PropertyCast != null)
                {
                    if (Ode.IsPropertyList(propertyDe) && Opara.IsPropertyList(Opara.GetPropertyEqualsName(propertyDe)) && Opara.GetCastPropertyEqualsName(propertyDe).TypeCast.Name == InfoObjectStruct.GetTypeList(propertyDe).Name)
                    {
                        InstanceList(Opara.Object, Opara.GetPropertyEqualsName(propertyDe), propertyDe.GetValue(de));

                    }
                    else if (Opara.GetPropertyEquals(propertyDe) != null)
                    {
                        if (Opara.GetPrivacidadePropertyEqualsName(propertyDe) != null)
                        {
                            Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, Opara.GetPrivacidadePropertyEqualsName(propertyDe).Privacidade);
                        }
                        else
                        {
                            Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, propertyDe.GetValue(de));
                        }
                    }
                    else if (Opara.GetPropertyEqualsName(propertyDe) != null)
                    {
                        if (Opara.GetPrivacidadePropertyEqualsName(propertyDe) != null)
                        {
                            if (Ode.GetValue(propertyDe) != null)
                            {
                                Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, Opara.GetPrivacidadePropertyEqualsName(propertyDe).Privacidade);
                            }
                        }
                        else
                        {
                            if (Ode.GetValue(propertyDe) != null)
                            {
                                Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, Ode.GetValue(propertyDe).mToViewModel(Opara.GetPropertyEqualsName(propertyDe).PropertyType));
                            }
                        }
                    }
                }
                else if (Opara.GetPropertyEquals(propertyDe) != null)
                {
                    if (Opara.GetPrivacidadePropertyEquals(propertyDe) != null)
                    {
                        Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, Opara.GetPrivacidadePropertyEqualsName(propertyDe).Privacidade);
                    }
                    else if (Opara.IsPropertyList(propertyDe) && Opara.IsPropertyList(Opara.GetPropertyEqualsName(propertyDe)) && Opara.GetCastPropertyEqualsName(propertyDe)?.TypeCast.Name == InfoObjectStruct.GetTypeList(propertyDe).Name)
                    {
                        InstanceList(Opara.Object, Opara.GetPropertyEqualsName(propertyDe), propertyDe.GetValue(de));
                    }
                    else if (propertyDe.PropertyType.IsPrimitive || Opara.GetPropertyEquals(propertyDe) != null)
                    {

                        var data = propertyDe.GetValue(de);
                        Opara.GetPropertyEquals(propertyDe).SetValue(Opara.Object, data);
                    }
                }

            }
            return Opara.Object;
        }
        public static Tout mToViewModel<Tout>(this object de)
        {
            //InfoObjectStruct Ode = new InfoObjectStruct(de);
            //InfoObjectStruct Opara = new InfoObjectStruct(typeof(Tout));

            //foreach (PropertyInfo propertyDe in Ode.Properties)
            //{
            //    CastAttribute PropertyCast = Opara.GetCastPropertyEqualsName(propertyDe);
            //    if (PropertyCast != null)
            //    {
            //        if (Ode.IsPropertyList(propertyDe))
            //        {
            //            var data = propertyDe.GetValue(de) as IEnumerable<object>;
            //            data = data.mToViewModel(PropertyCast.TypeCast);
            //            Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, data);
            //        }
            //        else
            //        {
            //            if (Opara.GetPrivacidadePropertyEqualsName(propertyDe) != null)
            //            {
            //                Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, Opara.GetPrivacidadePropertyEqualsName(propertyDe).Privacidade);
            //            }
            //            else
            //            {
            //                Opara.GetPropertyEqualsName(propertyDe).SetValue(Opara.Object, propertyDe.GetValue(de));
            //            }
            //        }
            //    }

            //}
            //return (Tout)Opara.Object;

            return (Tout)de.mToViewModel(typeof(Tout));
        }
        public static List<Tout> mToViewModel<Tout>(this IEnumerable<object> de)
        {
            var typeCast = de.GetType().GetTypeInfo().GenericTypeArguments[0];
            List<Tout> Lt = new List<Tout>();
            foreach (var item in de)
            {
                Lt.Add(item.mToViewModel<Tout>());

            }

            return Lt;
        }


        public static Tout ToViewModel<Tout>(this object de, bool unsafeMode = false)
          where Tout : class
        {
            Type DeType = de.GetType();
            PropertyInfo[] DeProperties = DeType.GetProperties();

            Type ParaType = typeof(Tout);
            object Para = Activator.CreateInstance(ParaType);
            PropertyInfo[] ParaProperties = ParaType.GetProperties();

            foreach (var propertyDe in DeProperties)
            {
                Type PropertyType = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name)?.PropertyType;
                var data = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name);
                CastAttribute PropertyCast = null;
                if (data != null)
                {
                    PropertyCast = data.GetCustomAttribute<CastAttribute>();
                    if (PropertyCast == null)
                        PropertyCast = data.PropertyType.GetCustomAttribute<CastAttribute>();
                }


                object value;
                if (PropertyCast != null)
                {
                    if (propertyDe.PropertyType.GetInterface("IList") == typeof(IList))
                    {
                        value = (propertyDe.GetValue(de) as IList).ToViewModel(PropertyType);
                    }
                    else
                    {
                        value = propertyDe.GetValue(de)?.ToViewModel(PropertyType);
                    }
                    var propertyPara = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name);
                    propertyPara.SetValue(Para, value);
                }
                else
                {

                    var propertyPara = ParaProperties.FirstOrDefault(x => x.Name.Contains(propertyDe.Name) && ((x.PropertyType.GetInterfaces().Contains(typeof(IEnumerable))) || x.PropertyType == propertyDe.PropertyType));

                    if (propertyPara != null)
                    {
                        if (propertyDe.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                        {
                            value = propertyDe.GetValue(de);

                            var propertyType = propertyPara.GetCustomAttribute<CastAttribute>()?.TypeCast ?? propertyPara.PropertyType.GetCustomAttribute<CastAttribute>()?.TypeCast;
                            if (propertyType != null)
                            {
                                value = value.ToViewModel(propertyType);
                                propertyPara.SetValue(Para, value);

                            }

                        }
                        else
                        {
                            var Privacidade = propertyPara.GetCustomAttribute<PrivacidadeAttribute>();
                            if (Privacidade != null)
                            {
                                propertyPara.SetValue(Para, Privacidade.Privacidade);
                            }
                            else
                            {
                                value = propertyDe.GetValue(de);
                                propertyPara.SetValue(Para, value);
                            }
                        }


                    }
                }
            }

            return (Tout)Para;

        }
        public static object ToViewModel(this object de, Type tipo)
        {
            Type DeType = de.GetType();
            PropertyInfo[] DeProperties = DeType.GetProperties();

            Type ParaType = tipo;
            object Para = Activator.CreateInstance(ParaType);
            PropertyInfo[] ParaProperties = ParaType.GetProperties();

            foreach (var propertyDe in DeProperties)
            {
                Type PropertyType = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name)?.PropertyType;/*ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name)?.GetCustomAttribute<CastAttribute>()?? ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name)?.PropertyType.GetCustomAttribute<CastAttribute>();*/
                CastAttribute PropertyCast = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name)?.GetCustomAttribute<CastAttribute>() ?? ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name)?.PropertyType.GetCustomAttribute<CastAttribute>();


                object value;
                if (PropertyCast != null)
                {
                    value = propertyDe.GetValue(de).ToViewModel(PropertyType);
                    var propertyPara = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name);
                    propertyPara.SetValue(Para, value);
                }
                else
                {

                    var propertyPara = ParaProperties.FirstOrDefault(x => x.Name.Contains(propertyDe.Name) && x.PropertyType == propertyDe.PropertyType);

                    if (propertyPara != null)
                    {
                        var Privacidade = propertyPara.GetCustomAttribute<PrivacidadeAttribute>();
                        if (Privacidade != null)
                        {
                            propertyPara.SetValue(Para, Privacidade.Privacidade);
                        }
                        else
                        {
                            value = propertyDe.GetValue(de);
                            propertyPara.SetValue(Para, value);
                        }

                    }
                }
            }

            return Para;

        }
        public static object ToViewModelCast(this object de)
        {
            if (de is null)
                return null;

            Type DeType = de.GetType();
            PropertyInfo[] DeProperties = DeType.GetProperties();
            CastAttribute DeCast = DeType.GetCustomAttribute<CastAttribute>();

            if (DeCast == null)
            {
                return null;
            }

            Type ParaType = DeCast.TypeCast;
            object Para = Activator.CreateInstance(ParaType);
            PropertyInfo[] ParaProperties = ParaType.GetProperties();

            foreach (var propertyDe in DeProperties)
            {
                CastAttribute PropertyCast = propertyDe.GetCustomAttribute<CastAttribute>();
                object value;
                if (PropertyCast != null)
                {
                    value = propertyDe.GetValue(de).ToViewModelCast();
                    var propertyPara = ParaProperties.FirstOrDefault(x => x.Name == propertyDe.Name);
                    propertyPara.SetValue(Para, value);
                }
                else
                {

                    var propertyPara = ParaProperties.FirstOrDefault(x => x.Name.Contains(propertyDe.Name) && x.PropertyType == propertyDe.PropertyType);

                    if (propertyPara != null)
                    {
                        var Privacidade = propertyPara.GetCustomAttribute<PrivacidadeAttribute>();
                        if (Privacidade != null)
                        {
                            propertyPara.SetValue(Para, Privacidade.Privacidade);
                        }
                        else
                        {
                            value = propertyDe.GetValue(de);
                            propertyPara.SetValue(Para, value);
                        }

                    }
                }
            }

            return Para;

        }
        public static T ToViewModelCast<T>(this object de)
        {
            return (T)de.ToViewModelCast();

        }
        public static Tout ToPesquisaViewModel<Tout>(this object de, bool unsafeMode = false)
        where Tout : class
        {

            Type De = de.GetType();
            Type Para = typeof(Tout);

            Tout para = Activator.CreateInstance<Tout>();

            var propertiesDe = De.GetProperties();
            var propertiesPara = Para.GetProperties();

            foreach (var propertyDe in propertiesDe)
            {
                var attr = propertyDe.GetCustomAttributes(typeof(PesquisaAttribute), false);

                var pesquisaAttr = attr.Length == 0 ? default(PesquisaAttribute[]) : attr;
                if (attr.Length != 0)
                {
                    foreach (PesquisaAttribute attributo in attr)
                    {
                        PropertyInfo propertyPara;
                        if (!unsafeMode)
                            propertyPara = propertiesPara.FirstOrDefault(x => x.Name == attributo.Propriedade && x.PropertyType == propertyDe.PropertyType);
                        else
                            propertyPara = propertiesPara.FirstOrDefault(x => x.Name == attributo.Propriedade);

                        if (propertyPara != null)
                        {
                            if (propertyDe.PropertyType.GetInterface("IList") == typeof(IList))
                            {
                                var data = propertyDe.GetValue(de);
                                propertyPara.SetValue(para, data.ToViewModelCast());
                            }
                            else
                            {
                                if (attributo.Propriedade == eCampoNome.Url.GetDescription())
                                {
                                    var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;
                                    string controller = (string)routeValues["controller"];
                                    var prp = propertiesDe.FirstOrDefault(x => x.CustomAttributes.Count(y => y.AttributeType == typeof(PesquisaAttribute)) > 0);
                                    propertyPara.SetValue(para, $"/{controller}/{attributo.Action}?{prp.Name}={prp.GetValue(de)}");
                                }
                                else if (attributo.Propriedade == eCampoNome.Selecionado.GetDescription())
                                {
                                    if (propertyDe.GetValue(de) is IList)
                                    {
                                        throw new Exception();
                                    }
                                    else
                                    {
                                        throw new Exception();
                                    }
                                }
                                else
                                    propertyPara.SetValue(para, propertyDe.GetValue(de));

                            }

                        }
                    }


                }

            }


            return para;
        }
        public static List<Tout> ToViewModel<Tout>(this IEnumerable<object> de, bool unsafeMode = false)
            where Tout : class
        {

            List<Tout> Lt = new List<Tout>();
            foreach (var item in de)
            {
                Lt.Add(item.ToViewModel<Tout>(unsafeMode));

            }

            return Lt;

        }
        public static IEnumerable<object> ToViewModel(this IEnumerable<object> de, Type typeCast)
        {

            List<object> Lt = new List<object>();
            foreach (var item in de)
            {
                Lt.Add(item.ToViewModel(typeCast));

            }

            return Lt;

        }
        public static List<Tout> ToPesquisaViewModel<Tout>(this IEnumerable<object> de, bool unsafeMode = false)
          where Tout : class
        {

            List<Tout> Lt = new List<Tout>();
            foreach (var item in de)
            {
                Lt.Add(item.ToPesquisaViewModel<Tout>(unsafeMode));
            }

            return Lt;

        }
    }
}