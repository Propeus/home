﻿using HomeSignalService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.SignalR
{
    public class CJSONSignalRChatModel : CJSONSignalRMensagemModel
    {
        public CJSONSignalRChatModel() : base()
        {
            status = HomeBase.eEstadoModel.SignalRChat;
            Nome = $"Chat_SignalR";
        }
        public DateTime DataEnvio { get; set; } = DateTime.Now;
        public string GUIDChat { get; set; }
        public bool Privativo { get; set; } = true;


    }
}