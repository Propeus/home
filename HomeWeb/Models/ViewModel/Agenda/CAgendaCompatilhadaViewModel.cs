﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeWeb.Models.Atributos;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(AgendaCompartilhada))]
    public class CAgendaCompatilhadaViewModel
    {
      
        public int IdAgenda { get; set; }
        public int IdUsuario { get; set; }
        public int Id { get; set; }

        public CUsuarioViewModel Usuario { get; set; }
    }
}