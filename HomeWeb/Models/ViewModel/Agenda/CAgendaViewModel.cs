﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeWeb.Models.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Agenda))]
    public class CAgendaViewModel : CViewModel
    {
        [Pesquisa(eCampoNome.Url, "ModalVisualizarAgenda")]
        [Pesquisa(eCampoNome.Valor)]
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        [Pesquisa(eCampoNome.Nome)]
        [Pesquisa(eCampoNome.Titulo)]
        public string NomeEvento { get; set; }
        public string Local { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime DataFim { get; set; }
        public DateTime HoraFim { get; set; }
        public System.DateTime Inicio { get; set; }
        public Nullable<System.DateTime> Fim { get; set; }
        public bool DiaInteiro { get; set; }
        [Pesquisa(eCampoNome.Descricao)]
        public string Descricao { get; set; }
        public DateTime Lembrete { get; set; }
        [Pesquisa(eCampoNome.Selecionado)]
        public List<CAmizadeViewModel> Amigos { get; set; }
        public CUsuarioViewModel Usuario { get; set; }
        public List<int> postAmigos { get; set; }
        public List<CAgendaCompatilhadaViewModel> AgendaCompartilhada { get; set; }

    }
}