﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    public class CAgendasViewModel : CViewModel
    {
        public List<CAgendaViewModel> Agendas { get; set; }
        public List<int> Ids { get; set; }  
    }
}