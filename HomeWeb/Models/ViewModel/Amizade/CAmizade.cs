﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeDBEntity.Enum;
using HomeWeb.Models.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Amizades))]
    public partial class CAmizadeViewModel : CViewModel
    {
        public int IdUsuario { get; set; }
        public int IdAmigo { get; set; }
        public CUsuarioViewModel Usuario1 { get; set; }
        public CUsuarioViewModel Usuario { get; set; }
        public eEstadoAmizade IdEstadoAmizade { get; set; } = eEstadoAmizade.Nenhum;
    }
}