﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    public class CAmizadesViewModel : CViewModel
    {
        public List<CAmizadeViewModel> Amizades { get; set; }
        public List<int> Id { get;  set; }
    }
}