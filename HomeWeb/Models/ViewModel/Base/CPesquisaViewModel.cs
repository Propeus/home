﻿using HomeWeb.Models.Atributos;
using HomeWeb.Models.Enum;

namespace HomeWeb.Models.ViewModel
{
    public class CPesquisaViewModel : CViewModel
    {
        public eTipoPesquisa tipoPesquisa { get; set; } = eTipoPesquisa.Search;
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public int value { get; set; }
        public string name { get; set; }
        public bool selected { get; set; } = false;

    }
}