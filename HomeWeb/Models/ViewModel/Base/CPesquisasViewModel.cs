﻿using HomeWeb.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    public class CPesquisasViewModel : CViewModel
    {
        public eTipoPesquisa tipoPesquisa { get; set; }
        public  List<CPesquisaViewModel> Objetos { get; set; }
    }
}