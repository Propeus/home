﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    public class CDropDownViewModel : CViewModel
    {

        public string ValorExibicao { get; set; }
        public List<CDropDownItemModel> Itens { get; set; }

    }

    public class CDropDownItemModel
    {
        public string Nome { get; set; }
        public int Codigo { get; set; }
    }
}