﻿using HomeBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    public class CViewModel : IModel
    {

        public CViewModel()
        {
            UrlAnterior = UrlAtual;
            UrlAtual = HttpContext.Current.Request.UrlReferrer?.AbsolutePath ?? string.Empty;
            if (UrlAnterior != null)
            {
                ControllerAnterior = HttpContext.Current.Request.UrlReferrer.Segments.Skip(1).Take(1).SingleOrDefault();
                ActionAnterior = HttpContext.Current.Request.UrlReferrer.Segments.Skip(2).Take(1).SingleOrDefault();
                Contexto = HttpContext.Current;
            }
        }

        public static string UrlAtual { get; private set; }
        public static string UrlAnterior { get; private set; }
        public static string ControllerAnterior { get; private set; }
        public static string ActionAnterior { get; private set; }
        public static HttpContext Contexto { get; private set; }
        //public List<string> AlvoPesquisa { get; set; } = new List<string>();
    }
}