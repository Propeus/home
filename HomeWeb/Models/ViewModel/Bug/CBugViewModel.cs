﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Bugs))]
    public class CBugViewModel : CViewModel
    {
        public int Id { get; set; }
        public int CodigoBug { get; set; }
        public string NomeBug { get; set; }
        public int Qunatidade { get; set; }
        public int Porcentagem { get; set; }

    }
}