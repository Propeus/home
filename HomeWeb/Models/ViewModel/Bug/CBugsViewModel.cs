﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    public class CBugsViewModel : CViewModel
    {

        public List<CBugViewModel> Bugs { get; set; }
    }
}