﻿using HomeDBEntity;
using System.Collections.Generic;
using System.Linq;

namespace HomeWeb.Models.ViewModel
{
    public class CJsonViewModel : CViewModel
    {
        public Dictionary<int, string> Destinatarios { get; set; }

        public CJsonViewModel(List<Usuario> usuarios = null)
        {
            if (usuarios != null)
            {
                Destinatarios = HomeSignalService.SignalSessao.Clientes.Where(x => usuarios.Select(y => y.Id).Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
            }
            else
            {
                Destinatarios = new Dictionary<int, string>();
            }
        }

        public HomeService.Enum.eTipoMensagem status { get; set; }
        public string url { get; set; }
        public string view { get; set; }
        public CMensagemViewModel mensagem { get; set; } = new CMensagemViewModel();
        public string Nome { get; set; }
        public CSignalViewModel Signal { get; set; }
    }
}