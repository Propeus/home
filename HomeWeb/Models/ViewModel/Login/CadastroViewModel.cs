﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeWeb.Models.Atributos;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Usuario))]
    public class CCadastroViewModel : CViewModel
    {

        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Usuario1 { get; set; }
        public string E_mail { get; set; }
        public string Sobrenome { get; set; }

    }
}