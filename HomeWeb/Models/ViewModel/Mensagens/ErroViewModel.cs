﻿using System;
using HomeService.Enum;

namespace HomeWeb.Models.ViewModel
{
    public class CErroViewModel : IMensagemViewModel
    {

        public CErroViewModel()
        {
            if (Titulo == null)
            {
                Titulo = eTipoMensagem.Error.ToString();
            }
        }

        public string Mensagem { get; set; }
        public string Titulo { get; set; }

        public string Stack { get; set; }

#if DEBUG
        public bool ExibirStackTrace { get; set; } = true;
#else
        public bool ExibirStackTrace { get; set; } = false;

#endif

    }
}