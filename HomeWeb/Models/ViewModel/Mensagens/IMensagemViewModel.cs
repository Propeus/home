﻿using HomeService.Enum;

namespace HomeWeb.Models.ViewModel
{
    public interface IMensagemViewModel
    {
        string Mensagem { get; set; }
        string Titulo { get; set; }
    }
}