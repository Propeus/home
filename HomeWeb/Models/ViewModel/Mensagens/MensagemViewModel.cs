﻿using HomeService.Enum;

namespace HomeWeb.Models.ViewModel
{
    public class CMensagemViewModel : CViewModel
    {
        public eTipoMensagem status { get; set; }
        public int timeout { get; set; } = 2000;
        public CErroViewModel erro { get; set; }
        public CSucessoViewModel sucesso { get; set; }
    }
}