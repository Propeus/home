﻿using System;
using HomeService.Enum;

namespace HomeWeb.Models.ViewModel
{
    public class CSucessoViewModel : IMensagemViewModel
    {
        public CSucessoViewModel()
        {

            if (Titulo == null)
            {
                Titulo = eTipoMensagem.Sucesso.ToString();
            }
        }
        public string Mensagem { get; set; }
        public string Titulo { get; set; }
    }
}