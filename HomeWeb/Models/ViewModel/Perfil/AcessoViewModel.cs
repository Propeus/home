﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeWeb.Models.Atributos;
using System;
using System.Collections.Generic;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Acesso))]
    public class CAcessoViewModel
    {
        public CAcessoViewModel(Acesso acesso)
        {
            Codigo = acesso.Codigo;
            Nome = acesso.Nome;
        }

        public CAcessoViewModel()
        { }
        [Pesquisa(eCampoNome.Valor)]
        public int Codigo { get; set; }
        [Pesquisa(eCampoNome.Nome)]
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public List<CPermissaoViewModel> Permissoes { get; set; } = new List<CPermissaoViewModel>();
        public bool Marcado { get; set; }
        public List<string> PermissoesPost { get; internal set; }
    }
}