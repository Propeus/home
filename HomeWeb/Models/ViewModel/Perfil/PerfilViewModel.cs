﻿using System.Collections.Generic;

namespace HomeWeb.Models.ViewModel
{
    public class CPerfilViewModel : CViewModel
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }

        public List<CAcessoViewModel> Acessos { get; set; }
        public List<string> AcessosPost { get; set; }
        public List<CUsuarioViewModel> Usuarios { get; set; }
        public List<int> UsuariosPost { get; set; }
    }
}