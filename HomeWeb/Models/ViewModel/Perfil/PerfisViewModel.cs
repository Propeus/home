﻿using System.Collections.Generic;

namespace HomeWeb.Models.ViewModel
{
    public class CPerfisViewModel
    {
        public List<CPerfilViewModel> Perfis { get; set; }
        public List<CAcessoViewModel> Acessos { get; set; }
        public List<CPermissaoViewModel> Permissoes { get; set; }
    }
}