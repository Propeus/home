﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using System;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Permissao))]
    public class CPermissaoViewModel
    {
        public CPermissaoViewModel(Permissao permissao)
        {
            Codigo = permissao.Codigo;
            Nome = permissao.Nome;
        }

        public CPermissaoViewModel() { }

        public int Codigo { get; set; }
        public string Nome { get; set; }
        public bool Marcado { get; set; }
        public string Descricao { get; set; }
    }
}