﻿using HomeBase;
using System;

namespace HomeWeb.Models.ViewModel
{
    public class CSignalViewModel
    {
        public string HandshakeCode { get; internal set; }
        public string Nome { get; set; } = $"Handshake_Affinity";
        public double ExpiresCookie { get; set; } = DateTime.Now.AddDays(2).TotaOfDays();

    }
}