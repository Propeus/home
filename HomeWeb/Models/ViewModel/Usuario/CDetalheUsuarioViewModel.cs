﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(DetalheUsuario))]
    public class CDetalheUsuarioViewModel : CViewModel
    {
        public int Id { get; set; }
        public int ContadorAgendaCompartilhada { get; set; }
        public int ContadorSolicitacaoAmizade { get; set; }
    }
}