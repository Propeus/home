﻿using HomeDBEntity;
using HomeDBEntity.Atributos;
using HomeWeb.Models.Atributos;

namespace HomeWeb.Models.ViewModel
{
    [Cast(typeof(Usuario))]
    public class CUsuarioViewModel : CViewModel
    {
        [Pesquisa(eCampoNome.Url, "ModalVisualizar")]
        [Pesquisa(eCampoNome.Valor)]
        public int Id { get; set; }
        [Pesquisa(eCampoNome.Titulo)]
        [Pesquisa(eCampoNome.Nome)]
        public string Usuario1 { get; set; }
        [Privacidade(eRegraPrivacidade.Senha)]
        public string Senha { get; set; } = Helper.GerarSenhaExibicao();
        public string E_mail { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }

    }
}