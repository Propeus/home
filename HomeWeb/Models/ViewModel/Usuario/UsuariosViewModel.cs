﻿using System.Collections.Generic;

namespace HomeWeb.Models.ViewModel
{
    public class CUsuariosViewModel : CViewModel
    {
        public List<CUsuarioViewModel> Usuarios { get; set; }
    }
}