﻿var Lista = CreateList('listaIds');

var vGroupButton = new Vue({
    el: '#master',
    data: {
        vColor: 'brown',
        vUrl: '',
        vName: 'Selecione uma opção',
        vIcon: '',
        vId: '',
        vAllowClick: 'false',
        vAllowMultiple: 'false',
        vView: ''
    },
    computed: {
        Habilitar: function () {
            var r = this.vAllowClick != 'true';
            if (r) {
                r = (this.vColor !== 'green' && (IsNullOrEmpty(this.vUrl) || IsNullOrEmpty(this.vId)))
                if (r)
                    return 'disabled';
                else
                    return '';

            } else {
                return '';
            }
        },
    },
    methods: {
        Redirect: function (url, Event) {
            $('.card .content i').removeClass(this.vIcon);
            $('.card .content i').removeClass(this.vColor);
            this.vColor = GetDataColor($(event.currentTarget));
            this.vName = GetDataName($(event.currentTarget));
            this.vIcon = GetDataIcon($(event.currentTarget));
            this.vAllowClick = GetAllowClick($(event.currentTarget));
            this.vAllowMultiple = GetAllowMultiple($(event.currentTarget));
            this.vView = GetDataView($(event.currentTarget));
            this.vUrl = url;
            Lista.Clear();
            this.vId = '';
        },
        Visualizar: function (url, Event) {
            $('.card .content i').removeClass(this.vIcon);
            $('.card .content i').removeClass(this.vColor);
            this.vColor = 'blue';
            this.vName = GetDataName($(event.currentTarget));
            this.vUrl = url;
            this.vIcon = 'search';
            Lista.Clear();
            this.vId = '';
        },
        Editar: function (url, Event) {
            $('.card .content i').removeClass(this.vIcon);
            $('.card .content i').removeClass(this.vColor);
            this.vColor = 'yellow';
            this.vName = GetDataName($(event.currentTarget));
            this.vUrl = url;
            this.vIcon = 'edit'
            Lista.Clear();
            this.vId = '';
        },
        Excluir: function (url, Event) {
            $('.card .content i').removeClass(this.vIcon);
            $('.card .content i').removeClass(this.vColor);
            this.vColor = 'red';
            this.vName = GetDataName($(event.currentTarget));
            this.vUrl = url;
            this.vIcon = 'delete'
            Lista.Clear();
            this.vId = '';
        },
        Novo: function (url, Event) {
            $('.card .content i').removeClass(this.vIcon);
            $('.card .content i').removeClass(this.vColor);
            this.vColor = 'green';
            this.vName = GetDataName($(event.currentTarget));
            this.vUrl = url;
            this.vIcon = 'add'
            Lista.Clear();
            this.vId = '';
        },
        Click: function () {
            var c = new ComunicacaoGenerica();
            c.DataUrl = this.vUrl;
            c.Params = this.vId;
            if (this.vView == '' || this.vView != 'page') {
                ShowModal(c);
            }
            else {
                RedirectComunicacaoGenerica(c);
            }
        },
        InsertId: function (Id, Event) {
            if (this.vAllowMultiple=='true') {
                Lista.Add(Id, 'Id');
                this.vId = Lista.SerializeString();
                $(Event.currentTarget).find('.content i').addClass(this.vColor);
                $(Event.currentTarget).find('.content i').addClass(this.vIcon);
            } else if (this.vAllowMultiple == 'false') {
                $('.card .content i').removeClass(this.vIcon);
                $('.card .content i').removeClass(this.vColor);
                $(Event.currentTarget).find('.content i').addClass(this.vColor);
                $(Event.currentTarget).find('.content i').addClass(this.vIcon);
                Lista.Clear();
                Lista.Add(Id, 'Id');
                this.vId = Lista.SerializeString();
            } else {
                $('.card .content i').removeClass(this.vIcon);
                $('.card .content i').removeClass(this.vColor);
                Lista.Clear();
                this.vId = '';
            }
            //if (Lista.Contains(Id)) {
            //    Lista.Remove(Id);
            //    this.vId = Lista.SerializeString();
            //    $(Event.currentTarget).find('.content i').removeClass(this.vColor);
            //    $(Event.currentTarget).find('.content i').removeClass(this.vIcon);
            //} else {
            //    if (this.vColor === 'red') {
            //        Lista.Add(Id, 'Id');
            //        this.vId = Lista.SerializeString();
            //        $(Event.currentTarget).find('.content i').addClass(this.vColor);
            //        $(Event.currentTarget).find('.content i').addClass(this.vIcon);
            //    } else if (this.vColor === 'green') {
            //        $('.card .content i').removeClass(this.vIcon);
            //        $('.card .content i').removeClass(this.vColor);
            //        Lista.Clear();
            //        this.vId = '';
            //    } else if (this.vColor !== 'brown') {
            //        $('.card .content i').removeClass(this.vIcon);
            //        $('.card .content i').removeClass(this.vColor);
            //        $(Event.currentTarget).find('.content i').addClass(this.vColor);
            //        $(Event.currentTarget).find('.content i').addClass(this.vIcon);
            //        Lista.Clear();
            //        Lista.Add(Id, 'Id');
            //        this.vId = Lista.SerializeString();
            //    }
            //}
            $(Event.currentTarget).transition('pulse');

        },

    }

})

$(document).ready(function () {
    Lista.IsMVC = true;
})

