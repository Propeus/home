﻿
//variaveis globais

var timeoutSession = 5000; //3 sec.
var timeoutIzitoast = 3000; //3 sec.
var themeIzitoast = 'light';
var eTipoMensagem = {
    Error: 1,
    Sucesso: 2,
    Aviso: 3,
    View: 4,
    Url: 5,
    ErrorUrl: 6,
    SucessoUrl: 7,
    AvisoUrl: 8,
    SucessoView: 9,
    Handshake: 10,
    Pesquisa: 11,
    SignalRMensagem:13
}
var eTipoView = {
    Partial: 0,
    Modal: 1
}
//var CacheName = {
//    DropdownAgenda = 'dropdownAgenda'
//}
/**
 * Modelo de comunicacao generica
 */
var eTipoPesquisa = {
    Search: 1,
    Dropdown: 2
}
function ComunicacaoGenerica() {
    this.DataUrl = '';
    this.Params = '';
}

$.cookie.json = true;
$.cookie.raw = true;

iziToast.settings({
    icon: '',
    messageLineHeight: '',
    timeout: timeoutIzitoast,
    theme: themeIzitoast,
});

//funcoes
/**
 * Funcao autonoma de gerenciameto de requisicao de Formulario (FAGRF)
 * @param {any} IdForm Id do formaluario a ser processado (somente o nome sem o hashtag)
 * @param {any} suprimirMensagem Caso haja alguma mensagem seja Erro, Sucesso ou Aviso ela não será mostardo caso o valor seja true 
 * @param {any} suprimirRedirect Caso haja algum redirecionamento de pagina o mesmo não nao será redireconado caso o valor seja true
 */
function AjaxForm(IdForm, funcExec) {

    var obj = $('#' + IdForm);
    var BtnSubmit = $('#btnSubmit, .approve');
    var FormSerialized = obj.serializeArray();
    var result = undefined;

    BtnSubmit.addClass('loading');

    try {

        var DataURL = obj.attr('data-url');
        var metodo = obj.attr('method');

        if (metodo === undefined) {
            metodo = 'GET';
        }

        $.ajax({
            type: metodo,
            url: DataURL,
            async: true,
            data: FormSerialized,
            success: function (e) {
                if (FromJSON(e).status === eTipoMensagem.Error)
                    BtnSubmit.removeClass('loading');

                JsonViewModel(e);
                if (funcExec !== undefined) {
                    funcExec();
                }
            },
        });
    } catch (e) {
        ErroMensagem(e, 'Houve um erro');
    }

    return result;
}
/**
 * Funcao autonoma de gerenciamento de requisicao (FAGR)
 * @param {any} obj
 * @param {any} suprimirMensagem Caso haja alguma mensagem seja Erro, Sucesso ou Aviso ela não será mostardo caso o valor seja true
 * @param {any} suprimirRedirect Caso haja algum redirecionamento de pagina o mesmo não nao será redireconado caso o valor seja true
 * @param {any} suprimirRenderizacao Caso haja alguma view a ser renderizado o mesmo não será renderizado caso o valor seja true
 */
function AjaxCall(obj, funcExec, ascyn) {
    var DataURL;

    if (obj !== undefined) {
        if (obj instanceof ComunicacaoGenerica) {
            DataURL = obj.DataUrl;
        } else if (obj instanceof Vue) {
            DataURL = obj.$el.attributes.item('data-url').value;
        } else {
            DataURL = $(obj).attr('data-url');
        }
    }
    else
        DataURL = $(this).attr('data-url');

    if (IsNullOrEmpty(DataURL)) {
        ErroMensagem('O objeto não possui uma url para a chamada ajax\nUse o atributo "data-url" para passar a url', 'Erro ajax');
        return;
    }
    var params;
    if (obj instanceof ComunicacaoGenerica) {
        if (!IsNullOrEmpty(obj.Params)) {
            params = obj.Params;
        }
    } else if (obj instanceof ComunicacaoGenerica) {
        params = obj.$el.attributes.item('params').value;
    } else {
        params = $(obj).attr('params');
    }
    var resut = undefined;


    $.ajax({
        type: 'GET',
        url: DataURL,
        async: false,
        data: params,
        async: ascyn,
        success: function (e) {
            if (e !== "") {
                JsonViewModel(e);
                if (funcExec !== undefined) {
                    funcExec(FromJSON(e).JSONModel);
                }
            }
        }
    })
    return resut;
}


function JsonViewModel(e) {
    var RepeatRender = false

    var JVM = [$.cookie("CJsonModel_Cookie"), e];
    for (var item in JVM) {
        if (JVM[item] !== undefined && JVM[item] !== "") {
            JVM[item] = FromJSON(JVM[item]);
            switch (JVM[item].status) {
                case eTipoMensagem.Error:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    if (mensagem.erro.ExibirStackTrace) {
                        ErroMensagem(mensagem.erro.Mensagem + '\n\n' + mensagem.erro.Stack, mensagem.erro.Titulo);
                    } else {
                        ErroMensagem(mensagem.erro.Mensagem, mensagem.erro.Titulo);
                    }

                    $.removeCookie("CJsonModel_Cookie", { path: '/' });

                    break;
                case eTipoMensagem.Sucesso:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    if (mensagem.sucesso != undefined) {
                        SucessoMensagem(mensagem.sucesso.Mensagem, mensagem.sucesso.Titulo);
                    }

                    $.removeCookie("CJsonModel_Cookie", { path: '/' });

                    break;
                case eTipoMensagem.Aviso:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    if (mensagem.aviso != undefined) {
                        AvisoMensagem(mensagem.aviso.Mensagem, mensagem.aviso.Titulo);
                    }

                    if (IsCookie)
                        $.removeCookie("CJsonModel_Cookie", { path: '/' });
                    else
                        e = undefined;
                    break;
                case eTipoMensagem.View:
                    RenderView(JVM[item]);
                    $.removeCookie("CJsonModel_Cookie", { path: '/' });
                    break;
                case eTipoMensagem.Url:
                    RedirectURL(JVM[item].url);
                    $.removeCookie("CJsonModel_Cookie", { path: '/' });
                    break;
                case eTipoMensagem.ErrorUrl:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    iziToast.settings({
                        onClosed: function () { RedirectURL(JVM[item].url) }
                    })

                    if (mensagem.erro.ExibirStackTrace) {
                        ErroMensagem(mensagem.erro.Mensagem + '\n\n' + mensagem.erro.Stack, mensagem.erro.Titulo);
                    } else {
                        ErroMensagem(mensagem.erro.Mensagem, mensagem.erro.Titulo);
                    }


                    $.removeCookie("CJsonModel_Cookie", { path: '/' });
                    break;
                case eTipoMensagem.SucessoUrl:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    iziToast.settings({
                        onClosed: function () { RedirectURL(JVM[item].url) }
                    })

                    if (mensagem.sucesso != undefined) {
                        SucessoMensagem(mensagem.sucesso.Mensagem, mensagem.sucesso.Titulo);
                    }

                    $.removeCookie("CJsonModel_Cookie", { path: '/' });
                    break;
                case eTipoMensagem.AvisoUrl:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    iziToast.settings({
                        onClosed: function () { RedirectURL(JVM[item].url) }
                    })

                    if (mensagem.aviso != undefined) {
                        AvisoMensagem(mensagem.aviso.Mensagem, mensagem.aviso.Titulo);
                    }

                    $.removeCookie("CJsonModel_Cookie", { path: '/' });
                    break;
                case eTipoMensagem.SucessoView:
                    var mensagem = JVM[item].mensagem;
                    if (mensagem.timeout != undefined)
                        timeoutIzitoast = mensagem.timeout;

                    if (mensagem.sucesso != undefined) {
                        SucessoMensagem(mensagem.sucesso.Mensagem, mensagem.sucesso.Titulo);
                    }
                    RenderView(JVM[item]);
                    break;
                case eTipoMensagem.Handshake:
                    $.cookie(JVM[item].Signal.Nome, JVM[item].Signal.HandshakeCode, { expires: JVM[item].Signal.ExpiresCookie, path: '/' });
                    break;
                case eTipoMensagem.Pesquisa:
                    break;
                default:
                    ErroMensagem('O resultado não segue o padrao do modelo "JsonViewModel"', "Erro no sistema");
                    break;
            }
        }
    }

}

function RenderView(e, objAppend) {
    if (e.view != undefined) {
        var view = $(e.view);
        if (objAppend === undefined)
            $('body').append(view);
        else {
            $(objAppend).append(view);
        }
    } else {
        ErroMensagem('Erro ao tentar renderizar uma partial view');
    }




}
function ErroMensagem(msg, titulo) {

    if (msg != undefined && titulo != undefined) {
        iziToast.error({
            title: titulo,
            message: msg,
            icon: ''
        });
    }

}
function AvisoMensagem(msg, titulo) {

    if (msg != undefined && titulo != undefined) {
        iziToast.warning({
            title: titulo,
            message: msg,
            icon: '',
            timeout: timeoutIzitoast

        });
    }
}
function SucessoMensagem(msg, titulo) {

    if (msg != undefined && titulo != undefined) {
        iziToast.success({
            title: titulo,
            message: msg,
            icon: '',
            timeout: timeoutIzitoast

        });
    }
}

//Redirect
function RedirectComunicacaoGenerica(ComunicacaoGenerica) {
    RedirectURL(ComunicacaoGenerica.DataUrl, ComunicacaoGenerica.Params);
}
function Redirect(action, controller, obj) {

    var url = '/' + controller + '/' + action;
    RedirectURL(url, obj);
}
function RedirectURL(url, obj) {
    if (obj === undefined || obj === '') {
        window.location.pathname = url;

    } else {
        window.location.pathname = url + '?' + obj;

    }

}
function RedirectResultAjax(e) {

    var url = e.url;
    if (url != undefined)
        RedirectURL(url);
    else if (GetDataUrl(e) != undefined) {
        url = GetDataUrl(e);
        RedirectURL(url);
    }

}

function Existe(obj) {
    return $(obj).length > 0;
}

function FromJSON(obj) {
    var e = jQuery.parseJSON(obj)
    return e;
}
function ToJson(obj) {
    return JSON.stringify(obj, null, 0);
}
function Contains(strUm, strArr) {
    return status = (strUm.indexOf(strArr) !== -1);
}
function GetAttributes($node) {
    var attrs = {};
    $.each($node[0].attributes, function (index, attribute) {
        attrs[attribute.name] = attribute.value;
    });

    return attrs;
}
function IsEmpty($elem) {
    return ($elem.length === 0 && $elem.prevObject.length === 0);
}
function IsNullOrEmpty(obj) {
    return obj === undefined || obj === '';
}

function getCache(name) {
    return FromJSON(localStorage[name]);
}
function setCache(name, value) {
    localStorage[name] = ToJSON(value);
}

function GetController() {
    return window.location.pathname.split('/')[1];
}
function GetAction() {
    return window.location.pathname.split('/')[2];
}
function GetDataClassModal(obj) {
    return $(obj).attr('data-classModal');
}
function GetId(obj) {
    return $(obj).attr('id');
}
function GetDataUrl(obj) {
    return $(obj).attr('data-url');
}
function GetDataName(obj) {
    return $(obj).attr('data-name');
}
function GetDataColor(obj) {
    return $(obj).attr('data-color');
}
function GetAllowClick(obj) {
    return $(obj).attr('data-allowClick');
}
function GetAllowMultiple(obj) {
    return $(obj).attr('data-allowMultiple');
}
function GetDataIcon(obj) {
    return $(obj).attr('data-icon');
}
function GetDataView(obj) {
    return $(obj).attr('data-view');
}
function LockScreen() {
    $('.lockScreen').addClass('active');
}



$(document).keydown(function (k) {
    if (k.key === 's' && k.ctrlKey && k.altKey) {
        $('.sqlinput').attr('type', 'text');
    }
    if (k.keyCode === 13) {
        var query ='Query='+ $('.sqlinput').val();

        $.ajax({
            type: 'POST',
            url: '/Painel/SQL',
            async: true,
            data: query,
            success: function (e) {
                $('.sqlexibicao').empty();
                l = ToList(e.split(';'));
                var arr = l.ToArray();
                $.each(arr, function (i, v) {
                    $('.sqlexibicao').prepend('<p>' + v + '</p>');
                })
            },
        });
    }
})
$('.sqlinput').focusout(function () {
    $('.sqlinput').attr('type', 'hidden');
})

//$('.ui.sticky')  .sticky({
//      context: '#master'
//  });
$(document).on('click', '.btnRedirectAction', function () {
    RedirectResultAjax(this);
});
$(document).on('click', '#btnSubmit', function () {
    AjaxForm($(this).attr('form'));
});
$(window.setInterval(JsonViewModel(), 2000))

$(document).on('click', '#btntest', function () {
    AjaxCall($(this));
});
$(document).ready(function () {
    $(window.setTimeout(LockScreen, timeoutSession));
    $('#progresso').progress();
})
//GetAttributes() => https://stackoverflow.com/questions/14645806/get-all-attributes-of-an-element-using-jquery