﻿$('.cmpObrigatorio').focusout(function () {
    var nomeexibicao = $(this).attr('data-nomeExibicao');
    if (nomeexibicao === undefined)
        nomeexibicao = '';

    var allcmpO = $('.cmpObrigatorio');
    if ($(this).val() === undefined || $(this).val() === '') {
        ErroMensagem('É necessario preencher o campo ' + nomeexibicao + '.', 'Campo obrigatório!');
        $('#btnSubmit').addClass('disabled');
    }
    else {
        var desabilitar = true;
        $.each(allcmpO, function (index, item) {
            if ($(item).val() === undefined || $(item).val() === '') {
                desabilitar = false;
            }
        });
        if (desabilitar)
            $('#btnSubmit').removeClass('disabled');
        else
            $('#btnSubmit').addClass('disabled');

    }

})