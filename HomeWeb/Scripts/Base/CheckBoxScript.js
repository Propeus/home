﻿var Estado = { Indeterminado: 'indeterminate', Marcado: 'checked' };

function GetFatherInForm(form) {
    return $(form).find('.FatherList');
}
function GetFather() {
    return $(document).find('.FatherList');
}
function GetFatherElem(elem) {
    var aux = elem;
    var status = true;
    do {
        aux = $(aux).parent();
        status = (aux !== undefined && aux.length > 0) && !aux.hasClass('FatherList');
        if (status === false && aux.length === 0)
            aux = undefined;
    } while (status);

    return aux;
}
function GetMaster(Father, estados) {

    var result = undefined;

    if (estados === undefined || estados === [])
        result = $(Father).find('.MasterList input');
    else {
        var checked = [];
        for (var estado in estados) {
            var t = GetMaster(Father);
            $.each(t, function (index, value) {
                if (Is($(value), estados[estado])) {
                    checked.push($(value));
                }
            })
        }

        result = checked;
    }

    return result;
}
function GetChild(Father, estados) {
    var result;
    if (estados === undefined)
        result = $(Father).find('.ChildList input');
    else {
        var checked = [];
        if (Array.isArray(estados)) {
            for (var estado in estados) {
                var t = GetChild(Father);
                $.each(t, function (index, value) {
                    if (Is($(value), estados[estado])) {
                        checked.push($(value));
                    }
                })
            }
        }
        else {
            var t = GetChild(Father);
            $.each(t, function (index, value) {
                if (Is($(value), estados)) {
                    checked.push($(value));
                }
            })


        }



        result = checked;
    }

    return result;
}

function Is(elem, estado) {
    return $(elem).prop(estado);
}
function All($elems, estado) {
    var result = true;

    $.each($elems, function (index, value) {
        result ? result = Is($(value), estado) : result = false;
    });

    return result;
}
function Some($elems, estado) {
    var result = false;

    $.each($elems, function (index, value) {
        result === false ? result = Is($(value), estado) : result = true;
    });

    return result;
}

function Check(elem, value, estado) {
    if (value) {
        switch (estado) {
            case Estado.Indeterminado:
                $(elem).prop({ 'indeterminate': true });
                break;
            case Estado.Marcado:
                $(elem).prop({ 'checked': true });
                $(elem).attr('value', true);
                break;
            default:
        }
    } else {
        switch (estado) {
            case Estado.Indeterminado:
                $(elem).prop({ 'indeterminate': false });
                break;
            case Estado.Marcado:
                $(elem).prop({ 'checked': false });
                $(elem).attr('value', false);
                break;
            default:
        }
    }
}

$(document).on('change', '.MasterList input', function () {
    var $elem = GetChild(GetFatherElem(this));
    if (Is(this, Estado.Marcado)) {
        $.each($elem, function (index, value) {
            Check(value, true, Estado.Marcado);
        });
    } else {
        $.each($elem, function (index, value) {
            Check(value, false, Estado.Marcado);
        });
    }
});
$(document).on('change', '.ChildList input', function () {

    var $elem = GetChild(GetFatherElem(this));
    if (IsEmpty($elem)) {
        Check($(this), Is($(this), Estado.Marcado), Estado.Marcado);
    }
    else if (All($elem, Estado.Marcado)) {
        Check(GetMaster(GetFatherElem(this)), true, Estado.Marcado);
        Check(GetMaster(GetFatherElem(this)), false, Estado.Indeterminado);
    } else if (Some($elem, Estado.Marcado)) {
        Check(GetMaster(GetFatherElem(this)), true, Estado.Indeterminado);
        Check(GetMaster(GetFatherElem(this)), false, Estado.Marcado);
    } else {
        Check(GetMaster(GetFatherElem(this)), false, Estado.Marcado);
        Check(GetMaster(GetFatherElem(this)), false, Estado.Indeterminado);
    }

});
$(document).on('change', '.checkbox.Conditional input', function () {
    Conditional(this);
})
$(document).on('change', '.checkbox.ConditionalInverted input', function () {
    InvertedConditional(this);
})

function Conditional($elem) {
    var ids = $($elem).attr('data-disable').split(',');


    Check($($elem), Is($($elem), Estado.Marcado), Estado.Marcado);


    if (Is($($elem), Estado.Marcado)) {
        for (var item in ids) {
            $('#' + ids[item]).addClass('disabled');
        }
    } else {
        for (var item in ids) {
            $('#' + ids[item]).removeClass('disabled');
        }
    }
}
function InvertedConditional($elem) {
    var ids = $($elem).attr('data-disable').split(',');
    if (Is($($elem), Estado.Marcado)) {
        for (var item in ids) {
            $('#' + ids[item]).removeClass('disabled');
        }
    } else {
        for (var item in ids) {
            $('#' + ids[item]).addClass('disabled');
        }
    }
}

$(document).ready(function () {
    var fathers = GetFather();
    $.each(fathers, function (index, father) {
        var childs = GetChild($(father));
        if (All(childs, Estado.Marcado)) {
            Check(GetMaster($(father)), true, Estado.Marcado);
            Check(GetMaster($(father)), false, Estado.Indeterminado);
        } else if (Some(childs, Estado.Marcado)) {
            Check(GetMaster($(father)), true, Estado.Indeterminado);
            Check(GetMaster($(father)), false, Estado.Marcado);
        } else {
            Check(GetMaster($(father)), false, Estado.Indeterminado);
            Check(GetMaster($(father)), false, Estado.Indeterminado);
        }
    });
    var chkb = $('.checkbox.Conditional input');
    $.each(chkb, function (i, v) {
        Conditional($(v));
    })
    InvertedConditional($('.checkbox.ConditionalInverted input'));
});