<script type="text/javascript">
var GUID = function uuidv4() {
    var r = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });

    return r;
}
function IsPrimitive(arg) {
    var type = typeof arg;
    return arg == null || (type != "object" && type != "function");
}
function HasList(obj) {
    var result = [];
    if (obj instanceof DoublyLinkedList) {
        var node = obj.Node;

        while (node !== undefined) {
            if (IsList(node.Value)) {
                result.push(node.Value);
            };

            node = node.Next;
        }

    } else {
        $.each(obj, function (index, item) {
            if (item instanceof NodeDoublyLinkedList) {
                if (IsList(item.Value)) {
                    result.push(item.Value);
                }
            } else if (IsList(item)) {
                result.push(item);
            }
        })
    }
    return result.length === 1 ? result[0] : result.length === 0 ? undefined : result;
}
function ToList(array, name) {
    var l = CreateList(name);

    $.each(array, function (index, item) {
        l.Add(item);
    })
    return l;
}

function DoublyLinkedList(name, value) {
    this.Name = name === undefined ? GUID() : name;
    this.Node = undefined;
    this.Count = 0;
    this.Last = undefined;
    this.Add = function Add(Value, name) {

        var aux = this.Node
        var node = CreateNode(Value, name);
        var ult = this.Last;
        if (ult != undefined) {
            ult.Next = node;
            node.Previous = ult;
            this.Last = node;
        } else {
            this.Node = node;
            this.Last = node;
        }
        this.Count++;

    };
    this.AddNode = function AddNode(node) {

        var aux = this.Node
        var ult = this.Last;
        if (ult != undefined) {
            ult.Next = node;
            node.Previous = ult;
            this.Last = node;
        } else {
            this.Node = node;
            this.Last = node;
        }
        this.Count++;

    };
    this.AddAfter = function AddAfter(nodeOrValueRef, nodeOrValue) {
        var node;
        if (nodeOrValue instanceof NodeDoublyLinkedList) {
            node = nodeOrValue;
        }
        else {
            node = CreateNode(nodeOrValue);
        }
        var nodeRef;
        if (nodeOrValueRef instanceof NodeDoublyLinkedList) {
            nodeRef = nodeOrValueRef;
        } else {
            nodeRef = this.GetByIndex(this.IndexOf(nodeOrValueRef));
        }

        var Next = nodeRef.Next;

        node.Next = Next;
        node.Previous = nodeRef;

        Next.Previous = node;
        nodeRef.Next = node;
        this.Count++;

    };
    this.AddBefore = function AddBefore(nodeOrValueRef, nodeOrValue) {
        var node;
        if (nodeOrValue instanceof NodeDoublyLinkedList) {
            node = nodeOrValue;
        }
        else {
            node = CreateNode(nodeOrValue);
        }
        var nodeRef;
        if (nodeOrValueRef instanceof NodeDoublyLinkedList) {
            nodeRef = nodeOrValueRef;
        } else {
            nodeRef = this.GetByIndex(this.IndexOf(nodeOrValueRef));
        }

        var Previous = nodeRef.Previous;

        node.Previous = Previous;
        node.Next = nodeRef;

        Previous.Next = node;
        nodeRef.Previous = node;
        this.Count++;

    };
    this.RemoveNode = function RemoveNode(node) {
        var aux = this.Node;
        while (aux !== undefined && aux.Value !== node.Value) {
            aux = aux.Next;
        };
        if (aux != undefined) {
            var prox = aux.Next;
            var ant = aux.Previous;
            if (prox === undefined && ant === undefined) {
                this.Node = undefined;
                this.Last = undefined;
            } else {
                if (ant !== undefined) {
                    ant.Next = prox;
                } else if (aux.Previous === undefined) {
                    this.Node = prox;
                }

                if (prox !== undefined)
                    prox.Previous = ant;

                aux = undefined;
            }
        }
        this.Count--

        return this;
    };
    this.Remove = function Remove(value) {
        this.RemoveNode(this.GetByIndex(this.IndexOf(value)));
    };
    this.ToArray = function ToArray() {

        var auxNo = this.Node;
        var arr = new Array(this.Count);
        var cout = 0;

        while (auxNo !== undefined) {
            if (auxNo.IsList()) {
                arr[cout] = (auxNo.Value.ToArray());
            } else {
                arr[cout] = (auxNo.Value);
            }
            cout++;
            auxNo = auxNo.Next;
        }

        return arr;
    };
    this.IsEmpty = function IsEmpty() {
        return this.Count == 0;
    };
    this.ToJSON = function ToJSON() {
        var r = this.ToArray();
        var cache = [];
        var result = JSON.stringify(r, function (key, Value) {
            if (typeof Value === 'object' && Value !== null) {
                if (cache.indexOf(Value) !== -1) {
                    return;
                }
                cache.push(Value);
            }
            return Value;
        });
        cache = null;

        return result;
    };
    this.IndexOf = function IndexOf(value) {
        var count = 0;
        var aux = this.Node;

        while (aux !== undefined && aux.Value !== value) {
            count++;
            aux = aux.Next;
        }
        if (aux != undefined && aux.Value === value)
            return count;
        else
            return -1;
    };
    this.GetByIndex = function GetByIndex(index) {
        if (index === -1)
            return undefined;

        var aux = this.Node;
        for (var i = 0; i < index; i++) {
            aux = aux.Next;
        }
        return aux;
    };
    this.Contains = function Contains(value) {
        return this.GetByIndex(this.IndexOf(value)) !== undefined;
    };
    this.Clear = function Clear() {
        this.Node = undefined;
        this.Last = undefined;
        this.Count = 0;
    };
    this.SerializeArray = function SerializeArray() {

        var auxNo = this.Node;
        var arr = new Array(this.Count);
        var cout = 0;

        while (auxNo !== undefined) {
            if (auxNo.IsList()) {
                arr[cout] = (auxNo.Value.Serialize());
            } else {
                arr[cout] = { name: auxNo.Name, value: (auxNo.Value) };
            }
            cout++;
            auxNo = auxNo.Next;
        }

        return arr;
    };
    this.SerializeString = function SerializeString() {

        var auxNo = this.Node;
        var SB = new CreateStringBuilder();

        while (auxNo !== undefined) {
            if (auxNo.IsList()) {
                SB.Append((auxNo.Value.ToArray()));
            } else {
                SB.Append((auxNo.Name + '=' + auxNo.Value));
            }
            auxNo = auxNo.Next;
        }

        return SB.ToString('&');
    };
}

function IsList(value) {
    if (value === undefined)
        return !IsPrimitive(this.Value) && this.Value instanceof DoublyLinkedList;
    else {
        return !IsPrimitive(value) && value instanceof DoublyLinkedList;
    }

};
function NodeDoublyLinkedList(value, name) {
    this.Previous = undefined;
    this.Value = value;
    this.Next = undefined;
    if (name === undefined) { this.Name = GUID(); } else { this.Name = name; };
    this.IsList = IsList;

}

function CreateList(name, Value) {
    var l = new DoublyLinkedList(name, Value);

    if (Value !== undefined)
        l.Add(Value);
    return l;

}
function CreateNode(Value, name) {
    return new NodeDoublyLinkedList(Value, name);
}
</script>