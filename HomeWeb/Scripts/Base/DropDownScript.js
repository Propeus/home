﻿var dropdown = $('.dropdownButton');
var lastColor;

var masterDropDown = dropdown.parent();
$('.dropdownButton').click(function () {

});

$(document).ready(function () {
    var color = dropdown.attr('data-color');
    lastColor = color;
    masterDropDown.addClass(lastColor);
})

$('.dropdownButtonItem').click(function () {

    masterDropDown.removeClass(lastColor);

    var color = $(this).attr('data-color');
    dropdown.attr('data-color', color);
    lastColor = color;
    masterDropDown.addClass(lastColor);

    var url = $(this).attr('data-url');
    if (url !== undefined)
        dropdown.attr('data-url', url);

    var params = $(this).attr('params');
    if (params !== undefined)
        dropdown.attr('params', params);

});