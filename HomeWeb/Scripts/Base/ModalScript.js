﻿var TModal;


$('.modalCall').click(function () {
    ShowModal(this)
});

var fOnApprove = function ($element) {
    if ($element !== undefined) {
        var idForm = $element.attr('form');
        var form = $('#' + idForm)
    }
    if (GetDataUrl(form) !== undefined) {
        AjaxForm(idForm, fOnApprove);
        return false;
    } else {
        $(TModal).find('.deny').click();
    }
}

//Usa quando o objeto é renderizado apos a conclusao da page
$(document).on('click', '#btnSubmit', function () {
    CloseModal();
});
$.fn.api.settings.api.searchDropdown = '/{/Controller}/Pesquisar?Query={query}|{/TipoPesquisa}';
function Show(JSONModel) {
    TModal = $('.ui.longer');
    //TModal.find('select').dropdown();
    var dropdown = TModal.find('select');
    if (dropdown !== undefined) {
        dropdown.dropdown({
            fields: { name: "name", value: "value" },
            filterRemoteData: false,
            saveRemoteData: false,
            apiSettings: {
                filterRemoteData: false,
                saveRemoteData: false,
                action: 'searchDropdown',
                urlData: {
                    Controller: GetController(),
                    TipoPesquisa: eTipoPesquisa.Dropdown
                },
              
                onResponse: function (response) {
                    var res = {
                        results: []
                    }


                    var sb = new CreateStringBuilder();
                    $.each(response, function (i, v) {
                        sb.Append(v);
                    });
                    JsonViewModel(sb.ToString());
                    var r = FromJSON(sb.ToString()).JSONModel;
                    $.each(r.Objetos, function (i, v) {
                        res.results.push({
                            name: v.name,
                            value: v.value
                            //url: v.url,
                            //link: v.url
                        });
                    });
                    return res;
                }

            }
        });
        if (JSONModel !== null && JSONModel !== undefined) {
            $.each(JSONModel.Objetos, function (i, v) {
                $('#divFormComparilhamento > div > div.menu').prepend('<div class="item" data-value="' + v.value + '">' + v.name + '</div>');
                $('select').prepend('<option value="' + v.value + '" class="addition">' + v.value + '</option>');
                dropdown.dropdown('set selected', v.value);

            });
        }
    }

    $(TModal).modal({
        //context: 'body',
        closable: false,
        allowMultiple: false,
        onHidden: function () {
            CloseModal();
        },
        onApprove: fOnApprove
    }).modal('show');
   

   
}

$('#mais').click(function () {
    var elem = $('.campo').last().clone().last();
    $('.DropdownAcessoPermissao').prepend(elem);
});

function ShowModal(obj) {
    CloseModal();
    if (obj != undefined) {
        AjaxCall(obj, Show);
    }
}

function CloseModal() {
    if (TModal != undefined) {
        $(TModal).remove();
    }

}

$(TModal).on(':hidden', function () {
    CloseModal();
})



