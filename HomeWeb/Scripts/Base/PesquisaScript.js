﻿$.fn.api.settings.api.search = '/{/Controller}/Pesquisar?Query={query}|{/TipoPesquisa}';
$('.ui.search').search({
    minCharacters: 3,
    maxResults: 10,
    onSelect: function (result, response) {
        var cg = new ComunicacaoGenerica();
        cg.DataUrl = result.link;
        AjaxCall(cg, Show);
        return response;
    },
    onResults: function (response) {
        return response;
    },
    apiSettings: {
        action: 'search',
        urlData: {
            Controller: GetController(),
            TipoPesquisa: eTipoPesquisa.Search
        },
        onResponse: function (response) {
            var res = {
                results: []
            };
            var sb = new CreateStringBuilder();
            $.each(response, function (i, v) {
                sb.Append(v);
            });
            JsonViewModel(sb.ToString());
            var r = FromJSON(sb.ToString()).JSONModel;
            $.each(r.Objetos, function (i, v) {
                res.results.push({
                    title: v.title,
                    description: v.description,
                    selected: v.selected,
                    //url: v.url,
                    link: v.url
                });
            });
            return res;
        }

    }


});

