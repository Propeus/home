﻿
var chat;
$(function () {
    chat = $.connection.signalRChat;
    chat.client.broadcastMessage = function (JSONModel) {
        SucessoMensagem('sucesso', 'ok');
        //JsonViewModel(JSONModel);
    };
    $.connection.hub.start();
    
})

function EnviarMensagemJson(JsonString) {
    chat.server.send(ToJson(JsonString));
}

function EnviarMensagem(titulo,mensagem) {
    var data = new CreateModel();
    data.mensagem.sucesso = new CreateSucessoMensagemModel();
    data.mensagem.sucesso.Mensagem = mensagem;
    data.mensagem.sucesso.Titulo = titulo;
    EnviarMensagemJson(data);
}

function EnviarChat(titulo, mensagem, Iddestinatarios) {
    var data = new CreateModel();
    data.mensagem.sucesso = new CreateSucessoMensagemModel();
    data.mensagem.sucesso.Mensagem = mensagem;
    data.mensagem.sucesso.Titulo = titulo;
    data.IdDestinatario = Iddestinatarios;
    chat.server.send(ToJson(data));
}

function CreateModel(privado) {
    this.IdDestinatario = [];
    this.mensagem = new CreateMensagemModel();
    this.status = eTipoMensagem.Sucesso;
    this.DataEnvio = undefined;
    this.Privativo = privado;
}

function CreateMensagemModel() {
    this.erro = undefined;
    this.sucesso = undefined;
}

function CreateSucessoMensagemModel() {
    this.Mensagem='';
    this.Titulo='';
}