﻿
function CreateStringBuilder() {
  
    this.List = new CreateList();
    this.Append = function (value) {
        this.List.Add(value);
    };
    this.ToString = function ToString(charJoin) {
        if (charJoin === undefined) {
            charJoin = '';
        }
        return this.List.ToArray().join(charJoin);
    }
    this.Clear = function Clear() {
        this.List.Clear();
    }
}