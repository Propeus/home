﻿var cmp = new Vue({
    el: '#formCadastro',
    data: {
        Nome: '',
        Email: '',
        Usuario: '',
        Senha: '',
        RSenha: '',
        NomeHidden: '',
        UsuarioHidden: '',
        EmailHidden: ''
    },
    computed: {
        isValid: function () {
            var r = this.Nome != '' && this.Email != '' && this.Usuario != '' && this.Senha != '' && this.RSenha != '' && (this.Senha == this.RSenha)
            this.Nome != '' ? this.NomeHidden = 'hidden' : this.NomeHidden = '';
            this.Usuario != '' ? this.UsuarioHidden = 'hidden' : this.UsuarioHidden = '';
            return r;
        },
        EmailValido: function () {
            var obj = this.Email;
            (Contains(obj, '@') && (Contains(obj, '.com') || Contains(obj, '.br'))) ? this.EmailHidden = 'hidden' : this.EmailHidden ='';
        }
    }
})