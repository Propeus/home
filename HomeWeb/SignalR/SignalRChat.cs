﻿using HomeBase;
using HomeSignalService;
using HomeWeb.Models.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HomeWeb.SignalR
{
    public class SignalRChat : SignalRMensagem
    {

        public static Dictionary<string, string> ConexaoGuidDic { get; set; } = new Dictionary<string, string>();


        public override void Send(string message)
        {

            message = ConfigureIdUser(message, typeof(CJSONSignalRChatModel));
            var data = message.FromJson<CJSONSignalRChatModel>();

            if (data.Privativo && data.IdDestinatario.Count == 0)
            {
                data.status = HomeBase.eEstadoModel.ErroSignal;
                data.IdDestinatario.Add(data.IdRementente);
                data.IdSignalRDestinatario.Add(data.IdSignalRRemetente);
                data.mensagem = new HomeBase.CMensagemModel
                {
                    erro = new HomeBase.CErroModel
                    {
                        Titulo = "Erro ao enviar a mensagem.",
                        Mensagem = "É obrigatório informar o destinatario caso a conversa seja privativa."
                    }
                };
            }
            else if (string.IsNullOrEmpty(data.GUIDChat) && data.Privativo==false)
            {
                if (ConexaoGuidDic.Count(x => x.Key == ConexaoAtual) > 0)
                {
                    var keyValuePair = ConexaoGuidDic.First(x => x.Key == ConexaoAtual);
                    data.GUIDChat = keyValuePair.Value;
                    if (!ConfiguracaoGlobal.IdGrupoAdmin.Contains(data.GUIDChat))
                        ConfiguracaoGlobal.IdGrupoAdmin.Add(data.GUIDChat);
                    JoinRoom(data.GUIDChat);

                }
                else
                {
                    var guid = HomeBase.Helper.RandomString();
                    if (!ConfiguracaoGlobal.IdGrupoAdmin.Contains(guid))
                        ConfiguracaoGlobal.IdGrupoAdmin.Add(guid);

                    ConexaoGuidDic.Add(ConexaoAtual, guid);
                    data.GUIDChat = guid;
                    JoinRoom(data.GUIDChat);
                }
            }

            if (string.IsNullOrEmpty(data.GUIDChat))
                base.Send(data.ToJson());
            else
            {

                var mensagem = data.ToJson();
                Clients.Group(data.GUIDChat).broadcastMessage(mensagem);
            }
        }

        public Task JoinRoom(string roomName)
        {
            return Groups.Add(Context.ConnectionId, roomName);
        }

        public Task LeaveRoom(string roomName)
        {
            return Groups.Remove(Context.ConnectionId, roomName);
        }

        public override Task OnConnected()
        {
            foreach (var gId in ConfiguracaoGlobal.IdGrupoAdmin)
            {
                JoinRoom(gId).Start();
            } 

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            foreach (var gId in ConfiguracaoGlobal.IdGrupoAdmin)
            {
                LeaveRoom(gId).Start();
            }

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            foreach (var gId in ConfiguracaoGlobal.IdGrupoAdmin)
            {
                JoinRoom(gId).Start();
            }
            return base.OnReconnected();
        }
    }
}